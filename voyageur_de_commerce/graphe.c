#include "graphe.h"

sommet_t *init_sommet(int num)
{
    sommet_t *sommet = (sommet_t *)malloc(sizeof(sommet_t));

    if (sommet)
    {
        sommet->numero = num;
        sommet->etat = 0;
        sommet->nb_voisins = 0;
        sommet->x = 0;
        sommet->y = 0;
    }

    return sommet;
}

/**
 * @brief création et initialisation d'une matrice de n par n
 *
 * @param n taille de la matrice
 * @return int** la matrice initialisé
 */
int **init_matrice(int n)
{
    int **matrice = (int **)malloc(n * sizeof(int *));
    if (matrice != NULL)
    {
        for (int i = 0; i < n; i++)
        {
            matrice[i] = (int *)malloc(n * sizeof(int));
            if (matrice[i]==NULL)
            {
                for (int j = 0; j < i; j++)
                {
                    free(matrice[j]);
                }
                free(matrice);
                matrice = NULL;
                exit(-1);
            }
        }
    }
    if (matrice != NULL)
    {
        for (int i = 0; i < n; i++)
        {
            for (int j = 0; j < n; j++)
            {
                matrice[i][j] = 0;
            }
        }
    }

    return matrice;
}

/**
 * @brief permet de libérer une matrice d'entiers
 * @param matrice matrice d'entier à libérer
 * @param taille entier qui contient la taille de la matrice
*/
void liberation_matrice(int **matrice, int taille){
    int i;
    for(i = 0; i <taille; i++){
        free(matrice[i]);
    }
    free(matrice);
    matrice = NULL;
}

/**
 * @brief libérer la table des sommets
 * @param tab_sommets table de pointeur de sommets
 * @param nb_sommets entier qui contient le nombre de sommets
*/
void liberation_tab_sommets(sommet_t **tab_sommets, int nb_sommets){
    int i;
    for(i = 0; i < nb_sommets; i++){
        free(tab_sommets[i]);
        tab_sommets[i] = NULL;
    }
}

/**
 * @brief calcule de la matrice des distance entre les points d'un graphe
 * @param tab tableau des arrete du graphe
 * @param taille taille de la matrice
*/
void floyd_warshall(int ** tab, int taille)
{
    for (int k = 0; k < taille; k++) {
        for (int i = 0; i < taille; i++) {
            for (int j = 0; j < taille; j++) {
                if (tab[i][k] + tab[k][j] < tab[i][j])
                    tab[i][j] = tab[i][k] + tab[k][j];
            }
        }
    }
}

/**
 * @brief crée un graphe complet à partir d'un autre graphe
 * @param
*/
void creation_graphe_complet(int **matrice_graphe_in, int taille_matrice, int **matrice_grpahe_complet){
    int i, j;
    for(i = 0; i < taille_matrice; i++){
        for(j = 0; j < taille_matrice; j++){
            if(!matrice_graphe_in[i][j] && i != j){
                matrice_grpahe_complet[i][j] = 999;
            }else{
                matrice_grpahe_complet[i][j] = matrice_graphe_in[i][j];
            }
        }
    }
    floyd_warshall(matrice_grpahe_complet, taille_matrice);
}


/**
 * @brief affiche la matrice de dimension dim
 * 
 * @param matrice matrice à afficher
 * @param dim dimension de la matrice
 */
void affiche_matrice(int **matrice, int dim)
{
    puts("");
    for (int i = 0; i < dim; i++)
    {
        for (int j = 0; j < dim; j++)
        {
            printf("%d ", matrice[i][j]);
        }
        puts("");
    }
}

/**
 * @brief génère un arbre couvrant pour une liste de sommet
 *
 * @param matrice
 * @param bas
 * @param haut
 */
void genere(int **matrice, int bas, int haut)
{
    if (bas < haut)
    {
        int k = bas + 1 + rand() % (haut - bas);
        matrice[bas][bas + 1] = 1;
        matrice[bas + 1][bas] = 1;
        if (k + 1 <= haut)
        {
            matrice[bas][k + 1] = 1;
            matrice[k + 1][bas] = 1;
        }
        genere(matrice, bas + 1, k);
        genere(matrice, k + 1, haut);
    }
}

/**
 * @brief création d'un graphe non orienté
 * @param matrice matrice du graphe
 * @param taille taille de la matrice
 * @param proba probabilité d'avoir un lien entre deux sommets
 */
void genere_graphe(int **matrice, int taille_matrice, float proba)
{
    int i, j;
    srand(time(NULL));
    for (i = 0; i < taille_matrice; i++)
    {
        for (j = i + 1; j < taille_matrice; j++)
        {
            if (rand() % 100 <= proba * 100)
            {
                matrice[i][j] = 1;
                matrice[j][i] = 1;
            }
        }
    }
}

/**
 * @brief initialise le tableau de sommets
 * @param matrice matrice d'entier contenant les liens entre sommets
 * @param taille_matrice entier qui défini la taille de la matrice
 * @param tab_sommet tableau de pointeurs de sommet à remplir
 */
void init_tab_sommets(int **matrice, int taille_matrice, sommet_t **tab_sommets)
{
    int i, j;
    for (i = 0; i < taille_matrice; i++)
    {
        tab_sommets[i] = init_sommet(i);
    }
    tab_sommets[0]->etat = 2;
    for(i = 0; i < taille_matrice; i++){
        for(j = i + 1; j < taille_matrice; j++){
            if(matrice[i][j] == 1){
                tab_sommets[i]->voisins[tab_sommets[i]->nb_voisins] = tab_sommets[j];
                tab_sommets[i]->nb_voisins++;
                tab_sommets[j]->voisins[tab_sommets[j]->nb_voisins] = tab_sommets[i];
                tab_sommets[j]->nb_voisins++;
            }
        }
    }
}


/**
 * @brief remise à zéro du parcours du graphe
 * @param listeSommet tableau de pointeur vers les sommets du grgenerateNeighboraphe
 * @param nb_sommet nombre de sommet dans le graphe
 */
void remise_a_zero(sommet_t **listeSommet, int nb_sommet)
{
    listeSommet[0]->etat = 2;
    for (int i = 1; i < nb_sommet; i++)
    {
        listeSommet[i]->etat = 0;
    }
}

/**
 * @brief passage d'un sommet à un autre
 *
 * @param src sommet initial
 * @param dst sommet de destination
 * @return sommet_t* sommet de destination avec les valeurs d'état mise à jour
 */
sommet_t *passage_sommet(sommet_t *src, sommet_t *dst)
{
    src->etat = 1;
    dst->etat = 2;

    return dst;
}

/**
 * @brief affiche les éléments d'un sommets
 * @param s sommet à afficher
*/
void affiche_sommet(sommet_t * s)
{
    printf("\n numéro : %d , état : %d ,nb_voisin : %d\n",s->numero, s->etat, s->nb_voisins);
    for(int i = 0 ; i < s->nb_voisins ; i++)
        printf(" voisin %d : sommet n°%d\n",i,s->voisins[i]->numero);
    puts("");
}

/**
 *  @brief vérifie si le joueur a fini de parcourir l'arbre
 * @param tab_sommets tableau conteant les adresse des sommets 
 * @param nb_sommets entier qui contient le nombre de sommets
 * @return entier pour savoir si le joueur a fini son parcours de l'arbre       //0 si fin        //1 si non fin
*/
int check_fin(sommet_t ** tab_sommets, int nb_sommets){
    int check = 0, i;
    if(tab_sommets[0]->etat == 2){
        for(i = 1; i < nb_sommets && check == 0; i++){
            if(tab_sommets[i]->etat != 1){
                check = 1;
            }
        }
    }
    else{
        check = 1;
    }
    return check;
}

/**
 * @brief trouve le parcour le moins long trouvé avec l'algo des fourmis
 * @param matrice_complet matrice du graphe complet
 * @param taille_matrice taille du graphe
 * @return retourne la distance du parcour le moins long trouvé avec l'algo des fourmis
*/
int algo_fourmi(int **matrice_complet, int taille_matrice){
    int tableau_dej_vis[MAX], ordre_vis[MAX][MAX], nb_vis = 0, 
    nb_fourmis = 10, 
    nb_jours = 10,
    distance_meil = 100;
    int i, j, k, l, m, suivant, sommet_courant, fin_parcour = 1, distance[MAX];
    float matrice_phero[MAX][MAX], total;
    for(i = 0; i < taille_matrice; i++){
        matrice_phero[i][i] = 0;
        total = 0;
        for(k = 0; k < taille_matrice; k++){
            if(matrice_complet[i][k] != 0){
                total += (1.0/matrice_complet[i][k]);
            }
        }
        for(j = 0; j < taille_matrice; j++){
            if(matrice_complet[i][j] != 0){
                matrice_phero[i][j] = (1.0/matrice_complet[i][j])/total;
            }
            else{
                matrice_phero[i][j] = 0;
            }
        }
    }
    for(m = 0; m < nb_jours; m++){//boucle jours de vie fourmis
        for(l = 0; l < nb_fourmis; l++){ // pour une seule fourmi l < 1
            for(i = 0; i < taille_matrice; i++){
                tableau_dej_vis[i] = 0;
            }
            nb_vis = 0;
            sommet_courant = 0;
            fin_parcour = 1;
            distance[l] = 0;
            ordre_vis[l][nb_vis] = 0;
            tableau_dej_vis[sommet_courant] = 1;
            nb_vis++;
            while (fin_parcour)
            {
                suivant = trouver_sommet_suivant(matrice_phero[sommet_courant], tableau_dej_vis, taille_matrice);
                if(suivant != -1){
                    tableau_dej_vis[suivant] = 1;
                    ordre_vis[l][nb_vis] = suivant;
                    nb_vis++;
                    distance[l] += matrice_complet[sommet_courant][suivant];
                    sommet_courant = suivant;
                }else{
                    distance[l] += matrice_complet[sommet_courant][0];
                    fin_parcour = 0;
                }
            }
        }
        ajuste_proba(matrice_phero, distance, nb_fourmis, taille_matrice, ordre_vis);
        distance_meil = trouver_distance_min(distance, nb_fourmis);
    }
    return distance[distance_meil];
}

/**
 * @brief permet d'ajuster les probas (les phéromones des fourmis) sur chaque arretes
 * @param matrice_phero matrice de contient les phéromones pour chaque arretes du graphe
 * @param distance tableau d'entier contenant les distances pour le parcours de chaque fourmi
 * @param nb_fourmis entier qui contient le nombre de fourmis
 * @param ordre_vis matrice d'entier qui contient pour chaque fourmi l'ordre de parcours des sommets
*/
void ajuste_proba(float matrice_phero[MAX][MAX], int * distance, int nb_fourmis, int taille_matrice, int ordre_vis[MAX][MAX])
{
    for(int i = 0; i < nb_fourmis; i++){
        for(int j = 0; j < taille_matrice-1; j++){
            if(ordre_vis[i][j] != ordre_vis[i][j+1])
            {
                matrice_phero[ordre_vis[i][j]][ordre_vis[i][j+1]] += 1.00/(float)distance[i];
                matrice_phero[ordre_vis[i][j+1]][ordre_vis[i][j]] += 1.00/(float)distance[i];
            }
        }
        if(ordre_vis[i][0] != ordre_vis[i][taille_matrice-1]){
            matrice_phero[ordre_vis[i][taille_matrice-1]][ordre_vis[i][0]] += 1.00/(float)distance[i];
            matrice_phero[ordre_vis[i][0]][ordre_vis[i][taille_matrice-1]] += 1.00/(float)distance[i];
        }
    }
    for(int i = 0; i < taille_matrice; i++){
        float total = 0;
        for(int k = 0; k < taille_matrice; k++){
            if(matrice_phero[i][k] != 0.0 && i!=k){
                total += (1.0/matrice_phero[i][k]);
            }
        }

        for(int j = 0; j < taille_matrice; j++){
            if(matrice_phero[i][j] != 0.0 && i!=j){
                matrice_phero[i][j] = (1.00/matrice_phero[i][j])/total;
            }
            else{
                matrice_phero[i][j] = 0.0;
            }
        }
    }
}

/**
 * @brief retourne l'indice du tableau ayant la plus petite distance
 * @param distances tableau d'entier contenat les distances
 * @param taille entier qui du tableau
 * @return indice du tableau ayant la plus petite distance
*/
int trouver_distance_min(int *distances, int taille){
    int i, distance_min = distances[0], indice = 0;
    for(i = 1; i < taille; i++){
        if(distance_min > distances[i]){
            distance_min = distances[i];
            indice = i;
        }
    }
    return indice;
} 


/**
 * @brief trouve une solution au jeu avec la méthode du recuit simulé
 * @param graphe_complet graphe complet liée au graphe de base 
 * @param nb_sommets entier qui contient le nombre de sommets
 * @param t valeur initiale de la température
 * @return entier pour savoir si le joueur a fini son parcours de l'arbre       //0 si fin        //1 si non fin
*/
int recuit_simule(int ** graphe_complet, int nb_sommets, float t)
{
    int solution[MAX], candidat[MAX];
    int valeur_parcours_sol = 0, valeur_parcours_cand = 0, x, y, tmp, diff;
    float proba;

    for (int i = 0; i < nb_sommets; i++)
    {
        solution[i] = i;
        candidat[i] = i;
    }

    for (int i = 0; i < nb_sommets-1; i++)
    {
        valeur_parcours_sol += graphe_complet[solution[i]][solution[i+1]];
    }
    valeur_parcours_sol += graphe_complet[solution[nb_sommets -1]][0];

    while (t > 0.004)
    {
        // variation du tableau echange 2 valeurs du tableau
        x = rand()%nb_sommets;
        y = rand()%nb_sommets;
        while (x==y || x==0 || y==0)
        {
            x = rand()%nb_sommets;
            y = rand()%nb_sommets;
        }

        for (int i = 0; i < nb_sommets; i++)
        {
            candidat[i] = solution[i];
        }

        tmp = candidat[x]; 
        candidat[x] = candidat[y];
        candidat[y] = tmp;

        // tmp = candidat[1];
        // for(int i = 1; i < nb_sommets-1; i++)
        //     candidat[i] = candidat[i+1];
        // candidat[nb_sommets-1] = tmp;

        valeur_parcours_cand = 0;

        for (int i = 0; i < nb_sommets-1; i++)
        {
            valeur_parcours_cand += graphe_complet[candidat[i]][candidat[i+1]];
        }
        valeur_parcours_cand += graphe_complet[candidat[nb_sommets -1]][0];

        diff = valeur_parcours_cand - valeur_parcours_sol;

        proba = expf(-diff/t);

        if ((valeur_parcours_cand < valeur_parcours_sol || checkProbability(proba)) && valeur_parcours_cand != valeur_parcours_sol)
        {
            // printf("sol : [ ");
            for(int i = 0 ; i < nb_sommets ; i++){
                solution[i] = candidat[i];
                // printf("%d ", candidat[i]);
            }
            valeur_parcours_sol = valeur_parcours_cand;
            // printf("] distance : %d  Température : %f proba : %f\n",valeur_parcours_sol, t, proba*100);
        }
        t -= 0.005;


    }
    return valeur_parcours_sol;
}

/**
 * @brief fonction qui prend en entrée une probabilité et renvoie vrai ou faus en fonction de cette proba
 * 
 * @param prob la probabilité de succes
 */
int checkProbability(float prob) {
    float randomValue = (float)rand() / RAND_MAX; // Génère une valeur aléatoire entre 0 et 1

    if (randomValue < prob) {
        return 1; // Vrai
    } else {
        return 0; // Faux
    }
}

/**
 * @brief permet de trouver le numéro du sommet suivant
 * @param tableau tableau contenant les proba du sommet courant
 * @param tableau_deja_visite tableau des sommets déjé visités
 * @param taille_matrice nombre de sommets de la matrice
*/
int trouver_sommet_suivant(float * tableau, int * tableau_deja_visite, int taille_matrice){
    float temp[MAX], total, somme_cumulee = 0;
    int i, fin = 1, sommet = -1;
    total = 0;
        for(i = 0; i < taille_matrice; i++){
            temp[i] = 0.0;
        }
        for(i = 0; i < taille_matrice; i++){
            if(tableau_deja_visite[i] == 0){
                total += tableau[i];
            }
        }
        if(total == 0.0){
            sommet = -1;
        }
        else{
            while(sommet <= 0){
                fin = 1;
                for(i=0; i<taille_matrice; i++){
                    if(tableau_deja_visite[i] == 0){
                        temp[i] = (tableau[i]/total)*100;
                    }else{
                        temp[i] = 0;
                    }
                }
                float alea = (float)(rand()%101);
                i=0;
                while(fin && i<taille_matrice)
                {
                    somme_cumulee += temp[i];
                    if(alea < somme_cumulee){
                        sommet = i;
                        fin = 0;
                    }
                    i++;        
                }
            }
        }

    return sommet;
}

/**
 * @brief affiche la matrice de dimension dim de floattant
 * 
 * @param matrice matrice à afficher
 * @param dim dimension de la matrice
 */
void affiche_matrice_float(float matrice[MAX][MAX], int dim)
{
    puts("");
    for (int i = 0; i < dim; i++)
    {
        for (int j = 0; j < dim; j++)
        {
            printf("%f ", matrice[i][j]);
        }
        puts("");
    }
}

void graphe()
{

    int n = 5; // nb de sommet
    //int distance = 0;    
    float p = 0.2; // probabilité lien

    int ** Tab = init_matrice(n);
    int ** Tab_complet = init_matrice(n);
    //sommet_t *tab_sommets[MAX];

    genere(Tab, 0, n - 1);

    affiche_matrice(Tab, n);

    genere_graphe(Tab, n, p);

    affiche_matrice(Tab, n);

    //init_tab_sommets(Tab, n, tab_sommets);

    /*for(int i=0 ; i < n; i++){
        affiche_sommet(tab_sommets[i]);
    }*/
    creation_graphe_complet(Tab, n, Tab_complet);
    
    
    affiche_matrice(Tab_complet, n);

    algo_fourmi(Tab_complet, n);

    liberation_matrice(Tab, n);
    liberation_matrice(Tab_complet, n);

}