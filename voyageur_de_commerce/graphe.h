/**
 * @file graphe.h
 * @brief fichier d'entete pour la création d'un graphe
 */
#ifndef __GRAPHE_H__
#define __GRAPHE_H__


#include<stdio.h>
#include<stdlib.h>
#include<time.h>
#include<math.h>

#define MAX 50


/**
 * @struct sommet
 * @brief structure des sommets pour le graphe
 * @param numero identifiant du sommet
 * @param etat etat du sommet : 1 si sommet déjà passée, 2 si sommet courant, 0 sinon
 * @param voisins liste des voisins
 * @param x coordonnée du sommet en x
 * @param yc oordonnée du sommet en y
 * */
typedef struct sommet
{
    int numero;
    int etat;
    struct sommet * voisins[MAX];
    int nb_voisins;
    int x;
    int y;

}sommet_t;


/**
 * @brief fonction initialisation d'un sommet
 * 
 * @param num le numéro du sommet
 * @return sommet_t* 
 */
sommet_t * init_sommet(int num);

/**
 * @brief création et initialisation d'une matrice de n par n
 * @param n taille de la matrice 
 * @return int** la matrice initialisé
 */
int ** init_matrice(int n);

/**
 * @brief génère un arbre couvrant pour une liste de sommet
 * @param matrice d'entier à générer
 * @param bas 
 * @param haut 
 */
void genere(int** matrice, int bas, int haut);

/**
 * @brief création d'un graphe non orienté
 * @param matrice matrice du graphe
 * @param taille taille de la matrice
 * @param proba probabilité d'avoir un lien entre deux sommets
*/
void genere_graphe(int **matrice, int taille_matrice, float proba);

/**
 * @brief initialise le tableau de sommets
 * @param matrice matrice d'netier contenant les liens entre sommets
 * @param taille_matrice entier qui défini la taille de la matrice
 * @param tab_sommet tableau de pointeurs de sommet àà remplir
*/
void init_tab_sommets(int **matrice, int taille_matrice, sommet_t ** tab_sommets);

/**
 * @brief affiche la matrice de dimension dim
 * @param matrice matrice à afficher
 * @param dim dimension de la matrice
 */
void affiche_matrice(int** matrice, int dim);

/**
 * @brief calcule de la matrice des distance entre les points d'un graphe
 * @param tab tableau des arrete du graphe
 * @param taille taille de la matrice
*/
void floyd_warshall(int ** tab, int taille);

/**
 * @brief crée un graphe complet à partir d'un autre graphe
 * @param matrice_graphe_in matrice du graphe d'entré non complet
 * @param taille_matrice taille de la matrice (= nombre de sommets)
 * @param matrice_grpahe_complet matrice du grpahe complet créé
*/
void creation_graphe_complet(int **matrice_graphe_in, int taille_matrice, int **matrice_grpahe_complet);

/**
 * @brief remise à zéro du parcours du graphe
 * @param listeSommet tableau de pointeur vers les sommets du graphe
 * @param nb_sommet nombre de sommet dans le graphe
 * @param courant sommet actuel
*/
void remise_a_zero(sommet_t ** listeSommet, int nb_sommet);


/**
 * @brief passage d'un sommet à un autre 
 * 
 * @param src sommet initial
 * @param dst sommet de destination
 * @return sommet_t* sommet de destination avec les valeurs d'état mise à jour
 */sommet_t * passage_sommet(sommet_t * src, sommet_t * dst);

/**
 * @brief affiche les éléments d'un sommets
 * @param s sommet à afficher
*/
void affiche_sommet(sommet_t * s);

/**
 *  @brief vérifie si le joueur a fini de parcourir l'arbre
 * @param tab_sommets tableau conteant les adresse des sommets 
 * @param nb_sommets entier qui contient le nombre de sommets
 * @return entier pour savoir si le joueur a fini son parcours de l'arbre       //0 si fin        //1 si non fin
*/
int check_fin(sommet_t ** tab_sommets, int nb_sommets);

/**
 * @brief trouve le parcour le moins long trouvé avec l'algo des fourmis
 * @param matrice_complet matrice du graphe complet
 * @param taille_matrice taille du graphe
 * @return retourne la distance du parcour le moins long trouvé avec l'algo des fourmis
*/
int algo_fourmi(int **matrice_complet, int taille_matrice);

/**
 *  @brief trouve une solution au jeu avec la méthode du recuit simulé
 * @param graphe_complet graphe complet liée au graphe de base 
 * @param nb_sommets entier qui contient le nombre de sommets
 * @param t valeur initiale de la température
 * @return entier pour savoir si le joueur a fini son parcours de l'arbre       //0 si fin        //1 si non fin
*/
int recuit_simule(int ** graphe_complet, int nb_sommets, float t);



/**
 * @brief fonction qui prend en entrée une probabilité et renvoie vrai ou faus en fonction de cette proba
 * 
 * @param prob la probabilité de succes
 */
int checkProbability(float prob);

/**
 * @brief permet de trouver le numéro du sommet suivant
 * @param tableau tableau contenant les proba du sommet courant
 * @param tableau_deja_visite tableau des sommets déjé visités
 * @param taille_matrice nombre de sommets de la matrice
*/
int trouver_sommet_suivant(float * tableau, int * tableau_deja_visite, int taille_matrice);

/**
 * @brief permet d'ajuster les probas (les phéromones des fourmis) sur chaque arretes
 * @param matrice_phero matrice de contient les phéromones pour chaque arretes du graphe
 * @param distance tableau d'entier contenant les distances pour le parcours de chaque fourmi
 * @param nb_fourmis entier qui contient le nombre de fourmis
 * @param ordre_vis matrice d'entier qui contient pour chaque fourmi l'ordre de parcours des sommets
*/
void ajuste_proba(float matrice_phero[MAX][MAX], int * distance, int nb_fourmis, int taille_matrice, int ordre_vis[MAX][MAX]);

/**
 * @brief affiche la matrice de dimension dim de floattant
 * @param matrice matrice à afficher
 * @param dim dimension de la matrice
 */
void affiche_matrice_float(float matrice[MAX][MAX], int dim);

/**
 * @brief retourne l'indice du tableau ayant la plus petite distance
 * @param distances tableau d'entier contenat les distances
 * @param taille entier qui du tableau
 * @return indice du tableau ayant la plus petite distance
*/
int trouver_distance_min(int *distances, int taille);

/**
 * @brief permet de libérer une matrice d'entiers
 * @param matrice matrice d'entier à libérer
 * @param taille entier qui contient la taille de la matrice
*/
void liberation_matrice(int **matrice, int taille);

/**
 * @brief libérer la table des sommets
 * @param tab_sommets table de pointeur de sommets
 * @param nb_sommets entier qui contient le nombre de sommets
*/
void liberation_tab_sommets(sommet_t **tab_sommets, int nb_sommets);

/**
 * @brief main de graphe
 * 
 */
void graphe();


#endif