/**
 * @file voyageur.h
 * @brief fichier d'entete pour le jeu du voyageur
 */
#ifndef __VOYAGEUR_H__
#define __VOYAGEUR_H__


#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>
#include <math.h>
#include <stdio.h>
#include <string.h>
#include <math.h>
#include "graphe.h"

/**
 * @brief fonction de fermeture de toutes les variables ouvertes par l'utilisation de la SDL
 * 
 * @param ok fin normale : ok = 0 ; anormale ok = 1
 * @param msg message à afficher
 * @param window fenêtre à fermer
 * @param renderer rendu à fermer
 * @param texture rendu à fermer
 */
void end_sdl(char ok, char const *msg, SDL_Window *window, SDL_Renderer *renderer, SDL_Texture *texture[10]);

/**
 * @brief permet de savoir quel sommet a été cliqué
 * @param current sommet_t sommet courant
 * @param x entier qui contient la position en abcisse du curseur
 * @param y entier qui contient la position en ordonnée du curseur
 * @param size entier taille du carré
*/
sommet_t *check_click_voisin(sommet_t *current, int x, int y, int size);

/**
 * @brief fonction qui charge une image dans une texture 
 * 
 * @param file_image_name nom du fichier de l'image
 * @param window fenetre à fermer si le chargement echoue
 * @param renderer rendu permettant le chargement de l'image
 * @return SDL_Texture* l'image charé dans l'adresse d'une sdl_texture 
 */
SDL_Texture* load_texture_from_image(char  *  file_image_name, SDL_Window *window, SDL_Renderer *renderer, SDL_Texture* texture[10]);

/**
 * @brief fonction d'affichage graphique des sommets du jeu
 * 
 * @param window fenêtre du jeu
 * @param renderer rendu à afficher
 * @param texture tableau de texture 
 * @param sommets liste sommet à afficher
 * @param nb_sommet nombre de sommets
 */
void affiche_sommet_sdl(SDL_Window * window, SDL_Renderer * renderer, SDL_Texture *texture[10], sommet_t ** sommets, int nb_sommet, int size, sommet_t * courant);

/**
 * @brief en fonction du nombre de sommet , cette fonction écrit la position des points dans la structure associé
 * 
 * @param sommets liste de sommets
 * @param nb_sommet le nombre de sommet de la liste
 * @param size la taille de l'écran
 */
void placement_sommet(sommet_t ** sommets, int nb_sommet, int size_x, int size_y);

/**
 * @brief main de voyageur
 * 
 */
void voyageur();

#endif