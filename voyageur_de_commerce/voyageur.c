#include "voyageur.h"
#include "graphe.h"
#include <SDL2/SDL_ttf.h>

/*********************************************************************************************************************/
/*                              Programme d'exemple de création de rendu + dessin                                    */
/*********************************************************************************************************************/

/**
 * @brief fonction de fermeture de toutes les variables ouvertes par l'utilisation de la SDL
 *
 * @param ok fin normale : ok = 0 ; anormale ok = 1
 * @param msg message à afficher
 * @param window fenêtre à fermer
 * @param renderer rendu à fermer
 * @param texture rendu à fermer
 */
void end_sdl(char ok, char const *msg, SDL_Window *window, SDL_Renderer *renderer, SDL_Texture *texture[10])
{
    char msg_formated[255];
    int l;

    if (!ok)
    { // Affichage de ce qui ne va pas
        strncpy(msg_formated, msg, 250);
        l = strlen(msg_formated);
        strcpy(msg_formated + l, " : %s\n");

        SDL_Log(msg_formated, SDL_GetError());
    }

    for (int i = 0; i < 10; i++)
    {
        if (texture[i] != NULL)
        {                                   // Destruction si nécessaire de la texture
            SDL_DestroyTexture(texture[i]); // Attention : on suppose que les NULL sont maintenus !!
            texture[i] = NULL;
        }
    }
    if (renderer != NULL)
    {                                  // Destruction si nécessaire du renderer
        SDL_DestroyRenderer(renderer); // Attention : on suppose que les NULL sont maintenus !!
        renderer = NULL;
    }
    if (window != NULL)
    {                              // Destruction si nécessaire de la fenêtre
        SDL_DestroyWindow(window); // Attention : on suppose que les NULL sont maintenus !!
        window = NULL;
    }

    IMG_Quit();
    SDL_Quit();

    if (!ok)
    { // On quitte si cela ne va pas
        exit(EXIT_FAILURE);
    }
}

/**
 * @brief fonction qui charge une image dans une texture
 *
 * @param file_image_name nom du fichier de l'image
 * @param window fenetre à fermer si le chargement echoue
 * @param renderer rendu permettant le chargement de l'image
 * @return SDL_Texture* l'image charé dans l'adresse d'une sdl_texture
 */
SDL_Texture *load_texture_from_image(char *file_image_name, SDL_Window *window, SDL_Renderer *renderer, SDL_Texture *texture[10])
{
    SDL_Surface *my_image = NULL;   // Variable de passage
    SDL_Texture *my_texture = NULL; // La texture

    my_image = IMG_Load(file_image_name); // Chargement de l'image dans la surface
                                          // image=SDL_LoadBMP(file_image_name); fonction standard de la SDL,
                                          // uniquement possible si l'image est au format bmp */
    if (my_image == NULL)
        end_sdl(0, "Chargement de l'image impossible", window, renderer, texture);

    my_texture = SDL_CreateTextureFromSurface(renderer, my_image); // Chargement de l'image de la surface vers la texture
    SDL_FreeSurface(my_image);                                     // la SDL_Surface ne sert que comme élément transitoire
    if (my_texture == NULL)
        end_sdl(0, "Echec de la transformation de la surface en texture", window, renderer, texture);

    return my_texture;
}

/**
 * @brief permet de savoir quel sommet a été cliqué
 * @param current sommet_t sommet courant
 * @param x entier qui contient la position en abcisse du curseur
 * @param y entier qui contient la position en ordonnée du curseur
 * @param size entier taille du carré
*/
sommet_t *check_click_voisin(sommet_t *current, int x, int y, int size)
{
    sommet_t *res = NULL;
    for (int i = 0; i < current->nb_voisins; i++)
    {
        if ((x > current->voisins[i]->x) && (x < current->voisins[i]->x + size) && (y > current->voisins[i]->y) && (y < current->voisins[i]->y + size))
        {
            res = current->voisins[i];
            break;
        }
    }
    return res;
}

/**
 * @brief en fonction du nombre de sommet , cette fonction écrit la position des points dans la structure associé
 *
 * @param sommets liste de sommets
 * @param nb_sommet le nombre de sommet de la liste
 * @param size la taille de l'écran
 */
void placement_sommet(sommet_t **sommets, int nb_sommet, int size_x, int size_y)
{
    double angle,
        angleIncr = 2 * 3.14 / nb_sommet;
    int i;

    for (i = 0; i < nb_sommet; i++)
    {
        angle = i * angleIncr;
        sommets[i]->x = size_x / 2 + 200 * cos(angle);
        sommets[i]->y = size_y / 2 + 200 * sin(angle);
    }
}

/**
 * @brief fonction d'affichage graphique des sommets du jeu
 *
 * @param window fenêtre du jeu
 * @param renderer rendu à afficher
 * @param texture tableau de texture
 * @param sommets liste sommet à afficher
 * @param nb_sommet nombre de sommets
 */
void affiche_sommet_sdl(SDL_Window *window, SDL_Renderer *renderer, SDL_Texture *texture[10], sommet_t **sommets, int nb_sommet, int size, sommet_t *courant)
{
    int h, w;
    SDL_GetWindowSize(window, &w, &h);

    SDL_Rect fond = {0},
             rect_temps = {0},
             pos = {0};

    fond.x = 0;
    fond.y = 0;
    fond.w = w;
    fond.h = h;

    pos.h = size;
    pos.w = size;

    SDL_RenderCopy(renderer, texture[0], NULL, &fond);
    SDL_SetRenderDrawColor(renderer, 255, 255, 255, 255);

    for (int i = 0; i < nb_sommet; i++)
    {
        for (int j = 0; j < sommets[i]->nb_voisins; j++)
        {
            SDL_RenderDrawLine(renderer, sommets[i]->x + size / 2, sommets[i]->y + size / 2,
                               sommets[i]->voisins[j]->x + size / 2, sommets[i]->voisins[j]->y + size / 2);
        }
    }

    for (int i = 0; i < nb_sommet; i++)
    {
        pos.x = sommets[i]->x;
        pos.y = sommets[i]->y;
        if (i == 0 && courant != sommets[0])
            SDL_SetRenderDrawColor(renderer, 255, 128, 0, 255);
        else if (sommets[i]->etat == 2)
            SDL_SetRenderDrawColor(renderer, 255, 0, 0, 255);
        else if (sommets[i]->etat == 1)
            SDL_SetRenderDrawColor(renderer, 0, 255, 0, 255);
        else
            SDL_SetRenderDrawColor(renderer, 255, 255, 255, 255);

        SDL_RenderFillRect(renderer, &pos);
        SDL_SetRenderDrawColor(renderer, 0, 0, 0, 255);
        SDL_RenderDrawRect(renderer, &pos);
    }
    
    rect_temps.y = w*0.025;
    SDL_QueryTexture(texture[2], NULL, NULL, &rect_temps.w, &rect_temps.h);
    SDL_RenderCopy(renderer, texture[2], NULL, &rect_temps);  
    SDL_RenderPresent(renderer);
    SDL_Delay(40);
}

void voyageur()
{

    SDL_Window *window = NULL;
    SDL_Renderer *renderer = NULL;
    SDL_Texture *texture[10];
    SDL_DisplayMode screen;
    int h, w, x, y;
    TTF_Font *font = NULL; // la variable 'police de caractère'
    SDL_Color color = {255, 255, 255, 255};
    SDL_Surface *text_surface = NULL;
    SDL_Surface *text_surface_temps = NULL;    
    char mot[255];
    char temps[255];

    /*********************************************************************************************************************/
    /*                         Initialisation de la SDL  + gestion de l'échec possible                                   */

    for (int i = 0; i < 10; i++)
    {
        texture[i] = NULL;
    }

    if (SDL_Init(SDL_INIT_VIDEO) != 0)
        end_sdl(0, "ERROR SDL INIT", window, renderer, texture);

    SDL_GetCurrentDisplayMode(0, &screen);

    /* Création de la fenêtre */
    window = SDL_CreateWindow("voyageur",
                              SDL_WINDOWPOS_CENTERED,
                              SDL_WINDOWPOS_CENTERED, screen.w * 0.8,
                              screen.h * 0.6,
                              SDL_WINDOW_OPENGL);
    if (window == NULL)
        end_sdl(0, "ERROR WINDOW CREATION", window, renderer, texture);

    /* Création du renderer */
    renderer = SDL_CreateRenderer(window, -1,
                                  SDL_RENDERER_ACCELERATED | SDL_RENDERER_PRESENTVSYNC);
    if (renderer == NULL)
        end_sdl(0, "ERROR RENDERER CREATION", window, renderer, texture);

    texture[0] = load_texture_from_image("img/fond.jpg", window, renderer, texture);

    SDL_GetWindowSize(window, &w, &h);

    const Uint8 *keystates = SDL_GetKeyboardState(NULL); // Récupère l'état des touches du clavier

    if (TTF_Init() < 0)
        end_sdl(0, "Couldn't initialize SDL TTF", window, renderer, texture);
    font = TTF_OpenFont("./fonts/APOLLO.ttf", 30); // La police à charger, la taille désirée
    if (font == NULL)
        end_sdl(0, "Can't load font", window, renderer, texture);

    // Initialisation Graphe et sommets

    int n = rand() % 5 + 5; // nb de sommet
    float p = 0.2;          // probabilité lien
    int size = w / 20, tentative_encours = 0, sol;

    int **Tab = init_matrice(n);
    int **Tab_complet = init_matrice(n);
    sommet_t *tab_sommets[MAX];
    sommet_t *courant = NULL, *suivant = NULL;

    genere(Tab, 0, n - 1);

    genere_graphe(Tab, n, p);

    init_tab_sommets(Tab, n, tab_sommets);

    placement_sommet(tab_sommets, n, w, h);

    creation_graphe_complet(Tab, n, Tab_complet);

    sol = recuit_simule(Tab_complet, n, 5.0);

    int sol_fourmi;
    sol_fourmi = algo_fourmi(Tab_complet, n);


    /*********************************************************************************************************************/
    /*                                     On dessine dans le renderer                                                   */
    /*********************************************************************************************************************/
    /*             Cette partie pourrait avantageusement être remplacée par la boucle évènementielle                     */

    SDL_bool program_on = SDL_TRUE; // Booléen pour dire que le programme doit continuer
    SDL_Event event;                // c'est le type IMPORTANT !!

    courant = tab_sommets[0];
    int startTime = SDL_GetTicks();

    //printf("SOLUtion fourmis : %d\n", sol_fourmi);
    while (program_on && check_fin(tab_sommets, n))
    { // Voilà la boucle des évènements
        if (SDL_PollEvent(&event))
        {
            keystates = SDL_GetKeyboardState(NULL);
            switch (event.type)
            {                           // En fonction de la valeur du type de cet évènement
            case SDL_QUIT:              // Un évènement simple, on a cliqué sur la x de la fenêtre
                program_on = SDL_FALSE; // Il est temps d'arrêter le programme
                break;

            case SDL_MOUSEBUTTONDOWN:
                x = event.button.x;
                y = event.button.y;
                suivant = check_click_voisin(courant, x, y, size);
                if (suivant)
                {
                    courant = passage_sommet(courant, suivant);
                    tentative_encours++;
                }
            default:
                break;
            }
            if (keystates[SDL_SCANCODE_ESCAPE])
                program_on = SDL_FALSE;
            else if (keystates[SDL_SCANCODE_BACKSPACE])
            {
                remise_a_zero(tab_sommets, n);
                courant = tab_sommets[0];
            }
        }
        else
        {
            // Calculer le temps écoulé
            int currentTime = SDL_GetTicks();
            int elapsedTime = currentTime - startTime;

            // Afficher le temps écoulé dans la console
            sprintf(temps, "Temps ecoule :  %d s", elapsedTime/1000);
            text_surface_temps = TTF_RenderText_Blended(font, temps, color);
            if (text_surface_temps == NULL){
                end_sdl(0, "Can't create text surface", window, renderer, texture); // la texture qui contient le texte
                liberation_matrice(Tab, n);
                liberation_matrice(Tab_complet, n);
                liberation_tab_sommets(tab_sommets, n);
            }
            texture[2] = SDL_CreateTextureFromSurface(renderer, text_surface_temps);    
            if (texture[2] == NULL){
                end_sdl(0, "Can't create texture from surface", window, renderer, texture);
                liberation_matrice(Tab, n);
                liberation_matrice(Tab_complet, n);
                liberation_tab_sommets(tab_sommets, n);
            }
            SDL_FreeSurface(text_surface_temps); 

            affiche_sommet_sdl(window, renderer, texture, tab_sommets, n, size, courant);
        }

    }
    if (!check_fin(tab_sommets, n))
    {     
        affiche_sommet_sdl(window, renderer, texture, tab_sommets, n, size, courant);
        sprintf(mot, "Distance parcourue %d  Distance trouvee recuit : %d  Distance trouvee fourmis : %d", 
        tentative_encours, sol, sol_fourmi);
        text_surface = TTF_RenderText_Blended(font, mot, color);
        if (text_surface == NULL){
            end_sdl(0, "Can't create text surface", window, renderer, texture); // la texture qui contient le texte
            liberation_matrice(Tab, n);
            liberation_matrice(Tab_complet, n);
            liberation_tab_sommets(tab_sommets, n);

        }
        texture[1] = SDL_CreateTextureFromSurface(renderer, text_surface);      // transfert de la surface à la texture
        if (texture[1] == NULL){
            end_sdl(0, "Can't create texture from surface", window, renderer, texture);
            liberation_matrice(Tab, n);
            liberation_matrice(Tab_complet, n);
            liberation_tab_sommets(tab_sommets, n);
            
        }
        SDL_FreeSurface(text_surface);                            // la texture ne sert plus à rien
        SDL_Rect pos = {0, 0, 0, 0};                              // rectangle où le texte va être prositionné
        SDL_QueryTexture(texture[1], NULL, NULL, &pos.w, &pos.h); // récupération de la taille (w, h) du texte
        SDL_RenderCopy(renderer, texture[1], NULL, &pos);         // Ecriture du texte dans le renderer
        SDL_RenderPresent(renderer);
        SDL_Delay(5000);
    }
    TTF_Quit();
    liberation_matrice(Tab, n);
    liberation_matrice(Tab_complet, n);
    liberation_tab_sommets(tab_sommets, n);
    /* on referme proprement la SDL */
    end_sdl(1, "Normal ending", window, renderer, texture);
}