#include <SDL2/SDL.h>
#include <math.h>
#include <stdio.h>
#include <string.h>


/**
 * @brief destruction du renderer utilisé pour dessiner et fermeture de la fenetre
 * @param ok défini si la fin s'est bien passé : fin normale : ok = 0 ; anormale ok = 1
 * @param message meassage à afficher
*/
void fin_dessin(char ok,            // fin normale : ok = 0 ; anormale ok = 1
             char const *msg,    // message à afficher
             SDL_Window *window, // fenêtre à fermer
             SDL_Renderer *renderer)
{ // renderer à fermer
    char msg_formated[255];
    int l;

    if (!ok)
    { // Affichage de ce qui ne va pas
        strncpy(msg_formated, msg, 250);
        l = strlen(msg_formated);
        strcpy(msg_formated + l, " : %s\n");

        SDL_Log(msg_formated, SDL_GetError());
    }

    if (renderer != NULL)
    {                                  // Destruction si nécessaire du renderer
        SDL_DestroyRenderer(renderer); // Attention : on suppose que les NULL sont maintenus !!
        renderer = NULL;
    }
    if (window != NULL)
    {                              // Destruction si nécessaire de la fenêtre
        SDL_DestroyWindow(window); // Attention : on suppose que les NULL sont maintenus !!
        window = NULL;
    }

    SDL_Quit();

    if (!ok)
    { // On quitte si cela ne va pas
        exit(EXIT_FAILURE);
    }
}

/**
 * @brief dessin dans un renderer d'une élipse
 * @param renderer adresse du renderer sur lequel on dessine
 * @param pos_x entier qui désinge l'abcisse utilisée pour dessiner l'élipse
 * @param pos_y entier qui désinge l'ordonnée utilisée pour dessiner l'élipse
*/
void draw(SDL_Renderer *renderer, int pos_x, int pos_y)
{ // Je pense que vous allez faire moins laid :)
    /* tracer un cercle n'est en fait pas trivial, voilà le résultat sans algo intelligent ... */
    for (float angle = 0; angle < 2 * M_PI; angle += M_PI / 4000)
    {
        SDL_SetRenderDrawColor(renderer,
                               (cos(angle * 2 + pos_y/100) + 1) * 255 / 2, // quantité de Rouge
                               (cos(angle * 5 + pos_x/100) + 1) * 255 / 2, //          de vert
                               (cos(angle) + 1) * 255 / 2,     //          de bleu
                               255);                           // opacité = opaque
        SDL_RenderDrawPoint(renderer,
                            pos_x + 100 * cos(angle),  // coordonnée en x
                            pos_y + 150 * sin(angle)); //            en y
    }
}

/**
 * @brief mise en place d'une fenetre de dessin
*/
void creation_fenetre(void){
    SDL_Window *window = NULL;
    SDL_Renderer *renderer = NULL;

    SDL_DisplayMode screen;

    SDL_bool program_on = SDL_TRUE;
    SDL_Event event;
    int pos_x = 0, pos_y = 0, hauteur, largeur, i=1,j=1; 

    /*********************************************************************************************************************/
    /*                         Initialisation de la SDL  + gestion de l'échec possible                                   */
    if (SDL_Init(SDL_INIT_VIDEO) != 0)
        fin_dessin(0, "ERROR SDL INIT", window, renderer);

    SDL_GetCurrentDisplayMode(0, &screen);
    printf("Résolution écran\n\tw : %d\n\th : %d\n",
           screen.w, screen.h);

    /* Création de la fenêtre */
    window = SDL_CreateWindow("Premier dessin",
                              SDL_WINDOWPOS_CENTERED,
                              SDL_WINDOWPOS_CENTERED, screen.w * 0.66,
                              screen.h * 0.66,
                              SDL_WINDOW_OPENGL);
    if (window == NULL)
        fin_dessin(0, "ERROR WINDOW CREATION", window, renderer);

    /* Création du renderer */
    renderer = SDL_CreateRenderer(window, -1,
                                  SDL_RENDERER_ACCELERATED | SDL_RENDERER_PRESENTVSYNC);
    if (renderer == NULL)
        fin_dessin(0, "ERROR RENDERER CREATION", window, renderer);

    SDL_GetWindowSize(window, &largeur, &hauteur);
    pos_x = largeur * 0.66/2;
    pos_y = hauteur * 0.66/2;  

    while (program_on)
        { //Boucle des évènements

        if (SDL_PollEvent(&event))
        { 
            switch (event.type)
            {                         // En fonction de la valeur du type de cet évènement
            case SDL_QUIT:            // Un évènement simple, on a cliqué sur la x de la fenêtre
                program_on = SDL_FALSE; // Il est temps d'arrêter le programme
                break;

            default: // L'évènement défilé ne nous intéresse pas
                break;
            }

        }
        if (pos_x + 100 > largeur || pos_x - 100 < 0)
            i = -i;
        if (pos_y + 150 > hauteur || pos_y - 150 < 0)
            j = -j;

        pos_x += i*7;
        pos_y += j*6;
        draw(renderer, pos_x, pos_y);              // appel de la fonction qui crée l'image
        SDL_RenderPresent(renderer); // affichage
        SDL_Delay(20);             // Pause exprimée en ms
    }

    /* on referme proprement la SDL */
    fin_dessin(1, "Normal ending", window, renderer);
}