/**
 * @file x_fenetre.h
 * @brief fichier d'entete pour la création de forme animée
 */


#include <SDL2/SDL.h>

#ifndef __PAVE_SERPENT_H__
#define __PAVE_SERPENT_H__



void creation_fenetre(void);
void draw(SDL_Renderer *renderer, int pos_x, int pos_y);
void fin_dessin(char ok,            // fin normale : ok = 0 ; anormale ok = 1
             char const *msg,    // message à afficher
             SDL_Window *window, // fenêtre à fermer
             SDL_Renderer *renderer);

#endif