#include <SDL2/SDL.h>
#include <math.h>
#include <stdio.h>
#include <string.h>

/*********************************************************************************************************************/
/*                              Programme d'exemple de création de rendu + dessin                                    */
/*********************************************************************************************************************/





void end_sdl(char ok,            // fin normale : ok = 0 ; anormale ok = 1
             char const *msg,    // message à afficher
             SDL_Window *window, // fenêtre à fermer
             SDL_Renderer *renderer)
{ // renderer à fermer
    char msg_formated[255];
    int l;

    

    if (!ok)
    { // Affichage de ce qui ne va pas
        strncpy(msg_formated, msg, 250);
        l = strlen(msg_formated);
        strcpy(msg_formated + l, " : %s\n");

        SDL_Log(msg_formated, SDL_GetError());
    }

    if (renderer != NULL)
    {                                  // Destruction si nécessaire du renderer
        SDL_DestroyRenderer(renderer); // Attention : on suppose que les NULL sont maintenus !!
        renderer = NULL;
    }
    if (window != NULL)
    {                              // Destruction si nécessaire de la fenêtre
        SDL_DestroyWindow(window); // Attention : on suppose que les NULL sont maintenus !!
        window = NULL;
    }

    SDL_Quit();

    if (!ok)
    { // On quitte si cela ne va pas
        exit(EXIT_FAILURE);
    }
}

void draw(SDL_Renderer *renderer, SDL_Window *window, int *x, int *y, int *i, int *j)
{ 
    int h ,w;

    SDL_GetWindowSize(window, &w, &h);

    if (*x + 100 > w || *x - 100 < 0)
        *i = -(*i);
    if (*y + 100 > h || *y - 100 < 0)
        *j = -(*j);

    *x += (*i)*7;
    *y += (*j)*6;

    SDL_Rect fond;


    fond.x = 0;                 // x haut gauche du rectangle
    fond.y = 0;                 // y haut gauche du rectangle
    fond.w = w;                 // sa largeur (w = width)
    fond.h = h;                 // sa hauteur (h = height)

    SDL_SetRenderDrawColor(renderer,
                           50, 50, 50, // mode Red, Green, Blue (tous dans 0..255)
                           255);
    SDL_RenderFillRect(renderer, &fond);

    for (float angle = 0; angle < 2 * M_PI; angle += M_PI / 4000)
    {
        SDL_SetRenderDrawColor(renderer,
                               (cos(angle * 2 + (*y)/100) + 1) * 255 / 2, // quantité de Rouge
                               (cos(angle * 5 + (*x)/100) + 1) * 255 / 2, //          de vert
                               (cos(angle) + 1) * 255 / 2,     //          de bleu
                               255);                           // opacité = opaque
        SDL_RenderDrawPoint(renderer,
                            *x + 100 * cos(angle),  // coordonnée en x
                            *y + 100 * sin(angle)); //            en y
    }

}

int main(int argc, char **argv)
{
    (void)argc;
    (void)argv;

    SDL_Window *window = NULL;
    SDL_Renderer *renderer = NULL;

    SDL_DisplayMode screen;

    /*********************************************************************************************************************/
    /*                         Initialisation de la SDL  + gestion de l'échec possible                                   */
    if (SDL_Init(SDL_INIT_VIDEO) != 0)
        end_sdl(0, "ERROR SDL INIT", window, renderer);

    SDL_GetCurrentDisplayMode(0, &screen);
    printf("Résolution écran\n\tw : %d\n\th : %d\n",
           screen.w, screen.h);

    /* Création de la fenêtre */
    window = SDL_CreateWindow("Premier dessin",
                              SDL_WINDOWPOS_CENTERED,
                              SDL_WINDOWPOS_CENTERED, screen.w * 0.66,
                              screen.h * 0.66,
                              SDL_WINDOW_OPENGL);
    if (window == NULL)
        end_sdl(0, "ERROR WINDOW CREATION", window, renderer);

    /* Création du renderer */
    renderer = SDL_CreateRenderer(window, -1,
                                  SDL_RENDERER_ACCELERATED | SDL_RENDERER_PRESENTVSYNC);
    if (renderer == NULL)
        end_sdl(0, "ERROR RENDERER CREATION", window, renderer);

    /*********************************************************************************************************************/
    /*                                     On dessine dans le renderer                                                   */
    /*********************************************************************************************************************/
    /*             Cette partie pourrait avantageusement être remplacée par la boucle évènementielle                     */

    SDL_bool program_on = SDL_TRUE; // Booléen pour dire que le programme doit continuer
    SDL_Event event;                // c'est le type IMPORTANT !!
    int x = screen.w * 0.66/2;
    int y = screen.h * 0.66/2;
    int i = 1;
    int j = 1;

    while (program_on)
        { // Voilà la boucle des évènements

        if (SDL_PollEvent(&event))
        { 
            switch (event.type)
            {                         // En fonction de la valeur du type de cet évènement
            case SDL_QUIT:            // Un évènement simple, on a cliqué sur la x de la fenêtre
                program_on = SDL_FALSE; // Il est temps d'arrêter le programme
                break;

            default: // L'évènement défilé ne nous intéresse pas
                break;
            }
        }
    
        draw(renderer, window, &x, &y, &i, &j);              // appel de la fonction qui crée l'image
        SDL_RenderPresent(renderer); // affichage
        SDL_Delay(20);             // Pause exprimée en ms
    }

    /* on referme proprement la SDL */
    end_sdl(1, "Normal ending", window, renderer);
    return EXIT_SUCCESS;
}