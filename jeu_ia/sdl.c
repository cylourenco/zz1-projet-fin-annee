#include "sdl.h"
#include "opti.h"

/**
 * @brief fonction d'initialisation des variables propre à la sdl
 *
 * @param window
 * @param renderer
 * @param texture
 * @param pos_texture
 */
void init_sdl(SDL_Window **window, SDL_Renderer **renderer, SDL_Texture *texture[10], SDL_Rect pos_texture[10])
{

    SDL_DisplayMode screen;

    // initialisation des textures
    for (int i = 0; i < 10; i++)
    {
        texture[i] = NULL;
    }

    if (SDL_Init(SDL_INIT_VIDEO) != 0)
        end_sdl(0, "ERROR SDL INIT", *window, *renderer, texture);

    SDL_GetCurrentDisplayMode(0, &screen);

    /* Création de la fenêtre */
    *window = SDL_CreateWindow("Jeu IA",
                               SDL_WINDOWPOS_CENTERED,
                               SDL_WINDOWPOS_CENTERED, screen.h * 0.7,
                               screen.h * 0.7,
                               SDL_WINDOW_OPENGL);
    if (*window == NULL)
        end_sdl(0, "ERROR WINDOW CREATION", *window, *renderer, texture);

    /* Création du renderer */
    *renderer = SDL_CreateRenderer(*window, -1,
                                   SDL_RENDERER_ACCELERATED | SDL_RENDERER_PRESENTVSYNC);
    if (*renderer == NULL)
        end_sdl(0, "ERROR RENDERER CREATION", *window, *renderer, texture);

    if (TTF_Init() < 0)
        end_sdl(0, "Couldn't initialize SDL TTF", *window, *renderer, texture);

    texture[0] = load_texture_from_image("img/fond1.png", *window, *renderer, texture);
    texture[1] = load_texture_from_image("img/mur.png", *window, *renderer, texture);
    texture[2] = load_texture_from_image("img/chasseur.png", *window, *renderer, texture);
    texture[3] = load_texture_from_image("img/proie.png", *window, *renderer, texture);
    texture[4] = load_texture_from_image("img/etoile.png", *window, *renderer, texture);

    // Initialisation pos fond
    pos_texture[0].x = 0;
    pos_texture[0].y = 0;
    pos_texture[0].w = screen.h * 0.7;
    pos_texture[0].h = screen.h * 0.7;
    printf("x: %d y: %d w: %d h: %d\n", pos_texture[0].x, pos_texture[0].y, pos_texture[0].w, pos_texture[0].h);

    // Initialisation taille autre sprite
    for (int i = 1; i < 5; i++)
    {
        pos_texture[i].w = (screen.h * 0.7) / SIZE;
        pos_texture[i].h = (screen.h * 0.7) / SIZE;
    }
}

/**
 * @brief fonction de fermeture de toutes les variables ouvertes par l'utilisation de la SDL
 *
 * @param ok fin normale : ok = 0 ; anormale ok = 1
 * @param msg message à afficher
 * @param window fenêtre à fermer
 * @param renderer rendu à fermer
 * @param texture rendu à fermer
 */
void end_sdl(int ok, char const *msg, SDL_Window *window, SDL_Renderer *renderer, SDL_Texture *texture[10])
{
    char msg_formated[255];
    int l;

    if (!ok)
    { // Affichage de ce qui ne va pas
        strncpy(msg_formated, msg, 250);
        l = strlen(msg_formated);
        strcpy(msg_formated + l, " : %s\n");

        SDL_Log(msg_formated, SDL_GetError());
    }

    for (int i = 0; i < 10; i++)
    {
        if (texture[i] != NULL)
        {                                   // Destruction si nécessaire de la texture
            SDL_DestroyTexture(texture[i]); // Attention : on suppose que les NULL sont maintenus !!
            texture[i] = NULL;
        }
    }
    if (renderer != NULL)
    {                                  // Destruction si nécessaire du renderer
        SDL_DestroyRenderer(renderer); // Attention : on suppose que les NULL sont maintenus !!
        renderer = NULL;
    }
    if (window != NULL)
    {                              // Destruction si nécessaire de la fenêtre
        SDL_DestroyWindow(window); // Attention : on suppose que les NULL sont maintenus !!
        window = NULL;
    }

    IMG_Quit();
    TTF_Quit();
    SDL_Quit();

    if (!ok)
    { // On quitte si cela ne va pas
        exit(EXIT_FAILURE);
    }
}

/**
 * @brief fonction qui charge une image dans une texture
 *
 * @param file_image_name nom du fichier de l'image
 * @param window fenetre à fermer si le chargement echoue
 * @param renderer rendu permettant le chargement de l'image
 * @return SDL_Texture* l'image charé dans l'adresse d'une sdl_texture
 */
SDL_Texture *load_texture_from_image(char *file_image_name, SDL_Window *window, SDL_Renderer *renderer, SDL_Texture *texture[10])
{
    SDL_Surface *my_image = NULL;   // Variable de passage
    SDL_Texture *my_texture = NULL; // La texture

    my_image = IMG_Load(file_image_name); // Chargement de l'image dans la surface
                                          // image=SDL_LoadBMP(file_image_name); fonction standard de la SDL,
                                          // uniquement possible si l'image est au format bmp */
    if (my_image == NULL)
        end_sdl(0, "Chargement de l'image impossible", window, renderer, texture);

    my_texture = SDL_CreateTextureFromSurface(renderer, my_image); // Chargement de l'image de la surface vers la texture
    SDL_FreeSurface(my_image);                                     // la SDL_Surface ne sert que comme élément transitoire
    if (my_texture == NULL)
        end_sdl(0, "Echec de la transformation de la surface en texture", window, renderer, texture);

    return my_texture;
}

/**
 * @brief affiche l'itération en cours avec de la SDL
 *
 * @param window fenetre d'affichage
 * @param renderer rendu associé à la fenetre
 * @param texture tableau des textures
 * @param pos_texture tableau de coordonnées pour les textures
 * @param carte carte de jeu à afficher
 * @param chasseur liste des chasseurs à afficher
 * @param proies liste des proies à afficher
 * @param etoiles liste des étoiles à afficher
 * @param ancienne_pos_chasseurs tableau des anciennes postions des chasseurs
 * @param ancienne_pos_proies tableau des anciennes postions des proies
 */
void affiche_itere(SDL_Renderer *renderer, SDL_Texture *texture[10], SDL_Rect pos_texture[10],
                   int carte[SIZE][SIZE], pos_t chasseurs[NB_CHASSEURS],
                   pos_t proies[NB_PROIES], pos_t etoiles[NB_ETOILES],
                   pos_t ancienne_pos_chasseurs[NB_CHASSEURS],
                   pos_t ancienne_pos_proies[NB_PROIES])
{

    // FOND
    SDL_RenderCopy(renderer, texture[0], NULL, &pos_texture[0]);
    int i, j;

    // MUR
    for (i = 0; i < SIZE; i++)
    {
        for (j = 0; j < SIZE; j++)
        {
            if (!carte[i][j])
            {
                pos_texture[1].y = pos_texture[1].w * i;
                pos_texture[1].x = pos_texture[1].h * j;
                SDL_RenderCopy(renderer, texture[1], NULL, &pos_texture[1]);
            }
        }
    }

    // ETOILES
    for (i = 0; i < NB_ETOILES; i++)
    {
        pos_texture[4].y = pos_texture[4].w * etoiles[i].x;
        pos_texture[4].x = pos_texture[4].h * etoiles[i].y;
        SDL_RenderCopy(renderer, texture[4], NULL, &pos_texture[4]);
    }

    // CHASSEURS
    for (i = 0; i < NB_CHASSEURS; i++)
    {
        pos_texture[2].y = pos_texture[2].w * chasseurs[i].x;
        pos_texture[2].x = pos_texture[2].h * chasseurs[i].y;
        if (chasseurs[i].y <= ancienne_pos_chasseurs[i].y)
        {
            SDL_RenderCopy(renderer, texture[2], NULL, &pos_texture[2]);
        }
        else
        {
            SDL_Point point = {pos_texture[2].w / 2, pos_texture[2].h / 2};
            SDL_RenderCopyEx(renderer, texture[2], // Préparation de l'affichage
                             NULL,
                             &pos_texture[2], 0.0, &point, SDL_FLIP_HORIZONTAL);
        }
    }

    // PROIES
    for (i = 0; i < NB_PROIES; i++)
    {
        if (verif_superposition(proies[i], chasseurs, NB_CHASSEURS) == -1)
        {
            pos_texture[3].y = pos_texture[3].w * proies[i].x;
            pos_texture[3].x = pos_texture[3].h * proies[i].y;
            SDL_RenderCopy(renderer, texture[3], NULL, &pos_texture[3]);
            if (proies[i].y <= ancienne_pos_proies[i].y)
            {
                SDL_RenderCopy(renderer, texture[3], NULL, &pos_texture[3]);
            }
            else
            {
                SDL_Point point = {pos_texture[3].w / 2, pos_texture[3].h / 2};
                SDL_RenderCopyEx(renderer, texture[3], // Préparation de l'affichage
                                 NULL,
                                 &pos_texture[3], 0.0, &point, SDL_FLIP_HORIZONTAL);
            }
        }
    }

    SDL_RenderPresent(renderer);
}

/**
 * @brief retourne si la souris se trouve dans un des boutons du menu
 *
 * @param mouse_x position de la souris en x
 * @param mouse_y position de la souris en y
 * @param size_restangle_x taille de la largeur du rectangle
 * @param size_restangle_y taille de la hauteur du rectangle
 * @param position_rectangle_x position du rectangle en x
 * @param position_rectangle_y position du rectangle en y
 * @return int entier qui retourne si la position de la souris se trouve dans Jouer (1) ou Quitter (0)
 */
int check_click_menu(int mouse_x, int mouse_y, int size_restangle_x, int size_restangle_y, int position_rectangle_x, int position_rectangle_y)
{
    int zone = 0;

    if ((mouse_x > position_rectangle_x) && (mouse_x < position_rectangle_x + size_restangle_x) && (mouse_y > position_rectangle_y) && (mouse_y < position_rectangle_y + size_restangle_y))
    {
        zone = 1;
    }
    else if ((mouse_x > position_rectangle_x) && (mouse_x < position_rectangle_x + size_restangle_x) && (mouse_y > 3 * position_rectangle_y) && (mouse_y < 3 * position_rectangle_y + size_restangle_y))
    {
        zone = -1;
    }

    return zone;
}

/**
 * @brief affiche le menu sur le renderer
 *
 * @param renderer le rendu
 * @param window la fenêtre
 * @param texture les texture
 * @param pos_texture les positions des textures
 * @param font la police
 * @param mouse_x la position de la souris en x
 * @param mouse_y la position de la souris en y
 * @param size_restangle_x la taille du rectangle en x
 * @param size_restangle_y la taille du rectangle en y
 * @param position_rectangle_x la position du rectangle en x
 * @param position_rectangle_y la position du rectangle en y
 */
void afficher_menu(SDL_Renderer *renderer, SDL_Window *window, SDL_Texture *texture[10], SDL_Rect pos_texture[10], TTF_Font *font, int mouse_x, int mouse_y, int size_restangle_x, int size_restangle_y, int position_rectangle_x, int position_rectangle_y)
{
    int check;
    check = check_click_menu(mouse_x, mouse_y, size_restangle_x, size_restangle_y, position_rectangle_x, position_rectangle_y);

    int color_jouer = 220;
    int color_param = 220;

    if (check == 1)
    {
        color_jouer = 255;
    }
    else if (check == -1)
    {
        color_param = 255;
    }

    SDL_RenderCopy(renderer, texture[0], NULL, &pos_texture[0]);
    draw_rectangle_menu(renderer, window, texture, font, 1, color_jouer);
    draw_rectangle_menu(renderer, window, texture, font, 0, color_param);
    SDL_RenderPresent(renderer);
}

/**
 * @brief dessine un rectangle avec un texte
 *
 * @param renderer le rendu
 * @param window la fenêtre
 * @param texture les textures
 * @param font la police
 * @param etat l'état du rectangle (si 1 pour simulation sinon parametre)
 * @param color_rect la couleur demandé
 */
void draw_rectangle_menu(SDL_Renderer *renderer, SDL_Window *window, SDL_Texture *texture[10], TTF_Font *font, int etat, Uint8 color_rect)
{
    SDL_Rect rectangle = {0};
    SDL_Color color = {255, 100, 0, 255};
    SDL_Surface *text_surface = NULL;

    int w, h;
    char mot[255];
    SDL_Rect pos = {0, 0, 0, 0};

    SDL_SetRenderDrawColor(renderer, color_rect, color_rect, color_rect, 255);

    SDL_GetWindowSize(window, &w, &h);

    int i = 0;

    if (etat == 1)
    {
        rectangle.y = (h - h * 0.2) / 4; // y haut gauche du rectangle
        sprintf(mot, "SIMULATION");
        i = 8;
    }
    else
    {
        rectangle.y = 3 * (h - h * 0.2) / 4; // y haut gauche du rectangle
        sprintf(mot, "QUITTER");
        i = 9;
    }

    rectangle.x = (w - w * 0.5) / 2; // x haut gauche du rectangle
    rectangle.w = w * 0.5;           // sa largeur (w = width)
    rectangle.h = h * 0.2;           // sa hauteur (h = height)

    SDL_RenderFillRect(renderer, &rectangle);
    SDL_SetRenderDrawColor(renderer, 0, 100, 0, 255);
    SDL_RenderDrawRect(renderer, &rectangle);

    text_surface = TTF_RenderText_Blended(font, mot, color);
    texture[i] = SDL_CreateTextureFromSurface(renderer, text_surface); // transfert de la surface à la texture
    SDL_FreeSurface(text_surface);                                     // la texture ne sert plus à rien
    SDL_QueryTexture(texture[i], NULL, NULL, &pos.w, &pos.h);
    // positionne l'écriture dans le rectangle
    pos.x = rectangle.x + rectangle.w / 2 - pos.w / 2;
    pos.y = rectangle.y + rectangle.h / 2 - pos.h / 2;
    SDL_RenderCopy(renderer, texture[i], NULL, &pos); // Ecriture du texte dans le renderer
    SDL_DestroyTexture(texture[i]);
    texture[i] = NULL;
}

/**
 * @brief fonction d'affichage d'une boucle de jeu en SDL
 *
 */
void SDL()
{
    SDL_Window *window = NULL;
    SDL_Renderer *renderer = NULL;
    SDL_Texture *texture[10];
    SDL_Rect pos_texture[10];
    TTF_Font *font = NULL;

    SDL_bool program_on = SDL_TRUE;
    SDL_Event event;

    int carte[SIZE][SIZE], nb_mange_par_chasseur[NB_CHASSEURS], i;
    pos_t chasseurs[NB_CHASSEURS],
        ancienne_pos_chasseurs[NB_CHASSEURS],
        proies[NB_PROIES],
        ancienne_pos_proies[NB_PROIES],
        etoiles[NB_ETOILES];
    RuleTyped rules[NB_RULES];

    const Uint8 *keystates = SDL_GetKeyboardState(NULL); // Récupère l'état des touches du clavier

    init_sdl(&window, &renderer, texture, pos_texture);
    font = TTF_OpenFont("./fonts/Salma.otf", 50); // La police à charger, la taille désirée

    lecture_regles(rules, "regles/nouvelles_regles.txt");
    init_Jeu(carte, chasseurs, proies, etoiles, nb_mange_par_chasseur);

    sauvegarder_ancienne_pos(chasseurs, ancienne_pos_chasseurs, NB_CHASSEURS);
    sauvegarder_ancienne_pos(proies, ancienne_pos_proies, NB_PROIES);
    i = 0;
    /*************************POUR LE MENU************************************/
    // dans quelle zone on est
    int zone = 0,
        w,
        h;

    SDL_GetWindowSize(window, &w, &h);

    // taille des rectangles dans le menu
    int size_restangle_x = w * 0.3,
        size_restangle_y = h * 0.15;

    // emplacement de la souris
    int mouse_x = 0, 
        mouse_y = 0;

    int position_rectangle_x = (w - size_restangle_x) / 2,
        position_rectangle_y = (h - size_restangle_y) / 4;
    /**************************************************************************/

    while (program_on && zone != -1)
    {
        if (SDL_PollEvent(&event))
        {
            keystates = SDL_GetKeyboardState(NULL);
            switch (event.type)
            {                           // En fonction de la valeur du type de cet évènement
            case SDL_QUIT:              // Un évènement simple, on a cliqué sur la x de la fenêtre
                program_on = SDL_FALSE; // Il est temps d'arrêter le programme
                break;
            default:
                break;
            }
        }

        // on est dans le menu
        if (zone == 0)
        {

            if(event.type == SDL_MOUSEMOTION)
            {
                SDL_GetMouseState(&mouse_x, &mouse_y);
            }
            afficher_menu(renderer, window, texture, pos_texture, font, mouse_x, mouse_y, size_restangle_x, size_restangle_y, position_rectangle_x, position_rectangle_y);
            SDL_GetMouseState(&mouse_x, &mouse_y);

            switch (event.type)
            {
            // si le joueur clique on regarde si il clique dans un carré
            case SDL_MOUSEBUTTONDOWN:
                mouse_x = event.button.x;
                mouse_y = event.button.y;
                zone = check_click_menu(mouse_x, mouse_y, size_restangle_x, size_restangle_y, position_rectangle_x, position_rectangle_y);
                break;
            default:
                break;
            }
        }

        // on est dans la simulation
        else if (zone == 1)
        {
            while (i < NB_ITERATIONS && zone == 1)
            {
                if (SDL_PollEvent(&event))
                {
                    keystates = SDL_GetKeyboardState(NULL);
                    switch (event.type)
                    { // En fonction de la valeur du type de cet évènement
                    case SDL_QUIT:
                        zone = -1;
                        break;
                    default:
                        break;
                    }

                    if (keystates[SDL_SCANCODE_ESCAPE])
                        zone = 0;
                }

                affiche_itere(renderer, texture, pos_texture, carte, chasseurs, proies, etoiles,
                              ancienne_pos_chasseurs, ancienne_pos_proies);
                // affiche_jeu(carte, chasseurs, proies, etoiles);
                sauvegarder_ancienne_pos(chasseurs, ancienne_pos_chasseurs, NB_CHASSEURS);
                sauvegarder_ancienne_pos(proies, ancienne_pos_proies, NB_PROIES);
                deplacement_chasseurs(carte, chasseurs, proies, rules);
                deplacement_proies(carte, proies, etoiles, chasseurs);
                proies_mangees(carte, proies, chasseurs, nb_mange_par_chasseur);
                etoiles_mangees(carte, proies, etoiles);
                SDL_Delay(VITESSE_SDL);
                i++;
            }
            if(zone == 1)
            {
                SDL_Delay(2000);
                zone = 0;
            }
        }
    }


    int miam = 0;

    for (i = 0; i < NB_CHASSEURS; i++)
        miam += nb_mange_par_chasseur[i];

    printf("nombre de morts totals : %d\n", miam);

    end_sdl(1, "Normal ending", window, renderer, texture);
}