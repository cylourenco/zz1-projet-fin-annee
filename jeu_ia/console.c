#include "console.h"

/**
 * @brief création et initialisation d'une matrice de n par n
 *
 * @param n taille de la matrice
 * @return int** la matrice initialisé
 */
int **init_matrice(int n)
{
    int **matrice = (int **)malloc(n * sizeof(int *));
    if (matrice != NULL)
    {
        for (int i = 0; i < n; i++)
        {
            matrice[i] = (int *)malloc(n * sizeof(int));
            if (matrice[i] == NULL)
            {
                for (int j = 0; j < i; j++)
                {
                    free(matrice[j]);
                }
                free(matrice);
                matrice = NULL;
                exit(-1);
            }
        }
    }
    if (matrice != NULL)
    {
        for (int i = 0; i < n; i++)
        {
            for (int j = 0; j < n; j++)
            {
                matrice[i][j] = 0;
            }
        }
    }

    return matrice;
}

/**
 * @brief permet de libérer une matrice d'entiers
 * @param matrice matrice d'entier à libérer
 * @param taille entier qui contient la taille de la matrice
 */
void liberation_matrice(int **matrice, int taille)
{
    int i;
    for (i = 0; i < taille; i++)
    {
        free(matrice[i]);
    }
    free(matrice);
    matrice = NULL;
}

/**
 * @brief affiche la matrice de dimension dim
 *
 * @param matrice matrice à afficher
 * @param dim dimension de la matrice
 */
void affiche_matrice(int matrice[SIZE][SIZE], int dim)
{
    puts("");
    for (int i = 0; i < dim; i++)
    {
        for (int j = 0; j < dim; j++)
        {
            printf("%d ", matrice[i][j]);
        }
        puts("");
    }
}

/**
 * @brief choisit la prochaine direction d'une proie
 *
 * @param carte carte du jeu
 * @param etoile position de l'étoile la plus proche
 * @param proie position de la proie à déplacer
 * @param chasseurs la liste des chasseurs sur la map
 * @return direction8_t
 */
direction8_t prochaine_direction_proie(int carte[SIZE][SIZE], pos_t etoile, pos_t proie, pos_t chasseurs[NB_CHASSEURS])
{
    direction8_t direction;

    // printf("proie x:%d y:%d  E_proche : x:%d y:%d \n", proie.x,proie.y,etoile.x, etoile.y);

    int x = proie.x,
        y = proie.y;

    // on choisit une direction en fonction de la distance en x et en y entre les 2 points
    if (abs(etoile.x - proie.x) > abs(etoile.y - proie.y))
    {
        if (etoile.x - proie.x < 0)
        {
            direction = N;
            x--;
        }
        else
        {
            direction = S;
            x++;
        }
    }
    else
    {
        if (etoile.y - proie.y < 0)
        {
            direction = O;
            y--;
        }
        else
        {
            direction = E;
            y++;
        }
    }

    pos_t pos;
    pos.x = x;
    pos.y = y;

    // tant que la direction choisit est un mur alors on choisit une autre direction de manière arbitraire
    while(carte[x][y] == 0 || (verif_superposition(pos, chasseurs, NB_CHASSEURS) != -1 && !proie_bloquee(carte, chasseurs, proie) && PROIE_INTELLIGENTE))
    {
        x = proie.x;
        y = proie.y;
        direction = ((rand() % 101) * 2) % NB_DIRECTION;

        if (direction == N)
        {
            x--;
        }
        else if (direction == S)
        {
            x++;
        }
        else if (direction == O)
        {
            y--;
        }
        else
        {
            y++;
        }

        pos.x = x;
        pos.y = y;
    }

    return direction;
}

/**
 * @brief savoir si la proie est bloquée ou non
 * 
 * @param carte la carte
 * @param chasseurs la liste des positions des chasseurs
 * @param proie la position de la proie
 * @return int 0 si la proie n'est pas bloquée, 1 sinon
 */
int proie_bloquee(int carte[SIZE][SIZE], pos_t chasseurs[NB_CHASSEURS], pos_t proie)
{
    int bloque = 1;
    pos_t pos;

    for(double i = 0; i < 2 * M_PI; i += M_PI / 2)
    {
        pos.x = proie.x + (int)cos(i);
        pos.y = proie.y + (int)sin(i);
        // printf("x : %d\ny : %d\n", proie.x, proie.y);
        // printf("i: %f\n", i);
        // printf("cos : %d\nsin : %d\n", (int)cos(i), (int)sin(i));
        // printf("posx : %d\nposy : %d\n\n", pos.x, pos.y);
        if(verif_superposition(pos, chasseurs, NB_CHASSEURS) == -1 && carte[pos.x][pos.y] != 0)
        {
            bloque = 0;
            break;
        }
    }
    return bloque;
}

/**
 * @brief choisit la prochaine direction d'un chasseur
 *
 * @param carte carte du jeu
 * @param chasseur_proche position du chasseur le plus proche
 * @param proie_proche postion de la proie la plus proche
 * @param chasseur position du chasseur à déplacer
 * @param rules la liste des règles que suit les chasseurs
 * @return direction8_t
 */
direction8_t prochaine_direction_chasseur(int carte[SIZE][SIZE], pos_t chasseur_proche, pos_t proie_proche, pos_t chasseur, RuleTyped rules[NB_RULES])
{
    int regle_correspondante_indice[NB_RULES];
    int nb_regle_correspondante_indice = 0;
    RuleTyped current;
    current.n = carte[chasseur.x - 1][chasseur.y];
    current.o = carte[chasseur.x][chasseur.y - 1];
    current.s = carte[chasseur.x + 1][chasseur.y];
    current.e = carte[chasseur.x][chasseur.y + 1];

    current.distance_chasseur = dist(chasseur_proche, chasseur);
    current.distance_proie = dist(proie_proche, chasseur);

    current.direction_chasseur = dir(chasseur, chasseur_proche);
    current.direction_proie = dir(chasseur, proie_proche);

    // printf("chasseur x:%d y:%d  C_proche : %d P_proche : %d\n", chasseur.x,chasseur.y,current.direction_chasseur,current.direction_proie);

    regles_correspondantes(rules, current, regle_correspondante_indice, &nb_regle_correspondante_indice);

    return tirage_direction(rules, regle_correspondante_indice, nb_regle_correspondante_indice);
}

/**
 * @brief tire une règle parmi les règle possible proportionnellement à leur probabilité
 *
 * @param rules tableau des règles
 * @param regle_correspondante_indice tableau contenant les indice des règles correspondantes
 * @param nb_regle_correspondante_indice nombre de règle correspondantes
 * @return direction8_t
 */
direction8_t tirage_direction(RuleTyped rules[NB_RULES], int regle_correspondante_indice[NB_RULES], int nb_regle_correspondante_indice)
{
    if (nb_regle_correspondante_indice)
    {

        float total = 0.0;
        int i;

        // somme des proba
        for (int i = 0; i < nb_regle_correspondante_indice; i++)
        {
            total += powf(rules[regle_correspondante_indice[i]].priorite, PUI);
        }

        // tableau des proba croissante cumulé
        float proba_regle[NB_RULES];
        proba_regle[0] = powf(rules[regle_correspondante_indice[0]].priorite, PUI) / total;
        for (i = 1; i < nb_regle_correspondante_indice; i++)
        {
            proba_regle[i] = proba_regle[i - 1] + powf(rules[regle_correspondante_indice[i]].priorite, PUI) / total;
        }

        // tirage de la probabilité
        float randomValue = (float)rand() / RAND_MAX;
        int indice = 0;

        for (i = 0; i < nb_regle_correspondante_indice; i++)
        {
            if (randomValue < proba_regle[i])
            {
                indice = regle_correspondante_indice[i];
                break;
            }
        }

        // printf("proba 0 : %f proba n : %f et indice = %d donc dir : %d proba = %f\n",proba_regle[0], proba_regle[nb_regle_correspondante_indice-1], indice, rules[indice].resultat, randomValue);

        return rules[indice].resultat;
    }
    else
        return ((rand() % 101) * 2) % NB_DIRECTION;
}

/**
 * @brief permet de charger une carte
 * @param matrice matrice d'entier qui va contenir la map
 */
void charger_carte(int matrice[SIZE][SIZE])
{
    char nom_fichier[255] = "cartes/carte";
    FILE *flot;
    char tmp[255];
    sprintf(tmp, "%d", CARTE_A_CHARGER);
    strcat(nom_fichier, tmp);
    strcat(nom_fichier, ".txt");
    flot = fopen(nom_fichier, "r");
    if (flot != NULL)
    {
        for (int i = 1; i < SIZE - 1; i++)
        {
            for (int j = 1; j < SIZE - 1; j++)
            {
                if(!fscanf(flot, "%d", &matrice[i][j]))
                    puts("erreur de lecture de carte");
            }
        }
        for (int i = 0; i < SIZE; i++)
        {
            matrice[0][i] = 0;
            matrice[i][0] = 0;
            matrice[SIZE - 1][i] = 0;
            matrice[i][SIZE - 1] = 0;
        }
    }
    fclose(flot);
}

/**
 * @brief effectue le déplacement d'un entité sur la carte si possible
 *
 * @param carte carte du jeu
 * @param perso position du personnage sur la carte
 * @param direction la direction où va le perso
 * @return int renvoie 1 si déplacement réussi 0 sinon
 */
int deplacement(int carte[SIZE][SIZE], pos_t *perso, direction8_t direction)
{
    int reussite = 1;

    if (direction == N && carte[perso->x - 1][perso->y] != 0)
    {
        perso->x--;
    }
    else if (direction == S && carte[perso->x + 1][perso->y] != 0)
    {
        perso->x++;
    }
    else if (direction == O && carte[perso->x][perso->y - 1] != 0)
    {
        perso->y--;
    }
    else if (direction == E && carte[perso->x][perso->y + 1] != 0)
    {
        perso->y++;
    }
    else
    {
        reussite = 0;
    }

    return reussite;
}

/**
 * @brief effectue le déplacement des proies sur la carte si possible
 *
 * @param carte carte du jeu
 * @param proie position des proies sur la carte
 * @param etoiles position des étoiles sur la carte
 * @param chasseurs position des chasseurs sur la carte
 */
void deplacement_proies(int carte[SIZE][SIZE], pos_t proies[NB_PROIES], pos_t etoiles[NB_ETOILES], pos_t chasseurs[NB_CHASSEURS])
{

    direction8_t dir = N;
    pos_t nearest_star = etoiles[0];

    for (int i = 0; i < NB_PROIES; i++)
    {
        if (verif_superposition(proies[i], chasseurs, NB_CHASSEURS) == -1)
        {
            nearest_star = plus_proche(proies[i], etoiles, NB_ETOILES);
            dir = prochaine_direction_proie(carte, nearest_star, proies[i], chasseurs);
            deplacement(carte, &proies[i], dir);
        }
    }
}

/**
 * @brief renvoie la position de l'élement le plus proche de e1 parmis L
 *
 * @param e1 position de l'élément
 * @param L tableau de position des autres éléments
 * @param nb_elements taille tableau L
 * @return pos_t la position de l'objet L la plus proche
 */
pos_t plus_proche(pos_t e1, pos_t *L, int nb_element)
{
    pos_t nearest = L[0];
    int i;
    float distance = distance_euclidienne(e1, L[0]),
          tmp;

    for (i = 1; i < nb_element; i++)
    {
        tmp = distance_euclidienne(e1, L[i]);
        if (distance > tmp && tmp != 0.0)
        {
            distance = tmp;
            nearest = L[i];
        }
    }
    return nearest;
}

/**
 * @brief calcule la distance euclidienne entre deux positions
 *
 * @param p1 position 1
 * @param p2 position 2
 * @return float distance euclidienne
 */
float distance_euclidienne(pos_t p1, pos_t p2)
{
    return sqrt(powf(p1.x - p2.x, 2) + powf(p1.y - p2.y, 2));
}

/**
 * @brief verifie si un objet se trouve au meme endroit que l'un de la liste passé en paramètre
 *
 * @param objet1 objet étudié
 * @param liste_objet liste d'objet que l'on verifie
 * @param nb_elements nombre d'éléments de la liste
 * @return int -1 si non trouvé sinon  indice de l'élément dans le tableau
 */
int verif_superposition(pos_t objet1, pos_t *liste_objet, int nb_elements)
{
    int res = -1;
    for (int j = 0; j < nb_elements; j++)
    {
        if (objet1.x == liste_objet[j].x && objet1.y == liste_objet[j].y)
        {
            res = j;
            break;
        }
    }
    return res;
}

/**
 * @brief effectue le déplacement des chasseurs sur la carte
 *
 * @param carte carte du jeu
 * @param chasseurs position des chasseurs
 * @param proies position des proies
 * @param rules les regles que le chasseur doit suivre
 */
void deplacement_chasseurs(int carte[SIZE][SIZE], pos_t chasseurs[NB_CHASSEURS], pos_t proies[NB_PROIES], RuleTyped rules[NB_RULES])
{
    direction8_t dir = N;
    pos_t nearest_proie = proies[0];
    pos_t nearest_chasseur = chasseurs[0];

    for (int i = 0; i < NB_CHASSEURS; i++)
    {
        nearest_proie = plus_proche(chasseurs[i], proies, NB_PROIES);
        nearest_chasseur = plus_proche(chasseurs[i], chasseurs, NB_CHASSEURS);
        dir = prochaine_direction_chasseur(carte, nearest_chasseur, nearest_proie, chasseurs[i], rules);
        // printf("déplacement final : %d\n",dir);
        deplacement(carte, &chasseurs[i], dir);
    }
}

/**
 * @brief initialise la position des enemis sur la carte
 *
 * @param carte la carte du jeu
 * @param chassseurs la liste des position des chasseurs
 */
void init_chasseurs(int carte[SIZE][SIZE], pos_t chasseurs[NB_CHASSEURS])
{
    int i;

    for (i = 0; i < NB_CHASSEURS; i++)
    {
        chasseurs[i] = trouver_position_libre(carte);
    }
}

/**
 * @brief initialise la position des proies sur la carte
 *
 * @param carte la carte du jeu
 * @param proies la liste des position des proies
 */
void init_proies(int carte[SIZE][SIZE], pos_t proies[NB_PROIES])
{
    for (int i = 0; i < NB_PROIES; i++)
    {
        proies[i] = trouver_position_libre(carte);
    }
}

/**
 * @brief initialise la position des étoiles sur la carte
 *
 * @param carte la carte du jeu
 * @param etoiles la liste des position des etoiles
 */
void init_etoiles(int carte[SIZE][SIZE], pos_t etoiles[NB_ETOILES])
{
    for (int i = 0; i < NB_ETOILES; i++)
    {
        etoiles[i] = trouver_position_libre(carte);
    }
}

/**
 * @brief affichage de debug du jeu à un moment donné
 * @param carte matrice d'entier représentant la carte
 * @param chasseurs tableau des positions des chasseurs
 * @param proies tableau des positions des proies
 * @param etoiles tableau des positions des etoiles
 */
void affiche_jeu(int carte[SIZE][SIZE], pos_t chasseurs[NB_CHASSEURS], pos_t proies[NB_PROIES], pos_t etoiles[NB_ETOILES])
{
    int chasseur, proie, etoile;
    puts("");
    for (int i = 0; i < SIZE; i++)
    {
        for (int j = 0; j < SIZE; j++)
        {
            proie = 0;
            chasseur = 0;
            etoile = 0;
            for (int k = 0; k < NB_CHASSEURS; k++)
            {
                if (chasseurs[k].x == i && chasseurs[k].y == j)
                {
                    chasseur++;
                }
            }
            for (int k = 0; k < NB_PROIES; k++)
            {
                if (proies[k].x == i && proies[k].y == j)
                {
                    proie++;
                }
            }
            for (int k = 0; k < NB_ETOILES; k++)
            {
                if (etoiles[k].x == i && etoiles[k].y == j)
                {
                    etoile++;
                }
            }
            /*
            if(chasseur == 1){
                printf(ANSI_COLOR_YELLOW "C " ANSI_COLOR_RESET);
            }
            else{
                if(proie == 1){
                     printf(ANSI_COLOR_GREEN "P " ANSI_COLOR_RESET);
                }
                else{
                    if(etoile == 1){
                        printf(ANSI_COLOR_SKYBLUE "E " ANSI_COLOR_RESET);
                    }
                    else{
                        printf("%d ", carte[i][j]);
                    }
                }
            }*/
            for (int k = 0; k < chasseur; k++)
            {
                printf(ANSI_COLOR_YELLOW "C" ANSI_COLOR_RESET);
            }
            for (int k = 0; k < proie; k++)
            {
                printf(ANSI_COLOR_GREEN "P" ANSI_COLOR_RESET);
            }
            for (int k = 0; k < etoile; k++)
            {
                printf(ANSI_COLOR_SKYBLUE "E" ANSI_COLOR_RESET);
            }
            if (!etoile && !chasseur && !proie)
            {
                if (carte[i][j] == 1)
                {
                    printf(" ");
                }
                else
                {
                    printf("■");
                }
            }
            printf("\t");
        }
        puts("");
        puts("");
    }
}

/**
 * @brief trouver une case vide dans la carte de manière aléatoire
 * @param carte matrice d'entiers représentant la carte du jeu
 * @return la position libre trouvée
 */
pos_t trouver_position_libre(int carte[SIZE][SIZE])
{
    pos_t pos;
    pos.x = (rand() % (SIZE - 2)) + 1;
    pos.y = (rand() % (SIZE - 2)) + 1;
    while (carte[pos.x][pos.y] == 0)
    {
        pos.x = (rand() % (SIZE - 2)) + 1;
        pos.y = (rand() % (SIZE - 2)) + 1;
    }
    return pos;
}

/**
 * @brief check si une proie est mangée par un chasseur
 *
 * @param carte carte de jeu
 * @param proies liste des postions des proies
 * @param chasseurs liste des positions des chasseurs
 * @param nb_mange_par_chasseur liste du nombre de proies mangés par chaque chasseur
 */
void proies_mangees(int carte[SIZE][SIZE], pos_t proies[NB_PROIES], pos_t chasseurs[NB_CHASSEURS], int nb_mange_par_chasseur[NB_CHASSEURS])
{
    int i, chasseur_indice;

    for (i = 0; i < NB_PROIES; i++)
    {
        chasseur_indice = verif_superposition(proies[i], chasseurs, NB_CHASSEURS);
        if (chasseur_indice != -1)
        {
            proies[i] = trouver_position_libre(carte);
            nb_mange_par_chasseur[chasseur_indice]++;
        }
    }
}

/**
 * @brief check si une proie est mangée par un chasseur
 *
 * @param carte carte de jeu
 * @param proies liste des postions des proies
 * @param etoiles liste des postions des étoiles
 */
void etoiles_mangees(int carte[SIZE][SIZE], pos_t proies[NB_PROIES], pos_t etoiles[NB_ETOILES])
{
    int i;
    for (i = 0; i < NB_ETOILES; i++)
        if (verif_superposition(etoiles[i], proies, NB_PROIES) != -1)
        {
            etoiles[i] = trouver_position_libre(carte);
        }
}

/**
 * @brief regarde quelles règles correspond le plus à la situation actuelle
 *
 * @param rules liste des règles
 * @param current la situation courante
 * @param tabeau_indice la liste des indices des règles qui correspondent
 * @param nb_regles_correspondantes le nombre de règles correspond le plus à la situation actuelle
 */
void regles_correspondantes(RuleTyped *rules, RuleTyped current, int regle_correspondante_indice[NB_RULES], int *nb_regle_correspondante_indice)
{
    int i;
    *nb_regle_correspondante_indice = 0;

    for (i = 0; i < NB_RULES; i++)
    {
        if ((rules[i].n == current.n || rules[i].n == -1) &&
            (rules[i].s == current.s || rules[i].s == -1) &&
            (rules[i].o == current.o || rules[i].o == -1) &&
            (rules[i].e == current.e || rules[i].e == -1) &&
            (rules[i].direction_chasseur == current.direction_chasseur || (rules[i].direction_chasseur == -1)) &&
            (rules[i].distance_chasseur == current.distance_chasseur || rules[i].distance_chasseur == -1) &&
            (rules[i].direction_proie == current.direction_proie || rules[i].direction_proie == -1) &&
            (rules[i].distance_proie == current.distance_proie || rules[i].distance_proie == -1))
        {
            regle_correspondante_indice[*nb_regle_correspondante_indice] = i;
            *nb_regle_correspondante_indice += 1;
        }
    }
}

/**
 * @brief fonction qui initialise les différents acteurs d'une partie
 *
 * @param carte carte à initialiser
 * @param chasseurs tableau des positions des chasseurs à initaliser
 * @param proies tableau des positions des proies à initaliser
 * @param etoiles tableau des positions des etoiles à initaliser
 * @param nb_mange_par_chasseur tableau du nombre de proies mangés par chaque chasseur
 */
void init_Jeu(int carte[SIZE][SIZE], pos_t chasseurs[NB_CHASSEURS], pos_t proies[NB_PROIES], pos_t etoiles[NB_ETOILES], int nb_mange_par_chasseur[NB_CHASSEURS])
{
    charger_carte(carte);
    init_chasseurs(carte, chasseurs);
    init_proies(carte, proies);
    init_etoiles(carte, etoiles);
    for (int i = 0; i < NB_CHASSEURS; i++)
    {
        nb_mange_par_chasseur[i] = 0;
    }
}

/**
 * @brief lance la simulation du jeu pour le nombre de proie chasseur et etoile choisi en préprocesseur
 *
 * @param rules regles
 */
int simulation(void *rules_n)
{
    RuleTyped *rules = (RuleTyped *)rules_n;
    int carte[SIZE][SIZE], nb_mange_par_chasseur[NB_CHASSEURS], i;
    pos_t chasseurs[NB_CHASSEURS], proies[NB_PROIES], etoiles[NB_ETOILES];
    for (int i = 0; i < SIZE; i++)
    {
        for (int j = 0; j < SIZE; j++)
        {
            carte[i][j] = 0;
        }
    }
    init_Jeu(carte, chasseurs, proies, etoiles, nb_mange_par_chasseur);

    for (i = 0; i < NB_ITERATIONS; i++)
    {
        // affiche_jeu(carte, chasseurs, proies, etoiles);
        // printf("----------------------------------------------------------------------------------------------------------\n");
        deplacement_chasseurs(carte, chasseurs, proies, rules);
        deplacement_proies(carte, proies, etoiles, chasseurs);
        proies_mangees(carte, proies, chasseurs, nb_mange_par_chasseur);
        etoiles_mangees(carte, proies, etoiles);
    }

    int miam = 0;

    for (int i = 0; i < NB_CHASSEURS; i++)
        miam += nb_mange_par_chasseur[i];

    return miam;
}

/**
 * @brief fonction de qualification de la distance entre deux éléments d'un plan orthonormé
 *
 * @param e1 premier élément repère
 * @param e2 deuxieme élément à positionner par rapport au premier
 * @return info_distance_t
 */
info_distance_t dist(pos_t e1, pos_t e2)
{
    info_distance_t info = LOIN;
    float distance = distance_euclidienne(e1, e2);
    if (distance < SEUIL_DISTANCE)
    {
        info = PROCHE;
    }
    return info;
}

/**
 * @brief fonction qui renvoie la position relative de e2 par rapport à e1
 *
 * @param e1 premier élément repère
 * @param e2 deuxieme élément à positionner par rapport au premier
 * @return direction8_t
 */
direction8_t dir(pos_t e1, pos_t e2)
{
    direction8_t direction;
    pos_t vect = {e2.x - e1.x, e2.y - e1.y};
    if (vect.x == 0)
    {
        if (vect.y > 0)
            direction = E;
        else
            direction = O;
    }
    else
    {
        float rapport = (float)vect.y / vect.x;
        if (vect.x > 0)
        {
            if (rapport >= tanf(M_PI / 4 + M_PI / 8))
                direction = S;
            else if (rapport >= tanf(M_PI / 8))
                direction = SE;
            else if (rapport >= -tanf(M_PI / 8))
                direction = E;
            else if (rapport >= -tanf(M_PI / 4 + M_PI / 8))
                direction = NE;
            else
                direction = N;
        }
        else
        {
            if (rapport <= -tanf(M_PI / 4 + M_PI / 8))
                direction = S;
            else if (rapport <= -tanf(M_PI / 8))
                direction = SO;
            else if (rapport <= tanf(M_PI / 8))
                direction = O;
            else if (rapport <= tanf(M_PI / 4 + M_PI / 8))
                direction = NO;
            else
                direction = N;
        }
    }

    return direction;
}

/**
 * @brief lecture d'un tableau de règles à partir d'un fichier
 * @param tableau tableau de règles à remplir
 * @param nom_fichier nom du fichier où l'on doit lire les règles
 * @return la moyenne correspondant aux règles lues
 */
int lecture_regles(RuleTyped table_regles[NB_RULES], char *nom_fichier)
{
    FILE *flot;
    int moyenne = 0, code_erreur;
    flot = fopen(nom_fichier, "r");
    if (flot != NULL)
    {
        code_erreur = fscanf(flot, "%d", &moyenne);
        for (int i = 0; i < NB_RULES; i++)
        {
            code_erreur = 0;
            code_erreur += fscanf(flot, "%d", &table_regles[i].n);
            code_erreur += fscanf(flot, "%d", &table_regles[i].o);
            code_erreur += fscanf(flot, "%d", &table_regles[i].s);
            code_erreur += fscanf(flot, "%d", &table_regles[i].e);
            code_erreur += fscanf(flot, "%d", &table_regles[i].direction_proie);
            code_erreur += fscanf(flot, "%d", &table_regles[i].distance_proie);
            code_erreur += fscanf(flot, "%d", &table_regles[i].direction_chasseur);
            code_erreur += fscanf(flot, "%d", &table_regles[i].distance_chasseur);
            code_erreur += fscanf(flot, "%d", &table_regles[i].resultat);
            code_erreur += fscanf(flot, "%d", &table_regles[i].priorite);
            if (code_erreur != 10)
                printf("erreur de lecture de fichier\n");
        }
    }
    fclose(flot);
    return moyenne;
}

/**
 * @brief écriture d'un tableau de règles dans un fichier
 * @param table_regles tableau de règles à sauvegarder
 * @param nom_fichier nom du fichier où l'on doit écrire les règles
 * @param moyenne entier correspondant à la moyenne de la règle à écrire
 */
void ecriture_regles(RuleTyped table_regles[NB_RULES], int moyenne, char *nom_fichier)
{
    FILE *flot;
    flot = fopen(nom_fichier, "w");
    if (flot != NULL)
    {
        fprintf(flot, "%d\n", moyenne);
        for (int i = 0; i < NB_RULES; i++)
        {
            fprintf(flot, "%d\t", table_regles[i].n);
            fprintf(flot, "%d\t", table_regles[i].o);
            fprintf(flot, "%d\t", table_regles[i].s);
            fprintf(flot, "%d\t", table_regles[i].e);
            fprintf(flot, "%d\t", table_regles[i].direction_proie);
            fprintf(flot, "%d\t", table_regles[i].distance_proie);
            fprintf(flot, "%d\t", table_regles[i].direction_chasseur);
            fprintf(flot, "%d\t", table_regles[i].distance_chasseur);
            fprintf(flot, "%d\t", table_regles[i].resultat);
            fprintf(flot, "%d\t", table_regles[i].priorite);
            fprintf(flot, "\n");
        }
    }
    fclose(flot);
}

/**
 * @brief affiche le tableau de règles
 * @param table_regles tableau contenant des règles
 */
void affiche_regles(RuleTyped table_regles[NB_RULES])
{
    for (int i = 0; i < NB_RULES; i++)
    {
        printf("%d\t%d\t%d\t%d\t%d\t%d\t%d\t%d\t%d\t%d\n",
               table_regles[i].n,
               table_regles[i].o,
               table_regles[i].s,
               table_regles[i].e,
               table_regles[i].direction_proie,
               table_regles[i].distance_proie,
               table_regles[i].direction_chasseur,
               table_regles[i].distance_chasseur,
               table_regles[i].resultat,
               table_regles[i].priorite);
    }
}

/**
 * @brief sauvegarde l'ancienne position
 * @param pos_acutelle tableau des postions actuelles
 * @param ancienne_pos tableau des anciennes positions
 * @param taille taille du tableau
 */
void sauvegarder_ancienne_pos(pos_t *pos_actuelle, pos_t *ancienne_pos, int taille)
{
    for (int i = 0; i < taille; i++)
    {
        ancienne_pos[i] = pos_actuelle[i];
    }
}

/**
 * @brief compare deux règles et sauvegarde la meilleure dans regles/meilleur_regles.txt
 * @param moyenne_anciennes_regles moyenne des anciennes regles
 * @param nouvelles_regles tableau des nouvelles regles
 * @param moyenne_nouvelles_regles moyenne des nouvelles regles
 *
 */
void ecrire_meilleure_regle(int moyenne_anciennes_regles, RuleTyped nouvelles_regles[NB_RULES], int moyenne_nouvelles_regles)
{
    printf("Moyenne avant : %d \n", moyenne_anciennes_regles);
    printf("Moyenne nouvelle %d \n", moyenne_nouvelles_regles);
    if (moyenne_anciennes_regles < moyenne_nouvelles_regles)
    {
        ecriture_regles(nouvelles_regles, moyenne_nouvelles_regles, "regles/meilleures_regles.txt");
    }
}

void test_console(void)
{
}
