#include "opti.h"
#include "console.h"

/**
 * @brief optimise les règles et les sauvegardes dans un fichier
 * @param table_regles tableau des règles à optimiser
 */
void optimisation_regles(RuleTyped table_regles[NB_RULES])
{
    float p;
    int tableau[NB_RULES * NB_ATTRIBUTS_REGLE];
    melanger_entiers(tableau, NB_RULES * NB_ATTRIBUTS_REGLE);
    for (int i = 0; i < NB_RULES * NB_ATTRIBUTS_REGLE; i++)
    {
        p = ((float)rand() / RAND_MAX);
        if (p < 0.05)
            printf("param n°%d : %d en cours de traitement\n", i, tableau[i]);

        modif_parametre(table_regles, tableau[i], p);

        // if (p < 0.05)
        //     printf("param n°%d : %d traité\n", i, tableau[i]);
    }
    // printf("Les Règles finales : \n");
    // affiche_regles(table_regles);
}

/**
 * @brief optimisation de deux variables et les sauvegardes dans un fichier
 * @param table_regles tableau des règles à optimiser
 */
void optimisation_double_regles(RuleTyped table_regles[NB_RULES])
{
    int regle1, regle2, param1, param2, univers1, univers2;
    int * var1, * var2, champs1, champs2;
    param1 = rand()%(NB_RULES * NB_ATTRIBUTS_REGLE);
    param2 = rand()%(NB_RULES * NB_ATTRIBUTS_REGLE);

    //param1 = rand()%(NB_RULES) * 10 + 4;
    //param2 = param1 + 4;
    
    while(param1 == param2)
        param2 = rand()%(NB_RULES * NB_ATTRIBUTS_REGLE);    
    
    regle1 = param1 / 10;
    champs1 = param1 - 10 * regle1;

    regle2 = param2 / 10;
    champs2 = param2 - 10 * regle2;    

    
    univers1 = calcul_param_champs(table_regles, regle1, champs1, &var1);
    univers2 = calcul_param_champs(table_regles, regle2, champs2, &var2);

    int ** moyennes = init_matrice_opti(univers1, univers2);


    printf("indice : %d regle : %d champs : %d univers : %d\n",param1,regle1,champs1,univers1);
    printf("indice : %d regle : %d champs : %d univers : %d\n",param2,regle2,champs2,univers2);

    printf("anciennes valeurs : %d %d \n",*var1, *var2);
    opti_threading_2_param(table_regles, moyennes, univers1, univers2, var1, var2);

}

/**
 * @brief crée et mélange un tableau d'entier
 * @param tableau tableau d'entier passé en paramètre
 * @param taille taille du tableau à créer
 */
void melanger_entiers(int *tableau, int taille)
{
    for (int i = 0; i < taille; ++i)
    {
        tableau[i] = i;
    }
    for (int i = taille - 1; i > 0; --i)
    {
        int j = rand() % (i + 1);

        int temp = tableau[i];
        tableau[i] = tableau[j];
        tableau[j] = temp;
    }
}

/**
 * @brief fonction qui creer une liste de regle aleatoire
 *
 * @param table_regles tableau où les regles sont stockées
 */
void creation_regle_aleatoire(RuleTyped table_regles[NB_RULES])
{
    int i;
    float proba = 0.4;
    for (i = 0; i < NB_RULES; i++)
    {
        table_regles[i].n = -1;
        table_regles[i].o = -1;
        table_regles[i].s = -1;
        table_regles[i].e = -1;
        maj_proba(&table_regles[i].n, proba, 3);
        maj_proba(&table_regles[i].o, proba, 3);
        maj_proba(&table_regles[i].s, proba, 3);
        maj_proba(&table_regles[i].e, proba, 3);

        // Ensuite les directions du chasseur et de la proie proches
        table_regles[i].direction_chasseur = -1;
        table_regles[i].direction_proie = -1;
        maj_proba(&table_regles[i].direction_chasseur, proba, 9);
        maj_proba(&table_regles[i].direction_proie, proba, 9);

        // Après, on fait les distances
        table_regles[i].distance_chasseur = -1;
        table_regles[i].distance_proie = -1;
        maj_proba(&table_regles[i].distance_chasseur, proba, 3);
        maj_proba(&table_regles[i].distance_proie, proba, 3);

        // Enfin la priorité et le resultat
        table_regles[i].priorite = 1 + rand() % 5;
        table_regles[i].resultat = ((rand() % 101) * 2) % NB_DIRECTION;
    }
}

/**
 * @brief change une valeur selon son univers avec une probabilité donnée
 *
 * @param valeur adresse de la valeur à changer
 * @param proba probabilité de changement
 * @param taille_univers taille de l'univers de la valeur
 */
void maj_proba(int *valeur, float proba, int taille_univers)
{
    float tirage = (float)rand() / RAND_MAX;
    if (tirage < proba)
        *valeur += 1 + rand() % (taille_univers - 1);
}

/**
 * @brief fonction qui evalue toutes les possibilité pour un champs donné et garde la meilleure
 *
 * @param rules tableau des regles
 * @param position poisition du champs à optimiser
 * @param p probabilité de l'affichage de débug
 */
void modif_parametre(RuleTyped rules[NB_RULES], int position, float p)
{
    int regle = position / 10;
    int champs = position - 10 * regle;


    switch (champs){
        case 0:
            opti_threading(rules, &(rules[regle].n), 3, p);
            break;
        case 1:
            opti_threading(rules, &(rules[regle].o), 3, p);
            break;
        case 2:
            opti_threading(rules, &(rules[regle].s), 3, p);
            break;
        case 3:
            opti_threading(rules, &(rules[regle].e), 3, p);
            break;
        case 4:
            opti_threading(rules, &(rules[regle].direction_proie), 9, p);
            break;
        case 5:
            opti_threading(rules, &(rules[regle].distance_proie), 3, p);
            break;
        case 6:
            opti_threading(rules, &(rules[regle].direction_chasseur), 9, p);
            break;
        case 7:
            opti_threading(rules, &(rules[regle].distance_chasseur), 3, p);
            break;
        case 8:
            opti_threading(rules, &(rules[regle].resultat), 4, p);
            break;
        case 9:
            opti_threading(rules, &(rules[regle].priorite), 5, p);
            break;
        default:
            break;
    }
}

/**
 * @brief fonction qui permet de lancer NB_PARTIES simulations avec des règles et stocke la moyenne des
 * resultats des simulations dans l'entier passé en paramètre
 *
 * @param rules règle à evaluer
 * @param resultat adresse pour stocker le resultat
 */
void performance_moyenne(RuleTyped rules[NB_RULES], int *resultat)
{
    thrd_t tab_thread[NB_THREADS];
    int tab_moyenne[NB_THREADS];
    int somme = 0;
    int e_carre = 0;

    for (int i = 0; i < NB_PARTIES / NB_THREADS; i++)
    {

        for (int j = 0; j < NB_THREADS; j++)
            thrd_create(&tab_thread[j], simulation, (void *)rules);

        for (int j = 0; j < NB_THREADS; j++)
            thrd_join(tab_thread[j], &tab_moyenne[j]);

        for (int j = 0; j < NB_THREADS; j++)
        {
            somme += tab_moyenne[j];
            e_carre += powf(tab_moyenne[j],2);
        }
    }

    *resultat = somme / NB_PARTIES;

    //printf("ecart_type = %f\n",sqrtf(e_carre/ NB_PARTIES - powf(somme / NB_PARTIES, 2)));
}

/**
 * @brief fonction qui teste en multithread toutes les possibilités d'une valeur
 *
 * @param rules tableau de regle
 * @param valeur pointeur sur la valeur à faire varier
 * @param univers univers de la valeur
 * @param p probabilité de l'affichage de débug
 */
void opti_threading(RuleTyped rules[NB_RULES], int *valeur, int univers, float p)
{
    int * reponses, res;
    reponses = (int*)malloc(univers*sizeof(int));
    

    for (int i = 0; i < univers; i++)
    {
        // traitement du paramètre contenant la réponse d'une regle
        if (univers == 4)
            *valeur = i * 2;
        // traitement du paramètre contenant la priorité d'une regle
        else if (univers == 5)
            *valeur = i + 1;
        // traitement des paramètres de preception
        else
            *valeur = i - 1;
        
        performance_moyenne(rules, &reponses[i]);
    }

    res = select_best(reponses, univers);

    if (p < 0.05)
    {
        printf("[ ");
        for (int i = 0; i < univers; i++)
        {
            printf("%d ", reponses[i]);
        }
        puts("]");
        printf("[ ");
        for (int i = 0; i < univers; i++)
        {
            if (univers == 4)
                printf("%d ", 2 * i);
            else if (univers == 5)
                printf("%d ", i + 1);
            else
                printf("%d ", i - 1);
        }
        puts("]");
        printf("resultat final : %d\n", res);
    }
    
    free(reponses);

    *valeur = res;
}



/**
 * @brief fonction qui renvoie la valeur correspondant à l'indice de la valeur la plus élevée du tableau
 *
 * @param tab tableau d'entier contenant les valeurs moyennes d'execution
 * @param taille taille du tableau
 * @return int
 */
int select_best(int *tab, int taille)
{
    int best_i = 0, best = tab[0], i;
    for (i = 1; i < taille; i++)
    {
        if (tab[i] > best)
        {
            best = tab[i];
            best_i = i;
        }
    }

    if (taille == 4)
        best_i *= 2;
    else if (taille == 5)
        best_i++;
    else
        best_i--;

    return best_i;
}

/**
 * @brief fonction qui prend le tableau des moyennes pour toutes les possibilités des deux var
 * et qui modifie les deux params en gardant les meilleurs
 *
 * @param moyennes tableau des moyennes des differentes regles
 * @param univers1 taille de l'univers de la variable 1
 * @param univers2 taille de l'univers de la variable 2
 * @param var1 adresse du paramètre1 à modifier
 * @param var2 adresse du paramètre2 à modifier
 */
void garde_meilleurs_params(int **moyennes, int univers1, int univers2, int *var1, int *var2)
{
    int i,j,
        meilleur = moyennes[0][0];
    *var1 = 0;
    *var2 = 0;


    for (i = 0; i < univers1; i++)
    {
        for (j = 0; j < univers2; j++)
        {
            if (moyennes[i][j] > meilleur)
            {
                *var1 = i;
                *var2 = j;
                meilleur = moyennes[i][j];
            }
        }
    }

    printf("resultat : %d %d\n", *var1, *var2);

    if (univers1 == 4)
        (*var1) *= 2;
    else if (univers1 == 5)
        (*var1) += 1;
    else if (univers1 > 0)
        (*var1) -= 1;

    if (univers2 == 4)
        (*var2) *= 2;
    else if (univers2 == 5)
        (*var2) +=  1;
    else if (univers2 > 0)
        (*var2) -=  1;

    printf("resultat final : %d %d\n", *var1, *var2);


    liberation_matrice_opti(moyennes, univers1);
}

/**
 * @brief initialisation d'une matrice
 * @param taille_n entier qui détermine le nombre de ligne de la matrice
 * @param taille_j entier qui détermine le nombre de colonne de la matrice
 * @return une matrice d'entier
 *
 */
int **init_matrice_opti(int taille_n, int taille_p)
{
    int **matrice;
    matrice = (int **)malloc(taille_n * sizeof(int *));
    for (int i = 0; i < taille_n; i++)
    {
        matrice[i] = (int *)malloc(taille_p * sizeof(int));
        for (int j = 0; j < taille_p; j++)
            matrice[i][j] = 0;
    }
    return matrice;
}

/**
 * @brief libération matrice
 * @param matrice tableau d'entier à 2 dimensions à libérer
 * @param taille_n le nombre des lignes de la matrice
 */
void liberation_matrice_opti(int **matrice, int taille_n)
{
    for (int i = 0; i < taille_n; i++)
    {
        free(matrice[i]);
    }
    free(matrice);
}

/**
 * @brief fonction qui remplie le tableau des moyennes
 * 
 * @param rules tableau des regles actuelles 
 * @param moyennes tableau des moyennes à remplir
 * @param univers1 taille de l'univers du param1
 * @param univers2 taille de l'univers du param2
 * @param var1 pointeur sur le param 1
 * @param var2 pointeur sur le param 2
 */
void opti_threading_2_param(RuleTyped rules[NB_RULES], int ** moyennes, int univers1, int univers2, int * var1, int * var2)
{

    for (int i = 0; i < univers1; i++)
    {
        // traitement du paramètre contenant la réponse d'une regle
        if (univers1 == 4)
            *var1 = i*2;
        // traitement du paramètre contenant la priorité d'une regle
        else if (univers1 == 5)
            *var1 = i + 1;
        // traitement des paramètres de preception
        else
            *var1 = i - 1;

        for (int j = 0; j < univers2; j++)
        {
            // traitement du paramètre contenant la réponse d'une regle
            if (univers2 == 4)
                *var2 = j*2;
            // traitement du paramètre contenant la priorité d'une regle
            else if (univers2 == 5)
                *var2 = j + 1;
            // traitement des paramètres de preception
            else
                *var2 = j - 1;
            
            performance_moyenne(rules, &moyennes[i][j]);
        }
        
    }



    for (int i = 0; i < univers1; i++)
    {
        for (int j = 0; j < univers2; j++)
        {
            printf("%d\t", moyennes[i][j]);
        }
        printf("\n ");
    }

    for (int i = 0; i < univers1; i++)
    {
        for (int j = 0; j < univers2; j++)
        {
            if (univers1 == 4)
                printf("%d ", i*2);
            // traitement du paramètre contenant la priorité d'une regle
            else if (univers1 == 5)
                printf("%d ", i + 1);
            // traitement des paramètres de preception
            else
                printf("%d ", i - 1);
            if (univers2 == 4)
                printf("%d\t", j*2);
            // traitement du paramètre contenant la priorité d'une regle
            else if (univers2 == 5)
                printf("%d\t", j + 1);
            // traitement des paramètres de preception
            else
                printf("%d\t", j - 1);
                
        }
        printf("\n ");
    }




    garde_meilleurs_params(moyennes, univers1, univers2, var1, var2);

}

/**
 * @brief calcul de la taille de l'univers et trouve le un champ d'une règle
 * @param tab_regles tableau des regles 
 * @param regle eniter qui désigne une règle
 * @param champ entier correspondant valeur du champ à modifier
 * @param var double pointeur sur un entier
 * @return la taille de l'univers
*/
int calcul_param_champs(RuleTyped tab_regles[NB_RULES], int regle, int champ, int **var){
    int univers = 3;
    switch (champ)
    {
        case 0:
            *var = &tab_regles[regle].n;
            univers = 3;
            break;
        case 1:
            *var = &tab_regles[regle].o;
            univers = 3;
            break;
        case 2:
            *var = &tab_regles[regle].s;
            univers = 3;
            break;
        case 3:
            *var = &tab_regles[regle].e;
            univers = 3;
            break;
        case 4:
            *var = &tab_regles[regle].direction_proie;
            univers = 9;
            break;
        case 5:
            *var = &tab_regles[regle].distance_proie;
            univers = 3;
            break;
        case 6:
            *var = &tab_regles[regle].direction_chasseur;
            univers = 9;
            break;
        case 7:
            *var = &tab_regles[regle].distance_chasseur;
            univers = 3;
            break;
        case 8:
            *var = &tab_regles[regle].resultat;
            univers = 4;
            break;
        case 9:
            *var = &tab_regles[regle].priorite;
            univers = 5;
            break;
        default:
            break;
    }

    return univers;
}

/**
 * @brief appel des 2 optimisations [nb_boucle] fois
 * 
 * @param table_regles tableau de règles 
 */
void appel_opti(RuleTyped table_regles[NB_RULES])
{
    int moy = 0, prec = 0, energie = 5, j, moy_shuffle = 0, moy_db_opti = 0;
    RuleTyped copy[NB_RULES];

    performance_moyenne(table_regles, &moy);
    printf("moyenne initiale opti : %d\n",moy);


    while (moy < 80)
    {
        j = 0;
        do
        {
            printf("tour n°%d\n", j);
            optimisation_regles(table_regles);
            prec = moy;
            performance_moyenne(table_regles, &moy);
            energie = moy - prec;
            printf("energie : %d\n",energie);
            j++;
        }while(energie > 2);
        if (SHUFFLE_OR_DOUBLE_OPTI)
        {
            printf("shuffle : %d s'arretera quand %d sera trouvé\n",moy, moy + 3);
            while(moy_shuffle <= moy + 2)//for(int i = 0 ; i < 15 ; i++)
            {
                copyRules(table_regles,copy);
                shuffle_alea(copy,NB_SHUFFLE);
                performance_moyenne(copy, &moy_shuffle);
                printf("moyenne après mélange : %d\n",moy_shuffle);
                if (moy_shuffle > moy + 2)
                {
                    copyRules(copy,table_regles);
                    printf("changement de voisinage\n");
                }
            }
            moy = moy_shuffle;
        }
        else 
        {
            while (moy_db_opti < moy + 1)
            {
                optimisation_double_regles(table_regles);
                performance_moyenne(table_regles, &moy_db_opti);
                printf("moyenne apres double opti : %d\n",moy_db_opti);
                if (moy_db_opti > moy + 1)
                {
                    printf("changement de voisinage");
                }
            }
            moy = moy_db_opti;
        }
        moy_db_opti = 0;
        moy_shuffle = 0;
        ecriture_regles(table_regles, moy, "regles/nouvelles_regles.txt");
    }
    printf("Les Règles finales avec les 2 optimisations : \n");
    affiche_regles(table_regles);
}



/**
 * @brief creer une copie du tableau de regles actuelles 
 *
 * @param rules tableau de regles
 * @param new_value nouvelle valeur pour la copie du tableau
 */
void copyRules(RuleTyped rules[NB_RULES], RuleTyped new[NB_RULES])
{
    
    for (int i = 0; i < NB_RULES; i++)
    {
        new[i].n = rules[i].n;
        new[i].o = rules[i].o;
        new[i].s = rules[i].s;
        new[i].e = rules[i].e;
        new[i].direction_proie = rules[i].direction_proie;
        new[i].distance_proie = rules[i].distance_proie;
        new[i].direction_chasseur = rules[i].direction_chasseur;
        new[i].distance_chasseur = rules[i].distance_chasseur;
        new[i].resultat = rules[i].resultat;
        new[i].priorite = rules[i].priorite;
    }

}

/**
 * @brief prend un tableau de regles en entrée et effectue nb_shuffle modification aléatoire dessus 
 * 
 * @param rules tableau de regle à mélanger
 * @param nb_suffle nombre de mélange à effectuer
 */
void shuffle_alea(RuleTyped rules[NB_RULES], int nb_suffle)
{
    int regle, param , univers, * var = NULL, champs;
    
    for (int i = 0; i < nb_suffle; i++)
    {
        param = rand()%(NB_RULES * NB_ATTRIBUTS_REGLE);

        regle = param / 10;
        champs = param - 10 * regle;
        
        univers = calcul_param_champs(rules, regle, champs, &var);

        if (univers == 4)
            *var = (rand() % 4) * 2;
        else if(univers == 5)
            *var = 1 + rand() % 5;
        else
            *var = rand() % univers - 1;
    }
    
}