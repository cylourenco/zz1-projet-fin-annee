#ifndef __OPTI_H__
#define __OPTI_H__

#include "console.h"
#include <threads.h>


#define NB_THREADS 8
#define NB_PARTIES 800
#define SHUFFLE_OR_DOUBLE_OPTI 0 // 1 mélange aleatoire des regles 0 opti deux variables
#define NB_SHUFFLE 10
#define MODE 0 // 1 : opti, 0 : jeu SDL meilleures_regles 
#define VITESSE_SDL 200

/**
 * @brief optimise les règles et les sauvegardes dans un fichier
 * @param table_regles tableau des règles à optimiser
 */
void optimisation_regles(RuleTyped table_regles[NB_RULES]);

/**
 * @brief optimisation de deux variables et les sauvegardes dans un fichier
 * @param table_regles tableau des règles à optimiser
 */
void optimisation_double_regles(RuleTyped table_regles[NB_RULES]);

/**
 * @brief crée et mélange un tableau d'entier
 * @param tableau tableau d'entier passé en paramètre
 * @param taille taille du tableau à créer
 */
void melanger_entiers(int *tableau, int taille);

/**
 * @brief fonction qui creer une liste de regle aleatoire
 *
 * @param table_regles tableau où les regles sont stockées
 */
void creation_regle_aleatoire(RuleTyped table_regles[NB_RULES]);

/**
 * @brief change une valeur selon son univers avec une probabilité donnée
 *
 * @param valeur adresse de la valeur à changer
 * @param proba probabilité de changement
 * @param taille_univers taille de l'univers de la valeur
 */
void maj_proba(int *valeur, float proba, int taille_univers);

/**
 * @brief fonction qui evalue toutes les possibilité pour un champs donné et garde la meilleure
 *
 * @param rules tableau des regles
 * @param position poisition du champs à optimiser
 * @param p probabilité de l'affichage de débug 
 */
void modif_parametre(RuleTyped rules[NB_RULES], int position, float p);

/**
 * @brief fonction qui permet de lancer NB_PARTIES simulations avec des règles et stocke la moyenne des
 * resultats des simulations dans l'entier passé en paramètre
 * 
 * @param rules règle à evaluer
 * @param resultat adresse pour stocker le resultat
 */
void performance_moyenne(RuleTyped rules[NB_RULES], int *resultat);

/**
 * @brief fonction qui teste en multithread toutes les possibilités d'une valeur
 *
 * @param rules tableau de regle
 * @param valeur pointeur sur la valeur à faire varier
 * @param univers univers de la valeur
 * @param p probabilité de l'affichage de débug 
 */
void opti_threading(RuleTyped rules[NB_RULES], int *valeur, int univers, float p);


/**
 * @brief fonction qui renvoie la valeur correspondant à l'indice de la valeur la plus élevée du tableau
 *
 * @param tab tableau d'entier contenant les valeurs moyennes d'execution
 * @param taille taille du tableau
 * @return int
 */
int select_best(int *tab, int taille);

/**
 * @brief fonction qui prend le tableau des moyennes pour toutes les possibilités des deux var
 * et qui modifie les deux params en gardant les meilleurs 
 * 
 * @param moyennes tableau des moyennes des differentes regles
 * @param univers1 taille de l'univers de la variable 1
 * @param univers2 taille de l'univers de la variable 2
 * @param var1 adresse du paramètre1 à modifier
 * @param var2 adresse du paramètre2 à modifier
 */
void garde_meilleurs_params(int ** moyennes, int univers1, int univers2, int * var1, int * var2);

/**
 * @brief initialisation d'une matrice 
 * @param taille_n entier qui détermine le nombre de ligne de la matrice
 * @param taille_j entier qui détermine le nombre de colonne de la matrice
 * @return une matrice d'entier
 * 
*/
int** init_matrice_opti(int taille_n, int taille_p);

/**
 * @brief libération matrice 
 * @param matrice tableau d'entier à 2 dimensions à libérer
 * @param taille_n le nombre des lignes de la matrice 
*/
void liberation_matrice_opti(int **matrice, int taille_n);


/**
 * @brief fonction qui remplie le tableau des moyennes
 * 
 * @param rules tableau des regles actuelles 
 * @param moyennes tableau des moyennes à remplir
 * @param univers1 taille de l'univers du param1
 * @param univers2 taille de l'univers du param2
 * @param var1 pointeur sur le param 1
 * @param var2 pointeur sur le param 2
 */
void opti_threading_2_param(RuleTyped rules[NB_RULES], int ** moyennes, int univers1, int univers2, int * var1, int * var2);


/**
 * @brief calcul de la taille de l'univers et trouve le un champ d'une règle
 * @param tab_regles tableau des regles 
 * @param regle eniter qui désigne une règle
 * @param champ entier correspondant valeur du champ à modifier
 * @param var double pointeur sur un entier
 * @return la taille de l'univers
*/
int calcul_param_champs(RuleTyped tab_regles[NB_RULES], int regle, int champ, int **var);

/**
 * @brief appel des 2 optimisations [nb_boucle] fois
 * 
 * @param table_regles tableau de règles 
 */
void appel_opti(RuleTyped table_regles[NB_RULES]);


/**
 * @brief creer une copie du tableau de regles actuelles 
 *
 * @param rules tableau de regles
 * @param new_value nouvelle valeur pour la copie du tableau
 */
void copyRules(RuleTyped rules[NB_RULES], RuleTyped new[NB_RULES]);
#endif

/**
 * @brief prend un tableau de regles en entrée et effectue nb_shuffle modification aléatoire dessus 
 * 
 * @param rules tableau de regle à mélanger
 * @param nb_suffle nombre de mélange à effectuer
 */
void shuffle_alea(RuleTyped rules[NB_RULES], int nb_suffle);