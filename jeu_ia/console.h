#ifndef __CONSOLE_H__
#define __CONSOLE_H__

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <math.h>

#define SIZE 15
#define NB_CHASSEURS 6
#define NB_PROIES 4
#define NB_ETOILES 2
#define NB_ITERATIONS 2000
#define NB_DIRECTION 8
#define SEUIL_DISTANCE 9
#define NB_RULES 20
#define PUI 3
#define NB_ATTRIBUTS_REGLE 10
#define CARTE_A_CHARGER 2
#define PROIE_INTELLIGENTE 1

#define ANSI_COLOR_RESET "\x1b[0m"
#define ANSI_COLOR_SKYBLUE "\x1b[96;1m"
#define ANSI_COLOR_GREEN "\x1b[32;1m"
#define ANSI_COLOR_ORANGE "\x1b[33;1m"
#define ANSI_COLOR_YELLOW "\x1b[93;1m"

typedef enum direction8
{
    JOKER1 = -1,
    N,
    NO,
    O,
    SO,
    S,
    SE,
    E,
    NE
} direction8_t;

typedef enum objet
{
    JOKER2 = -1,
    MUR,
    VIDE
} objet_t;

typedef enum info_distance
{
    JOKER3 = -1,
    PROCHE,
    LOIN
} info_distance_t;

typedef struct
{
    objet_t n, o, s, e;           // le contenu des 4 cases voisines
    direction8_t direction_proie; // dans quelle direction se trouve la proie la plus proche
    info_distance_t distance_proie;
    direction8_t direction_chasseur;
    info_distance_t distance_chasseur;
    int priorite;
    direction8_t resultat;
} RuleTyped;

typedef struct position
{
    int x;
    int y;
} pos_t;

/**
 * @brief création et initialisation d'une matrice de SIZE par SIZE
 * @param n taille de la matrice
 * @return int** la matrice initialisé
 */
int **init_matrice();

/**
 * @brief permet de libérer une matrice d'entiers
 * @param matrice matrice d'entier à libérer
 * @param taille entier qui contient la taille de la matrice
 */
void liberation_matrice(int **matrice, int taille);

/**
 * @brief affiche la matrice de dimension dim
 * @param matrice matrice à afficher
 * @param dim dimension de la matrice
 */
void affiche_matrice(int matrice[SIZE][SIZE], int dim);

/**
 * @brief choisit la prochaine direction d'une proie
 *
 * @param carte carte du jeu
 * @param etoile position de l'étoile la plus proche
 * @param proie position de la proie à déplacer
 * @param chasseurs la liste des chasseurs sur la map
 * @return direction8_t
 */
direction8_t prochaine_direction_proie(int carte[SIZE][SIZE], pos_t etoile, pos_t proie, pos_t chasseurs[NB_CHASSEURS]);

/**
 * @brief savoir si la proie est bloquée ou non
 * 
 * @param carte la carte
 * @param chasseurs la liste des positions des chasseurs
 * @param proie la position de la proie
 * @return int 0 si la proie n'est pas bloquée, 1 sinon
 */
int proie_bloquee(int carte[SIZE][SIZE], pos_t chasseurs[NB_CHASSEURS], pos_t proie);

/**
 * @brief choisit la prochaine direction d'un chasseur
 *
 * @param carte carte du jeu
 * @param chasseur_proche position du chasseur le plus proche
 * @param proie_proche postion de la proie la plus proche
 * @param chasseur position du chasseur à déplacer
 * @param rules la liste des règles que suit les chasseurs
 * @return direction8_t
 */
direction8_t prochaine_direction_chasseur(int carte[SIZE][SIZE], pos_t chasseur_proche, pos_t proie_proche, pos_t chasseur, RuleTyped rules[NB_RULES]);

/**
 * @brief tire une règle parmi les règle possible proportionnellement à leur probabilité
 *
 * @param rules tableau des règles
 * @param regle_correspondante_indice tableau contenant les indice des règles correspondantes
 * @param nb_regle_correspondante_indice nombre de règle correspondantes
 * @return direction8_t
 */
direction8_t tirage_direction(RuleTyped rules[NB_RULES], int regle_correspondante_indice[NB_RULES], int nb_regle_correspondante_indice);

/**
 * @brief permet de charger une carte
 * @param matrice matrice d'entier qui va contenir la map
 */
void charger_carte(int matrice[SIZE][SIZE]);

/**
 * @brief effectue le déplacement d'un entité sur la carte si possible
 *
 * @param carte carte du jeu
 * @param perso position du personnage sur la carte
 * @param direction la direction où va le perso
 * @return int renvoie 1 si déplacement réussi 0 sinon
 */
int deplacement(int carte[SIZE][SIZE], pos_t *perso, direction8_t direction);

/**
 * @brief effectue le déplacement des proies sur la carte si possible
 *
 * @param carte carte du jeu
 * @param proie position des proies sur la carte
 * @param etoiles position des étoiles sur la carte
 * @param chasseurs position des chasseurs sur la carte
 */
void deplacement_proies(int carte[SIZE][SIZE], pos_t proie[NB_PROIES], pos_t etoiles[NB_ETOILES], pos_t chasseur[NB_CHASSEURS]);

/**
 * @brief renvoie la position de l'élement le plus proche de e1 parmis L
 *
 * @param e1 position de l'élément
 * @param L tableau de position des autres éléments
 * @param nb_elements taille tableau L
 * @return pos_t la position de l'objet L la plus proche
 */
pos_t plus_proche(pos_t e1, pos_t *L, int nb_element);

/**
 * @brief calcule la distance euclidienne entre deux positions
 *
 * @param p1 position 1
 * @param p2 position 2
 * @return float distance euclidienne
 */
float distance_euclidienne(pos_t p1, pos_t p2);

/**
 * @brief verifie si un objet se trouve au meme endroit que l'un de la liste passé en paramètre
 *
 * @param objet1 objet étudié
 * @param liste_objet liste d'objet que l'on verifie
 * @param nb_elements nombre d'éléments de la liste
 * @return int -1 si non trouvé sinon  indice de l'élément dans le tableau
 */
int verif_superposition(pos_t objet1, pos_t *liste_objet, int nb_elements);

/**
 * @brief effectue le déplacement des chasseurs sur la carte
 *
 * @param carte carte du jeu
 * @param chasseurs position des chasseurs
 * @param proies position des proies
 * @param rules les regles que le chasseur doit suivre
 */
void deplacement_chasseurs(int carte[SIZE][SIZE], pos_t chasseurs[NB_CHASSEURS], pos_t proies[NB_PROIES], RuleTyped rules[NB_RULES]);

/**
 * @brief initialise la position des enemis sur la carte
 *
 * @param carte la carte du jeu
 * @param chassseurs la liste des position des chasseurs
 */
void init_chasseurs(int carte[SIZE][SIZE], pos_t chasseurs[NB_CHASSEURS]);

/**
 * @brief initialise la position des proies sur la carte
 *
 * @param carte la carte du jeu
 * @param proies la liste des position des proies
 */
void init_proies(int carte[SIZE][SIZE], pos_t proies[NB_PROIES]);

/**
 * @brief initialise la position des étoiles sur la carte
 *
 * @param carte la carte du jeu
 * @param etoiles la liste des position des etoiles
 */
void init_etoiles(int carte[SIZE][SIZE], pos_t etoiles[NB_ETOILES]);

/**
 * @brief affichage de debug du jeu à un moment donné
 * @param carte matrice d'entier représentant la carte
 * @param chasseurs tableau des positions des chasseurs
 * @param proies tableau des positions des proies
 * @param etoiles tableau des positions des etoiles
 */
void affiche_jeu(int carte[SIZE][SIZE], pos_t chasseurs[NB_CHASSEURS], pos_t proies[NB_PROIES], pos_t etoiles[NB_ETOILES]);

/**
 * @brief trouver une case vide dans la carte de manière aléatoire
 * @param carte matrice d'entiers représentant la carte du jeu
 * @return la position libre trouvée
 */
pos_t trouver_position_libre(int carte[SIZE][SIZE]);

/**
 * @brief check si une proie est mangée par un chasseur
 *
 * @param carte carte de jeu
 * @param proies liste des postions des proies
 * @param chasseurs liste des positions des chasseurs
 * @param nb_mange_par_chasseur liste du nombre de proies mangés par chaque chasseur
 */
void proies_mangees(int carte[SIZE][SIZE], pos_t proie[NB_PROIES], pos_t chasseurs[NB_CHASSEURS], int nb_mange_par_chasseur[NB_CHASSEURS]);

/**
 * @brief check si une proie est mangée par un chasseur
 *
 * @param carte carte de jeu
 * @param proies liste des postions des proies
 * @param etoiles liste des postions des étoiles
 */
void etoiles_mangees(int carte[SIZE][SIZE], pos_t proies[NB_PROIES], pos_t etoiles[NB_ETOILES]);

/**
 * @brief regarde quelles règles correspond le plus à la situation actuelle
 *
 * @param rules liste des règles
 * @param current la situation courante
 * @param tabeau_indice la liste des indices des règles qui correspondent
 * @param nb_regles_correspondantes le nombre de règles correspond le plus à la situation actuelle
 */
void regles_correspondantes(RuleTyped *rules, RuleTyped current, int regle_correspondante_indice[NB_RULES], int *nb_regle_correspondante_indice);

/**
 * @brief fonction qui initialise les différents acteurs d'une partie
 *
 * @param carte carte à initialiser
 * @param chasseurs tableau des positions des chasseurs à initaliser
 * @param proies tableau des positions des proies à initaliser
 * @param etoiles tableau des positions des etoiles à initaliser
 * @param nb_mange_par_chasseur tableau du nombre de proies mangés par chaque chasseur
 */
void init_Jeu(int carte[SIZE][SIZE], pos_t chasseurs[NB_CHASSEURS], pos_t proies[NB_PROIES], pos_t etoiles[NB_ETOILES], int nb_mange_par_chasseur[NB_CHASSEURS]);

/**
 * @brief lance la simulation du jeu pour le nombre de proie chasseur et etoile choisi en préprocesseur
 *
 * @param rules regles
 */
int simulation(void *rules);

/**
 * @brief fonction de qualification de la distance entre deux éléments d'un plan orthonormé
 *
 * @param e1 premier élément repère
 * @param e2 deuxieme élément à positionner par rapport au premier
 * @return info_distance_t
 */
info_distance_t dist(pos_t e1, pos_t e2);

/**
 * @brief fonction qui renvoie la position relative de e2 par rapport à e1
 *
 * @param e1 premier élément repère
 * @param e2 deuxieme élément à positionner par rapport au premier
 * @return direction8_t
 */
direction8_t dir(pos_t e1, pos_t e2);

/**
 * @brief lecture d'un tableau de règles à partir d'un fichier
 * @param tableau tableau de règles à remplir
 * @param nom_fichier nom du fichier où l'on doit lire les règles
 * @return la moyenne correspondant aux règles lues
 */
int lecture_regles(RuleTyped table_regles[NB_RULES], char *nom_fichier);

/**
 * @brief écriture d'un tableau de règles dans un fichier
 * @param table_regles tableau de règles à sauvegarder
 * @param nom_fichier nom du fichier où l'on doit écrire les règles
 * @param moyenne entier correspondant à la moyenne de la règle à écrire
 */
void ecriture_regles(RuleTyped table_regles[NB_RULES], int moyenne, char *nom_fichier);

/**
 * @brief affiche le tableau de règles
 * @param table_regles tableau contenant des règles
 */
void affiche_regles(RuleTyped table_regles[NB_RULES]);

/**
 * @brief sauvegarde l'ancienne position
 * @param pos_acutelle tableau des postions actuelles
 * @param ancienne_pos tableau des anciennes positions
 * @param taille taille du tableau
 */
void sauvegarder_ancienne_pos(pos_t *pos_actuelle, pos_t *ancienne_pos, int taille);

/**
 * @brief compare deux règles et sauvegarde la meilleure dans regles/meilleur_regles.txt
 * @param moyenne_anciennes_regles moyenne des anciennes regles
 * @param nouvelles_regles tableau des nouvelles regles
 * @param moyenne_nouvelles_regles moyenne des nouvelles regles
 *
 */
void ecrire_meilleure_regle(int moyenne_anciennes_regles, RuleTyped nouvelles_regles[NB_RULES], int moyenne_nouvelles_regles);

/**
 * @brief fonction de test
 */
void test_console(void);

#endif