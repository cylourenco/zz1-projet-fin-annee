#include "console.h"
#include "sdl.h"
#include "opti.h"

int main()
{
    srand(time(NULL));
    if(MODE)
    {
        RuleTyped tab_regles[NB_RULES];
        int moyenne_avant;
        int moyenne_nouv_regles;

        moyenne_avant = lecture_regles(tab_regles, "regles/meilleures_regles.txt");
        creation_regle_aleatoire(tab_regles);
        //lecture_regles(tab_regles, "regles/nouvelles_regles.txt");

        performance_moyenne(tab_regles, &moyenne_nouv_regles);
        printf("perfomance moyenne avant : %d\n",moyenne_nouv_regles);

        appel_opti(tab_regles);

        performance_moyenne(tab_regles, &moyenne_nouv_regles);
        ecrire_meilleure_regle(moyenne_avant, tab_regles, moyenne_nouv_regles);
        printf("perfomance moyenne apres : %d\n", moyenne_nouv_regles);
    }

    SDL();  
    
    return 0;

}