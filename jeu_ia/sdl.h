#ifndef __SDL_H__
#define __SDL_H__

#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>
#include <SDL2/SDL_ttf.h>
#include <math.h>
#include <stdio.h>
#include <string.h>
#include "console.h"

/**
 * @brief fonction d'initialisation des variables propre à la sdl
 *
 * @param window
 * @param renderer
 * @param texture
 */
void init_sdl(SDL_Window **window, SDL_Renderer **renderer, SDL_Texture *texture[10], SDL_Rect pos_texture[10]);

/**
 * @brief fonction de fermeture de toutes les variables ouvertes par l'utilisation de la SDL
 *
 * @param ok fin normale : ok = 0 ; anormale ok = 1
 * @param msg message à afficher
 * @param window fenêtre à fermer
 * @param renderer rendu à fermer
 * @param texture rendu à fermer
 */
void end_sdl(int ok, char const *msg, SDL_Window *window, SDL_Renderer *renderer, SDL_Texture *texture[10]);

/**
 * @brief fonction qui charge une image dans une texture
 *
 * @param file_image_name nom du fichier de l'image
 * @param window fenetre à fermer si le chargement echoue
 * @param renderer rendu permettant le chargement de l'image
 * @return SDL_Texture* l'image charé dans l'adresse d'une sdl_texture
 */
SDL_Texture *load_texture_from_image(char *file_image_name, SDL_Window *window, SDL_Renderer *renderer, SDL_Texture *texture[10]);

/**
 * @brief affiche l'itération en cours avec de la SDL
 *
 * @param window fenetre d'affichage
 * @param renderer rendu associé à la fenetre
 * @param texture tableau des textures
 * @param pos_texture tableau de coordonnées pour les textures
 * @param carte carte de jeu à afficher
 * @param chasseur liste des chasseurs à afficher
 * @param proies liste des proies à afficher
 * @param etoiles liste des étoiles à afficher
 */
void affiche_itere(SDL_Renderer *renderer, SDL_Texture *texture[10], SDL_Rect pos_texture[10],
                   int carte[SIZE][SIZE], pos_t chasseurs[NB_CHASSEURS],
                   pos_t proies[NB_PROIES], pos_t etoiles[NB_ETOILES],
                   pos_t ancienne_pos_chasseurs[NB_CHASSEURS],
                   pos_t ancienne_pos_proies[NB_PROIES]);

/**
 * @brief retourne si la souris se trouve dans un des boutons du menu
 *
 * @param mouse_x position de la souris en x
 * @param mouse_y position de la souris en y
 * @param size_restangle_x taille de la largeur du rectangle
 * @param size_restangle_y taille de la hauteur du rectangle
 * @param position_rectangle_x position du rectangle en x
 * @param position_rectangle_y position du rectangle en y
 * @return int entier qui retourne si la position de la souris se trouve dans Jouer (1) ou Quitter (0)
 */
int check_click_menu(int mouse_x, int mouse_y, int size_restangle_x, int size_restangle_y, int position_rectangle_x, int position_rectangle_y);

/**
 * @brief affiche le menu sur le renderer
 *
 * @param renderer le rendu
 * @param window la fenêtre
 * @param texture les texture
 * @param pos_texture les positions des textures
 * @param font la police
 * @param mouse_x la position de la souris en x
 * @param mouse_y la position de la souris en y
 * @param size_restangle_x la taille du rectangle en x
 * @param size_restangle_y la taille du rectangle en y
 * @param position_rectangle_x la position du rectangle en x
 * @param position_rectangle_y la position du rectangle en y
 */
void afficher_menu(SDL_Renderer *renderer, SDL_Window *window, SDL_Texture *texture[10], SDL_Rect pos_texture[10], TTF_Font *font, int mouse_x, int mouse_y, int size_restangle_x, int size_restangle_y, int position_rectangle_x, int position_rectangle_y);

/**
 * @brief dessine un rectangle avec un texte
 *
 * @param renderer le rendu
 * @param window la fenêtre
 * @param texture les textures
 * @param font la police
 * @param etat l'état du rectangle (si 1 pour simulation sinon parametre)
 * @param color_rect la couleur demandé
 */
void draw_rectangle_menu(SDL_Renderer *renderer, SDL_Window *window, SDL_Texture *texture[10], TTF_Font *font, int etat, Uint8 color_rect);

/**
 * @brief fonction d'affichage d'une boucle de jeu en SDL
 *
 */
void SDL();

#endif