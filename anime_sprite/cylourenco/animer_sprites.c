#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>
#include <math.h>
#include <stdio.h>
#include <string.h>

/// @brief fonction de fermeture de toutes les variables ouvertes par l'utilisation de la SDL
/// @param ok voir si la fin se passe bien ok = 0 fin reussi 1 sinon
/// @param msg message à afficher
/// @param window fenetre
/// @param renderer rendu
/// @param texture texture
void end_sdl(char ok, char const *msg, SDL_Window *window, SDL_Renderer *renderer, SDL_Texture *texture[4])
{
    char msg_formated[255];
    int l;

    if (!ok)
    { // Affichage de ce qui ne va pas
        strncpy(msg_formated, msg, 250);
        l = strlen(msg_formated);
        strcpy(msg_formated + l, " : %s\n");

        SDL_Log(msg_formated, SDL_GetError());
    }

    // fermer toutes les textures
    for (int i = 0; i < 4; i++)
    {
        if (texture[i] != NULL)
        {                                   // Destruction si nécessaire de la texture
            SDL_DestroyTexture(texture[i]); // Attention : on suppose que les NULL sont maintenus !!
            texture[i] = NULL;
        }
    }
    if (renderer != NULL)
    {                                  // Destruction si nécessaire du renderer
        SDL_DestroyRenderer(renderer); // Attention : on suppose que les NULL sont maintenus !!
        renderer = NULL;
    }
    if (window != NULL)
    {                              // Destruction si nécessaire de la fenêtre
        SDL_DestroyWindow(window); // Attention : on suppose que les NULL sont maintenus !!
        window = NULL;
    }

    IMG_Quit();
    SDL_Quit();

    if (!ok)
    { // On quitte si cela ne va pas
        exit(EXIT_FAILURE);
    }
}

/**
 * @brief fonction d'affichage du sprite de base
 *
 * @param renderer rendu à afficher
 * @param window fenetre à afficher
 * @param texture liste des textures à afficher
 * @param destinationRect_objet1 emplacement de l'objet 1 au moment pres
 * @param destinationRect_objet2 emplacement de l'objet 2 au moment pres
 * @param vitesse_objet1 vitesse de l'objet 1
 * @param vitesse_objet2 vitesse de l'objet 2
 */
void animer(SDL_Renderer *renderer, SDL_Window *window, SDL_Texture *texture[4], SDL_Rect *destinationRect_objet1, SDL_Rect *destinationRect_objet2, int *vitesse_objet1, int *vitesse_objet2)
{
    int window_width, window_height;

    SDL_GetWindowSize(window, &window_width, &window_height);
    SDL_Rect
        source = {0},            // Rectangle définissant la zone totale de la planche
        window_dimensions = {0}, // Rectangle définissant la fenêtre, on n'utilisera que largeur et hauteur
        destination = {0},       // Rectangle définissant où la zone_source doit être déposée dans le renderer
        state = {0};             // Rectangle de la vignette en cours dans la planche

    SDL_GetWindowSize(window, // Récupération des dimensions de la fenêtre
                      &window_dimensions.w,
                      &window_dimensions.h);
    SDL_QueryTexture(texture[1], // Récupération des dimensions de l'image
                     NULL, NULL,
                     &source.w, &source.h);

    SDL_Rect fond;

    fond.x = 0;                   // x haut gauche du rectangle
    fond.y = 0;                   // y haut gauche du rectangle
    fond.w = window_dimensions.w; // sa largeur (w = width)
    fond.h = window_dimensions.h; // sa hauteur (h = height)

    int nb_images = 9;                   // Il y a 8 vignette dans la ligne de l'image qui nous intéresse
    float zoom = 1.5;                    // zoom, car ces images sont un peu petites
    int offset_x = source.w / nb_images, // La largeur d'une vignette de l'image, marche car la planche est bien réglée
        offset_y = source.h / 5;         // La hauteur d'une vignette de l'image, marche car la planche est bien réglée

    state.x = 0;            // La première vignette est en début de ligne
    state.y = 4 * offset_y; // On s'intéresse à la 4ème ligne, le bonhomme qui court
    state.w = offset_x;     // Largeur de la vignette
    state.h = offset_y;     // Hauteur de la vignette

    destination.w = offset_x * zoom; // Largeur du sprite à l'écran
    destination.h = offset_y * zoom; // Hauteur du sprite à l'écran

    destination.y = (window_dimensions.h - destination.h) / 1.5;

    int speed = 9;

    for (int x = 0; x < window_dimensions.w - destination.w; x += speed)
    {
        destination.x = x;              // Position en x pour l'affichage du sprite
        state.x += offset_x;            // On passe à la vignette suivante dans l'image
        state.x %= source.w - offset_x; // La vignette qui suit celle de fin de ligne est

        if ((*destinationRect_objet1).x > window_width - (*destinationRect_objet1).w || (*destinationRect_objet1).x < 0)
        {
            (*destinationRect_objet1).x = 0;
        }

        (*destinationRect_objet1).x += *vitesse_objet1 + 2;

        if ((*destinationRect_objet2).x > window_width - (*destinationRect_objet2).w || (*destinationRect_objet2).x < 0)
        {
            (*destinationRect_objet2).x = 0;
        }

        (*destinationRect_objet2).x += *vitesse_objet2 + 2;

        SDL_RenderClear(renderer); // Effacer l'image précédente avant de dessiner la nouvelle
        SDL_RenderCopy(renderer, texture[0], NULL, &fond);
        SDL_RenderCopy(renderer, texture[1], &state, &destination);
        SDL_RenderCopy(renderer, texture[2], NULL, destinationRect_objet1);
        SDL_RenderCopy(renderer, texture[3], NULL, destinationRect_objet2);
        SDL_RenderPresent(renderer); // Affichage
        SDL_Delay(50);               // Pause en ms
    }
}

int main(int argc, char **argv)
{
    (void)argc;
    (void)argv;

    SDL_Window *window = NULL;
    SDL_Renderer *renderer = NULL;
    SDL_Texture *texture[4];

    SDL_DisplayMode screen;

    if (SDL_Init(SDL_INIT_VIDEO) != 0)
        end_sdl(0, "ERROR SDL INIT", window, renderer, texture);

    SDL_GetCurrentDisplayMode(0, &screen);

    /* Création de la fenêtre */
    window = SDL_CreateWindow("Premier sprites", SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED, screen.w * 0.5,
                              screen.h * 0.6,
                              SDL_WINDOW_OPENGL);
    if (window == NULL)
        end_sdl(0, "ERROR WINDOW CREATION", window, renderer, texture);

    /* Création du renderer */
    renderer = SDL_CreateRenderer(window, -1, SDL_RENDERER_ACCELERATED | SDL_RENDERER_PRESENTVSYNC);

    if (renderer == NULL)
        end_sdl(0, "ERROR RENDERER CREATION", window, renderer, texture);

    /******************************************************Image de fond*******************************************************/

    SDL_Surface *surface = IMG_Load("img/fond.webp");
    texture[0] = SDL_CreateTextureFromSurface(renderer, surface);
    SDL_FreeSurface(surface);

    if (texture[0] == NULL)
        end_sdl(0, "ERROR TEXTURE CREATION", window, renderer, texture);

    SDL_RenderCopy(renderer, texture[0], NULL, NULL);
    SDL_RenderPresent(renderer);

    SDL_bool program_on = SDL_TRUE; // Booléen pour dire que le programme doit continuer
    SDL_Event event;                // c'est le type IMPORTANT !!

    int window_width, window_height;

    SDL_GetWindowSize(window, &window_width, &window_height);

    SDL_Rect fond;
    fond.x = 0;             // x haut gauche du rectangle
    fond.y = 0;             // y haut gauche du rectangle
    fond.w = window_width;  // sa largeur (w = width)
    fond.h = window_height; // sa hauteur (h = height)

    SDL_Surface *surface_lune = IMG_Load("img/lune.png");
    texture[2] = SDL_CreateTextureFromSurface(renderer, surface_lune);
    SDL_FreeSurface(surface_lune);
    int vitesse_lune = 3;

    SDL_Rect destinationRect_lune;
    destinationRect_lune.y = 50;
    destinationRect_lune.w = 230; // Largeur de l'image
    destinationRect_lune.h = 200; // Hauteur de l'image

    SDL_Surface *surface_planete = IMG_Load("img/planete.png");
    texture[3] = SDL_CreateTextureFromSurface(renderer, surface_planete);
    SDL_FreeSurface(surface_planete);
    int vitesse_planete = 1.5;

    SDL_Rect destinationRect_planete;
    destinationRect_planete.y = 0;  // Coordonnée y de l'image au centre de la fenêtre
    destinationRect_planete.w = 60; // Largeur de l'image
    destinationRect_planete.h = 60; // Hauteur de l'image

    SDL_Surface *surface_perso = IMG_Load("img/perso.png");
    texture[1] = SDL_CreateTextureFromSurface(renderer, surface_perso);
    SDL_FreeSurface(surface_perso);
    SDL_SetRenderDrawBlendMode(renderer, SDL_BLENDMODE_BLEND);

    while (program_on)
    { // Voilà la boucle des évènements

        if (SDL_PollEvent(&event))
        {
            switch (event.type)
            {                           // En fonction de la valeur du type de cet évènement
            case SDL_QUIT:              // Un évènement simple, on a cliqué sur la x de la fenêtre
                program_on = SDL_FALSE; // Il est temps d'arrêter le programme
                break;
            case SDL_KEYDOWN:
                switch (event.key.keysym.sym)
                { // la touche appuyée est ...

                // si le bouton droit est touche le perso passe
                case SDLK_RIGHT:
                    animer(renderer, window, texture, &destinationRect_lune, &destinationRect_planete, &vitesse_lune, &vitesse_planete);
                    break;

                default:
                    break;
                }

            default:
                break;
            }
        }


        // si aucun bouton presse alors le temps passe plus vite
        if (destinationRect_lune.x > window_width - destinationRect_lune.w || destinationRect_lune.x < 0)
        {
            destinationRect_lune.x = 0;
        }

        destinationRect_lune.x += vitesse_lune;

        if (destinationRect_planete.x > window_width - destinationRect_planete.w || destinationRect_planete.x < 0)
        {
            destinationRect_planete.x = 0;
        }

        destinationRect_planete.x += vitesse_planete;

        SDL_RenderClear(renderer); // Effacer l'image précédente avant de dessiner la nouvelle
        SDL_RenderCopy(renderer, texture[0], NULL, &fond);
        SDL_RenderCopy(renderer, texture[2], NULL, &destinationRect_lune);
        SDL_RenderCopy(renderer, texture[3], NULL, &destinationRect_planete);
        SDL_RenderPresent(renderer); // Affichage
        

        
    }

    /* on referme proprement la SDL */
    end_sdl(1, "Normal ending", window, renderer, texture);

    return EXIT_SUCCESS;
}