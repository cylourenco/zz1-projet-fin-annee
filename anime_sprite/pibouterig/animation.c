#include "animation.h"

/*********************************************************************************************************************/
/*                              Programme d'exemple de création de rendu + dessin                                    */
/*********************************************************************************************************************/





/**
 * @brief fonction de fermeture de toutes les variables ouvertes par l'utilisation de la SDL
 * 
 * @param ok fin normale : ok = 0 ; anormale ok = 1
 * @param msg message à afficher
 * @param window fenêtre à fermer
 * @param renderer rendu à fermer
 * @param texture rendu à fermer
 */
void end_sdl(char ok, char const *msg, SDL_Window *window, SDL_Renderer *renderer, SDL_Texture *texture[10])
{
    char msg_formated[255];
    int l;

    

    if (!ok)
    { // Affichage de ce qui ne va pas
        strncpy(msg_formated, msg, 250);
        l = strlen(msg_formated);
        strcpy(msg_formated + l, " : %s\n");

        SDL_Log(msg_formated, SDL_GetError());
    }

    for (int i = 0 ; i <10 ; i++)
    {
        if(texture[i] != NULL)
        {                                  // Destruction si nécessaire de la texture
            SDL_DestroyTexture(texture[i]);   // Attention : on suppose que les NULL sont maintenus !!
            texture[i] = NULL;
        }
    }
    if (renderer != NULL)
    {                                  // Destruction si nécessaire du renderer
        SDL_DestroyRenderer(renderer); // Attention : on suppose que les NULL sont maintenus !!
        renderer = NULL;
    }
    if (window != NULL)
    {                              // Destruction si nécessaire de la fenêtre
        SDL_DestroyWindow(window); // Attention : on suppose que les NULL sont maintenus !!
        window = NULL;
    }

    IMG_Quit();
    SDL_Quit();

    if (!ok)
    { // On quitte si cela ne va pas
        exit(EXIT_FAILURE);
    }
}



/**
 * @brief fonction qui charge une image dans une texture 
 * 
 * @param file_image_name nom du fichier de l'image
 * @param window fenetre à fermer si le chargement echoue
 * @param renderer rendu permettant le chargement de l'image
 * @return SDL_Texture* l'image charé dans l'adresse d'une sdl_texture 
 */
SDL_Texture* load_texture_from_image(char  *  file_image_name, SDL_Window *window, SDL_Renderer *renderer, SDL_Texture* texture[10])
{
    SDL_Surface *my_image = NULL;           // Variable de passage
    SDL_Texture* my_texture = NULL;         // La texture

    my_image = IMG_Load(file_image_name);   // Chargement de l'image dans la surface
                                            // image=SDL_LoadBMP(file_image_name); fonction standard de la SDL, 
                                            // uniquement possible si l'image est au format bmp */
    if (my_image == NULL) end_sdl(0, "Chargement de l'image impossible", window, renderer, texture);
   
    my_texture = SDL_CreateTextureFromSurface(renderer, my_image); // Chargement de l'image de la surface vers la texture
    SDL_FreeSurface(my_image);                                     // la SDL_Surface ne sert que comme élément transitoire 
    if (my_texture == NULL) end_sdl(0, "Echec de la transformation de la surface en texture", window, renderer, texture);

    return my_texture;
}


/**
 * @brief fonction d'affichage du sprite de base
 * 
 * @param renderer rendu à afficher
 * @param window fenetre à afficher
 */
void draw(SDL_Renderer *renderer, SDL_Window *window, SDL_Texture *texture[10], SDL_Rect nuage[2], SDL_Rect *destination, SDL_Rect * state)
{ 
    int h ,w;

    SDL_GetWindowSize(window, &w, &h);


    SDL_Rect    
        fond = {0},                      // Rectangle définissant la zone totale de la planche
        source = {0};                   // Rectangle définissant la zone totale de la planche

    SDL_QueryTexture(texture[1],           // Récupération des dimensions de l'image
            NULL, NULL,
            &source.w, &source.h);

    fond.x = 0;                 // x haut gauche du rectangle
    fond.y = 0;                 // y haut gauche du rectangle
    fond.w = w;                 // sa largeur (w = width)
    fond.h = h;                 // sa hauteur (h = height)

    SDL_RenderCopy(renderer, texture[0], NULL, &fond);

    int nb_images = 9;                     // Il y a 8 vignette dans la ligne de l'image qui nous intéresse
    float zoom = 2;                        // zoom, car ces images sont un peu petites
    int offset_x = source.w / nb_images,   // La largeur d'une vignette de l'image, marche car la planche est bien réglée
        offset_y = source.h / 5;           // La hauteur d'une vignette de l'image, marche car la planche est bien réglée
    

    state->x = 0 ;                          // La première vignette est en début de ligne
    state->y = 0 * offset_y;                // On s'intéresse à la 0ème ligne, le bonhomme qui est debout
    state->w = offset_x;                    // Largeur de la vignette
    state->h = offset_y;                    // Hauteur de la vignette

    destination->w = offset_x * zoom;       // Largeur du sprite à l'écran
    destination->h = offset_y * zoom;       // Hauteur du sprite à l'écran


    SDL_RenderCopy(renderer, texture[2], NULL, &nuage[0]);
    SDL_RenderCopy(renderer, texture[3], NULL, &nuage[1]);

    SDL_RenderCopy(renderer, texture[1], // Préparation de l'affichage
        state,
        destination); 
    
    SDL_RenderPresent(renderer); // affichage
    SDL_Delay(40);             // Pause exprimée en ms
  
}

/**
 * @brief fonction d'affichage du sprite marchant sur la droite
 * 
 * @param my_texture tableau des texture chargées
 * @param window fenetre à afficher
 * @param renderer rendu à afficher
 */
void marche_droite(SDL_Texture* my_texture[10], SDL_Window* window, SDL_Renderer* renderer, SDL_Rect nuage[2], SDL_Rect *destination, SDL_Rect* state) {
    SDL_Rect 
        source = {0},                    // Rectangle définissant la zone totale de la planche
        window_dimensions = {0};      // Rectangle définissant la fenêtre, on n'utilisera que largeur et hauteur

    SDL_GetWindowSize(window,              // Récupération des dimensions de la fenêtre
            &window_dimensions.w,
            &window_dimensions.h);
    SDL_QueryTexture(my_texture[1],           // Récupération des dimensions de l'image
            NULL, NULL,
            &source.w, &source.h);


    SDL_Rect fond;

    fond.x = 0;                                   // x haut gauche du rectangle
    fond.y = 0;                                   // y haut gauche du rectangle
    fond.w = window_dimensions.w;                 // sa largeur (w = width)
    fond.h = window_dimensions.h;                 // sa hauteur (h = height)


    /* Mais pourquoi prendre la totalité de l'image, on peut n'en afficher qu'un morceau, et changer de morceau :-) */

    int nb_images = 9;                     // Il y a 8 vignette dans la ligne de l'image qui nous intéresse
    float zoom = 2;                        // zoom, car ces images sont un peu petites
    int offset_x = source.w / nb_images,   // La largeur d'une vignette de l'image, marche car la planche est bien réglée
        offset_y = source.h / 5;           // La hauteur d'une vignette de l'image, marche car la planche est bien réglée
    

    state->y = 4 * offset_y;                // On s'intéresse à la 4ème ligne, le bonhomme qui court
    state->w = offset_x;                    // Largeur de la vignette
    state->h = offset_y;                    // Hauteur de la vignette

    destination->w = offset_x * zoom;       // Largeur du sprite à l'écran
    destination->h = offset_y * zoom;       // Hauteur du sprite à l'écran


    int speed = 15;
    

    //parallax des nuages
    nuage[0].x += 7;
    nuage[1].x += 4;
    if (nuage[0].x > window_dimensions.w + window_dimensions.w/5)
    {
        nuage[0].x = -window_dimensions.w/5;
    }
    if (nuage[1].x > window_dimensions.w + window_dimensions.w/5)
    {
        nuage[1].x = -window_dimensions.w/5;
    }
    



    destination->x += speed;                   // Position en x pour l'affichage du sprite
    if (destination->x > (window_dimensions.w - destination->w))
    {
        destination->x = (window_dimensions.w - destination->w);
    }


    SDL_RenderClear(renderer);           // Effacer l'image précédente avant de dessiner la nouvelle
    SDL_RenderCopy(renderer, my_texture[0], NULL, &fond);
    SDL_RenderCopy(renderer, my_texture[2], NULL, &nuage[0]);
    SDL_RenderCopy(renderer, my_texture[3], NULL, &nuage[1]);
    SDL_RenderCopy(renderer, my_texture[1], // Préparation de l'affichage
            state,
            destination);  
    SDL_RenderPresent(renderer);         // Affichage
    SDL_Delay(40);                       // Pause en ms

    SDL_RenderClear(renderer);             // Effacer la fenêtre avant de rendre la main

    state->x += offset_x;                         // passage à la vignette suivante
    state->x %= source.w - offset_x;
}


/**
 * @brief fonction d'affichage du sprite marchant sur la gauche
 * 
 * @param my_texture tableau des texture chargées
 * @param window fenetre à afficher
 * @param renderer rendu à afficher
 */
void marche_gauche(SDL_Texture* my_texture[10], SDL_Window* window, SDL_Renderer* renderer, SDL_Rect nuage[2], SDL_Rect *destination, SDL_Rect* state) {
    SDL_Rect 
        source = {0},                    // Rectangle définissant la zone totale de la planche
        window_dimensions = {0};         // Rectangle définissant la fenêtre, on n'utilisera que largeur et hauteur
 

    SDL_GetWindowSize(window,              // Récupération des dimensions de la fenêtre
            &window_dimensions.w,
            &window_dimensions.h);
    SDL_QueryTexture(my_texture[1],           // Récupération des dimensions de l'image
            NULL, NULL,
            &source.w, &source.h);

    float angle = 0.0f; // set the angle.
    SDL_RendererFlip flip = SDL_FLIP_HORIZONTAL; // the flip of the texture.

    SDL_Rect fond;

    fond.x = 0;                                   // x haut gauche du rectangle
    fond.y = 0;                                   // y haut gauche du rectangle
    fond.w = window_dimensions.w;                 // sa largeur (w = width)
    fond.h = window_dimensions.h;                 // sa hauteur (h = height)


    /* Mais pourquoi prendre la totalité de l'image, on peut n'en afficher qu'un morceau, et changer de morceau :-) */

    int nb_images = 9;                     // Il y a 8 vignette dans la ligne de l'image qui nous intéresse
    float zoom = 2;                        // zoom, car ces images sont un peu petites
    int offset_x = source.w / nb_images,   // La largeur d'une vignette de l'image, marche car la planche est bien réglée
        offset_y = source.h / 5;           // La hauteur d'une vignette de l'image, marche car la planche est bien réglée
    

    state->y = 4 * offset_y;                // On s'intéresse à la 4ème ligne, le bonhomme qui court
    state->w = offset_x;                    // Largeur de la vignette
    state->h = offset_y;                    // Hauteur de la vignette
    SDL_Point center = {offset_x/2, offset_y/2};             // the center where the texture will be rotated.

    destination->w = offset_x * zoom;       // Largeur du sprite à l'écran
    destination->h = offset_y * zoom;       // Hauteur du sprite à l'écran

    int speed = 15;

    //parallax des nuages
    nuage[0].x += 13;
    nuage[1].x += 10;
    if (nuage[0].x > window_dimensions.w + window_dimensions.w/5)
    {
        nuage[0].x = -window_dimensions.w/5;
    }
    if (nuage[1].x > window_dimensions.w + window_dimensions.w/5)
    {
        nuage[1].x = -window_dimensions.w/5;
    }
    


    destination->x -= speed;                   // Position en x pour l'affichage du sprite

    if (destination->x < 0)
    {
        destination->x = 0;
    }


    SDL_RenderClear(renderer);           // Effacer l'image précédente avant de dessiner la nouvelle
    SDL_RenderCopy(renderer, my_texture[0], NULL, &fond);
    SDL_RenderCopy(renderer, my_texture[2], NULL, &nuage[0]);
    SDL_RenderCopy(renderer, my_texture[3], NULL, &nuage[1]);
    SDL_RenderCopyEx(renderer, my_texture[1], state , destination, angle, &center, flip);
    SDL_RenderPresent(renderer);         // Affichage
    SDL_Delay(40);                       // Pause en ms
    
    SDL_RenderClear(renderer);             // Effacer la fenêtre avant de rendre la main

    state->x += offset_x;                 // On passe à la vignette suivante dans l'image
    state->x %= source.w - offset_x;                 // La vignette qui suit celle de fin de ligne est
}


int main(int argc, char **argv)
{
    (void)argc;
    (void)argv;

    SDL_Window *window = NULL;
    SDL_Renderer *renderer = NULL;
    SDL_Texture *texture[10];
    SDL_Rect nuage[2], destination, state;

    int h ,w;

    for (int i = 0; i < 10; i++)
    {
        texture[i] = NULL;
    }
    

    SDL_DisplayMode screen;

    /*********************************************************************************************************************/
    /*                         Initialisation de la SDL  + gestion de l'échec possible                                   */
    if (SDL_Init(SDL_INIT_VIDEO) != 0)
        end_sdl(0, "ERROR SDL INIT", window, renderer,texture);

    SDL_GetCurrentDisplayMode(0, &screen);
    printf("Résolution écran\n\tw : %d\n\th : %d\n",
           screen.w, screen.h);

    /* Création de la fenêtre */
    window = SDL_CreateWindow("Premier dessin",
                              SDL_WINDOWPOS_CENTERED,
                              SDL_WINDOWPOS_CENTERED, screen.w * 0.66,
                              screen.h * 0.66,
                              SDL_WINDOW_OPENGL);
    if (window == NULL)
        end_sdl(0, "ERROR WINDOW CREATION", window, renderer,texture);

    /* Création du renderer */
    renderer = SDL_CreateRenderer(window, -1,
                                  SDL_RENDERER_ACCELERATED | SDL_RENDERER_PRESENTVSYNC);
    if (renderer == NULL)
        end_sdl(0, "ERROR RENDERER CREATION", window, renderer,texture);

    SDL_GetWindowSize(window, &w, &h);

    destination.y = (h - 400)/2; // position du sprite au centre de l'écran
    

    destination.x = (w - 300)/2; 

    state.x = 0;
    state.y = 0;

    const Uint8* keystates = SDL_GetKeyboardState(NULL); // Récupère l'état des touches du clavier


    texture[0] = load_texture_from_image("img/fond.png",window,renderer, texture);
    texture[1] = load_texture_from_image("img/character.png",window,renderer,texture);
    texture[2] = load_texture_from_image("img/nuage1.png",window,renderer,texture);
    texture[3] = load_texture_from_image("img/nuage2.png",window,renderer,texture);

    SDL_QueryTexture(texture[2],           // Récupération des dimensions de l'image
            NULL, NULL,
            &(nuage[0].w), &(nuage[0].h));

    SDL_QueryTexture(texture[3],           // Récupération des dimensions de l'image
            NULL, NULL,
            &(nuage[1].w), &(nuage[1].h));

    nuage[0].x = -w/5;
    nuage[0].y = h/12;
    nuage[0].h = h/4;
    nuage[0].w = w/4;

    nuage[1].x = -w/5;
    nuage[1].y = h/7;
    nuage[1].h = h/3;
    nuage[1].w = w/3;



    /*********************************************************************************************************************/
    /*                                     On dessine dans le renderer                                                   */
    /*********************************************************************************************************************/
    /*             Cette partie pourrait avantageusement être remplacée par la boucle évènementielle                     */

    SDL_bool program_on = SDL_TRUE; // Booléen pour dire que le programme doit continuer
    SDL_Event event;                // c'est le type IMPORTANT !!


    while (program_on)
        { // Voilà la boucle des évènements

        //parallax des nuages
        nuage[0].x += 10;
        nuage[1].x += 7;
        if (nuage[0].x > w + w/5)
        {
            nuage[0].x = -w/5;
        }
        if (nuage[1].x > w + w/5)
        {
            nuage[1].x = -w/5;
        }
        

        if (SDL_PollEvent(&event))
        {
            keystates = SDL_GetKeyboardState(NULL);
            switch (event.type)
            {                         // En fonction de la valeur du type de cet évènement
            case SDL_QUIT:            // Un évènement simple, on a cliqué sur la x de la fenêtre
                program_on = SDL_FALSE; // Il est temps d'arrêter le programme
                break;
            default:
                break;
            }
            if (keystates[SDL_SCANCODE_RIGHT])
                marche_droite(texture, window, renderer, nuage, &destination, &state);
            else if(keystates[SDL_SCANCODE_LEFT])
                marche_gauche(texture, window, renderer, nuage, &destination, &state);
            else if (keystates[SDL_SCANCODE_ESCAPE])
                program_on = SDL_FALSE;
        }
        
        else
            draw(renderer, window, texture, nuage, &destination, &state);
    }

    /* on referme proprement la SDL */
    end_sdl(1, "Normal ending", window, renderer, texture);
    return EXIT_SUCCESS;
}