#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>
#include <math.h>
#include <stdio.h>
#include <string.h>

/**
 * @brief fonction de fermeture de toutes les variables ouvertes par l'utilisation de la SDL
 * 
 * @param ok fin normale : ok = 0 ; anormale ok = 1
 * @param msg message à afficher
 * @param window fenêtre à fermer
 * @param renderer rendu à fermer
 * @param texture rendu à fermer
 */
void end_sdl(char ok, char const *msg, SDL_Window *window, SDL_Renderer *renderer, SDL_Texture *texture[10]);

/**
 * @brief fonction qui charge une image dans une texture 
 * 
 * @param file_image_name nom du fichier de l'image
 * @param window fenetre à fermer si le chargement echoue
 * @param renderer rendu permettant le chargement de l'image
 * @return SDL_Texture* l'image charé dans l'adresse d'une sdl_texture 
 */
SDL_Texture* load_texture_from_image(char  *  file_image_name, SDL_Window *window, SDL_Renderer *renderer, SDL_Texture* texture[10]);

/**
 * @brief fonction d'affichage du sprite de base
 * 
 * @param renderer rendu à afficher
 * @param window fenetre à afficher
 * @param nuage tableau de coordonnée pour les nuages
 */
void draw(SDL_Renderer *renderer, SDL_Window *window, SDL_Texture *texture[10], SDL_Rect nuage[2], SDL_Rect *destination, SDL_Rect *state);

/**
 * @brief fonction d'affichage du sprite marchant sur la droite
 * 
 * @param my_texture tableau des texture chargées
 * @param window fenetre à afficher
 * @param renderer rendu à afficher
 * @param nuage tableau de coordonnée pour les nuages
 */
void marche_droite(SDL_Texture* my_texture[10], SDL_Window* window, SDL_Renderer* renderer, SDL_Rect nuage[2], SDL_Rect *destination, SDL_Rect *state); 

/**
 * @brief fonction d'affichage du sprite marchant sur la gauche
 * 
 * @param my_texture tableau des texture chargées
 * @param window fenetre à afficher
 * @param renderer rendu à afficher
 * @param nuage tableau de coordonnée pour les nuages
 */
void marche_gauche(SDL_Texture* my_texture[10], SDL_Window* window, SDL_Renderer* renderer, SDL_Rect nuage[2], SDL_Rect *destination, SDL_Rect *state); 