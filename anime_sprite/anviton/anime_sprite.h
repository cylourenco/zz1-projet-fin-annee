/**
 * @file x_fenetre.h
 * @brief fichier d'entete pour la création de forme animée
 */


#include <SDL2/SDL.h>

#ifndef __ANIME_SPRITE_H__
#define __ANIME_SPRITE_H__



void creation_fenetre(void);
void fin_dessin(char ok,            // fin normale : ok = 0 ; anormale ok = 1
             char const *msg,    // message à afficher
             SDL_Window *window, // fenêtre à fermer
             SDL_Renderer *renderer,
             SDL_Texture *textures[10], int nb_textures);
void play_with_texture_4(SDL_Texture** my_texture,
            SDL_Window* window,
            SDL_Renderer* renderer, SDL_Rect *state, SDL_Rect *destination, SDL_Rect *nuages, int direction);
void play_with_texture_1_1(SDL_Texture *my_texture[10], SDL_Window *window,
            SDL_Renderer *renderer, 
            SDL_Rect *destination, SDL_Rect *state, SDL_Rect *nuages);
void draw_fond(SDL_Texture **my_texture, SDL_Window *window,
            SDL_Renderer *renderer, SDL_Rect *nuages);

#endif