# ZZ1-Projet-fin-annee

Ici nous codons en C avec une partie en SDL.
Un site est fait pour répertorier le travail fourni.

Le projet se décompose en deux phases, correspondant chacune approximativement à une semaine de travail. 
La première phase met en place des outils permettant de manipuler des images et d’interagir avec l’utilisateur. Cette phase est ponctuée de petits exercices assez libres pour démontrer la maîtrise des points abordés en individuel et se termine par la création d’un petit jeu.
La seconde phase, plus spécifique à chaque groupe, consiste à réinvestir les acquis de la première phase afin de créer un prototype d'un « jeu vidéo » respectant la thématique du projet.

## Arborescence du projet

## Agenda des 2 semaines

![Agenda de la semaine 1](pages_web/images/agenda.png)

![Agenda de la semaine 2](pages_web/images/planning2.png)

## Première semaine

### X fenêtré

(travail individuel)
Savoir ouvrir, fermer, déplacer, redimensionner des fenêtres graphiques.

### Pavé de serpents 

(travail individuel)
Savoir dessiner des formes géométriques, les animer.

### Animer des sprites

(travail individuel)
Créer une animation possédant un fond (si possible en mouvement), et un sprite qui se déplace sur ce fond.

### Voyageur de commerce 

(travail de groupe)
Créer un jeu basé sur le voyageur de commerce ou équivalent, mettant en oeuvre la partie graphique (formes géométriques, interarctions à la souris et au clavier), une petite partie graphe, et des techniques d’optimisation.

### Chef d’œuvre

(travail de groupe)
Créer un jeu d’arcade simple démontrant votre maîtrise de toute la partie graphique étudiée précédemment. Ce travail termine la première semaine.

## Deuxième semaine

(travail de groupe)
Créer un jeu graphique, avec une partie du jeu où des règles sont apprises automatiquement.

## Auteurs
* **Antoine Viton** _alias_ [@anviton](https://gitlab.isima.fr/anviton)
* **Pierre Bouterige** _alias_ [@pibouterig](https://gitlab.isima.fr/pibouterig)
* **Cynthia Lourenco** _alias_ [@cylourenco](https://gitlab.isima.fr/cylourenco)
