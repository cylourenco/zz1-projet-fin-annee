#ifndef __SMILEY_H__
#define __SMILEY_H__


#include <SDL2/SDL.h>
#include<stdio.h>
#include<stdlib.h>
#include<time.h>
#include<math.h>

#define MAX 50

typedef struct smiley
{
    int x;
    int y;
    int size;
    int score;
    int texture;
    int vitesse;
}smiley_t;

/**
 * @brief initialise des smileys
 * 
 * @param width la taille des smileys 
 * @param time le temps qui s'utilise pour trouver l'accéleration
 * @return smiley_t* retourne une structure smileys initialisé
 */
smiley_t * init_smiley(int width, int time);

/**
 * @brief fonction de mise à jour de la position de tous les smileys 
 * 
 * @param tab_smileys tableau de tous les smileys 
*/
void mise_a_jour_pos_smiley(smiley_t * tab_smileys[MAX], int h_fenetre);

/**
 * @brief liberation du tableau des smileys
 * 
 * @param tab_smileys tableau des smileys
 */
void liberation_smiley(smiley_t * tab_smileys[MAX]);

#endif
