#include "smiley.h"

/**
 * @brief initialise des smileys
 * 
 * @param width la taille des smileys 
 * @param time le temps qui s'utilise pour trouver l'accéleration
 * @return smiley_t* retourne une structure smileys initialisé
 */
smiley_t * init_smiley(int width, int time)
{

    smiley_t *smiley = (smiley_t *)malloc(sizeof(smiley_t));
    int acceleration =  2 * (time/2000);

    if (smiley)
    {
        smiley->x = rand()%width;
        if (width < smiley->x + width*0.13)
        {
            smiley->x = smiley->x - width*0.13;
        }
        
        smiley->y =  -width / 5;
        smiley->texture = rand()%4;

        if(smiley->texture == 0)
        {
            smiley->size = width*0.13;
            smiley->score = -10;
            smiley->vitesse = 5 + rand()%5 + acceleration;
        }
        else if(smiley->texture == 1)
        {
            smiley->size = width*0.1;
            smiley->score = -5;
            smiley->vitesse = 7 + rand()%5 + acceleration;
        }
        else if(smiley->texture == 2)
        {
            smiley->size = width*0.08;
            smiley->score = 5;
            smiley->vitesse = 8 + rand()%5 + acceleration;
        }    
        else
        {
            smiley->size = width*0.07;
            smiley->score = 10;
            smiley->vitesse = 11 + rand()%5 + acceleration;
        }        
    }
    return smiley;
}

/**
 * @brief fonction de mise à jour de la position de tous les smileys 
 * 
 * @param tab_smileys tableau de tous les smileys 
*/
void mise_a_jour_pos_smiley(smiley_t * tab_smileys[MAX], int h_fenetre)
{
    for (int i = 0 ; i < MAX ; i ++)
    {
        if(tab_smileys[i] != NULL)
        {
            tab_smileys[i]->y += tab_smileys[i]->vitesse;
            if (tab_smileys[i]->y + tab_smileys[i]->size > h_fenetre)
            {

                free(tab_smileys[i]);
                tab_smileys[i] = NULL;
            }
        }
    }
}

/**
 * @brief liberation du tableau des smileys
 * 
 * @param tab_smileys tableau des smileys
 */
void liberation_smiley(smiley_t * tab_smileys[MAX])
{
    for (int i = 0 ; i < MAX ; i ++)
    {
        if(tab_smileys[i] != NULL)
        {
            {  
                free(tab_smileys[i]);
                tab_smileys[i] = NULL;
            }
        }
    }
}

/**
 * @brief main test du fichier  
*/
void smiley()
{

}
