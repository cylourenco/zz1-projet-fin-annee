#ifndef __JEU_H__
#define __JEU_H__

#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>
#include <SDL2/SDL_ttf.h>
#include <math.h>
#include <stdio.h>
#include <string.h>
#include "smiley.h"

/**
 * @brief fonction de fermeture de toutes les variables ouvertes par l'utilisation de la SDL
 * 
 * @param ok fin normale : ok = 0 ; anormale ok = 1
 * @param msg message à afficher
 * @param window fenêtre à fermer
 * @param renderer rendu à fermer
 * @param texture rendu à fermer
 */
void end_sdl(char ok, char const *msg, SDL_Window *window, SDL_Renderer *renderer, SDL_Texture *texture[10]);

/**
 * @brief fonction qui charge une image dans une texture 
 * 
 * @param file_image_name nom du fichier de l'image
 * @param window fenetre à fermer si le chargement echoue
 * @param renderer rendu permettant le chargement de l'image
 * @return SDL_Texture* l'image charé dans l'adresse d'une sdl_texture 
 */
SDL_Texture* load_texture_from_image(char  *  file_image_name, SDL_Window *window, SDL_Renderer *renderer, SDL_Texture* texture[10]);

/**
 * @brief retourne si la souris se trouve dans un des boutons du menu
 * 
 * @param mouse_x position de la souris en x
 * @param mouse_y position de la souris en y
 * @param size_menu_x taille de la largeur du rectangle
 * @param size_menu_y taille de la hauteur du rectangle
 * @param x position du rectangle en x
 * @param y position du rectangle en y
 * @return int entier qui retourne si la position de la souris se trouve dans Jouer (1) ou Quitter (0)
 */
int check_click_menu(int mouse_x, int mouse_y, int size_menu_x, int size_menu_y, int x, int y, int * score);

/**
 * @brief dessine les rectangle dans le menu
 * 
 * @param renderer pointeur du rendu
 * @param window pointeur de la fenêtre
 * @param font pointeur de la police
 * @param etat savoir dans quel type d'état pour afficher le texte dans le rectangle
 */
void draw_rectangle_menu(SDL_Renderer *renderer, SDL_Window *window, TTF_Font *font, int etat, Uint8 color_rect);

/**
 * @brief gestion des smileys et du scores lorsque le perso touche un smiley
 * 
 * @param tab_smileys tableau des smileys
 * @param rect_perso postion du perso
 * @param score valeur du score
 */
void inter_perso_smiley(smiley_t *tab_smileys[MAX], SDL_Rect rect_perso, int * score);

/**
 * @brief afficher les smileys
 * 
 * @param smileys liste de smileys
 * @param renderer le rendu
 * @param rect_smiley la liste des positions des smileys
 * @param texture liste de texture
 */
void affichage_smileys(smiley_t * smileys[MAX], SDL_Renderer * renderer, SDL_Rect rect_smiley[4],SDL_Texture * texture[10]);

/**
 * @brief fonction affichage qui affiche le fond, les nuages et les smileys
 * 
 * @param my_texture liste des textures
 * @param window la fenetre
 * @param renderer le rendu
 * @param nuages liste des nuages
 * @param smileys liste des smileys
 * @param rect_smiley la liste des position des smileys
 */
void draw_fond(SDL_Texture **my_texture, SDL_Window *window,
            SDL_Renderer *renderer, SDL_Rect *nuages, 
            smiley_t * smileys[MAX], SDL_Rect rect_smiley[4]);

/**
 * @brief affichage lorsque le perso est à l'arrêt
 * 
 * @param my_texture liste des textures
 * @param window la fenetre
 * @param renderer le rendu
 * @param state la position du rectangle sur les smileys
 * @param destination la position du rectangle sur le renderer
 * @param nuages liste des nuages
 * @param smileys liste des smileys
 * @param rect_smiley la liste des position des smileys
 */
void perso_arret(SDL_Texture *my_texture[10], SDL_Window *window,
            SDL_Renderer *renderer, 
            SDL_Rect *destination, SDL_Rect *state, SDL_Rect *nuages,
            smiley_t * smileys[MAX], SDL_Rect rect_smiley[4]);

/**
 * @brief affichage lorsque le perso bouge
 * 
 * @param my_texture liste des textures
 * @param window la fenetre
 * @param renderer le rendu
 * @param state la position du rectangle sur les smileys
 * @param destination la position du rectangle sur le renderer
 * @param nuages liste des nuages
 * @param direction la direction du perso 
 * @param smileys liste des smileys
 * @param rect_smiley la liste des position des smileys
 */
void perso_bouge(SDL_Texture** my_texture,
            SDL_Window* window,
            SDL_Renderer* renderer, SDL_Rect *state, SDL_Rect *destination, SDL_Rect *nuages, int direction,
            smiley_t * smileys[MAX], SDL_Rect rect_smiley[4]);

/**
 * @brief lit le meilleur score 
 * @param nom_fichier chemin du fichier qui contient le meilleur score
 * @return un entier qui contient le meilleur score lu
*/
int lecture_du_meilleur_score(char *nom_fichier);

/**
 * @brief sauvegarde le score en paramètre dans le fichier
 * @param nom_fichier chaines de caractère contenant le nom du fichier
 * @param score entier qui contient le score à save
*/
void sauvegarde_du_meilleur_score(char *nom_fichier, int score);

int SDL();

#endif