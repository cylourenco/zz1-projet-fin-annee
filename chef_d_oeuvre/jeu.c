#include "jeu.h"
#include "smiley.h"



/*********************************************************************************************************************/
/*                              Programme d'exemple de création de rendu + dessin                                    */
/*********************************************************************************************************************/

/**
 * @brief fonction de fermeture de toutes les variables ouvertes par l'utilisation de la SDL
 *
 * @param ok fin normale : ok = 0 ; anormale ok = 1
 * @param msg message à afficher
 * @param window fenêtre à fermer
 * @param renderer rendu à fermer
 * @param texture rendu à fermer
 */
void end_sdl(char ok, char const *msg, SDL_Window *window, SDL_Renderer *renderer, SDL_Texture *texture[10])
{
    char msg_formated[255];
    int l;

    if (!ok)
    { // Affichage de ce qui ne va pas
        strncpy(msg_formated, msg, 250);
        l = strlen(msg_formated);
        strcpy(msg_formated + l, " : %s\n");

        SDL_Log(msg_formated, SDL_GetError());
    }

    for (int i = 0; i < 10; i++)
    {
        if (texture[i] != NULL)
        {                                   // Destruction si nécessaire de la texture
            SDL_DestroyTexture(texture[i]); // Attention : on suppose que les NULL sont maintenus !!
            texture[i] = NULL;
        }
    }
    if (renderer != NULL)
    {                                  // Destruction si nécessaire du renderer
        SDL_DestroyRenderer(renderer); // Attention : on suppose que les NULL sont maintenus !!
        renderer = NULL;
    }
    if (window != NULL)
    {                              // Destruction si nécessaire de la fenêtre
        SDL_DestroyWindow(window); // Attention : on suppose que les NULL sont maintenus !!
        window = NULL;
    }

    IMG_Quit();
    TTF_Quit(); 
    SDL_Quit();

    if (!ok)
    { // On quitte si cela ne va pas
        exit(EXIT_FAILURE);
    }
}

/**
 * @brief fonction qui charge une image dans une texture
 *
 * @param file_image_name nom du fichier de l'image
 * @param window fenetre à fermer si le chargement echoue
 * @param renderer rendu permettant le chargement de l'image
 * @return SDL_Texture* l'image charé dans l'adresse d'une sdl_texture
 */
SDL_Texture *load_texture_from_image(char *file_image_name, SDL_Window *window, SDL_Renderer *renderer, SDL_Texture *texture[10])
{
    SDL_Surface *my_image = NULL;   // Variable de passage
    SDL_Texture *my_texture = NULL; // La texture

    my_image = IMG_Load(file_image_name); // Chargement de l'image dans la surface
                                          // image=SDL_LoadBMP(file_image_name); fonction standard de la SDL,
                                          // uniquement possible si l'image est au format bmp */
    if (my_image == NULL)
        end_sdl(0, "Chargement de l'image impossible", window, renderer, texture);

    my_texture = SDL_CreateTextureFromSurface(renderer, my_image); // Chargement de l'image de la surface vers la texture
    SDL_FreeSurface(my_image);                                     // la SDL_Surface ne sert que comme élément transitoire
    if (my_texture == NULL)
        end_sdl(0, "Echec de la transformation de la surface en texture", window, renderer, texture);

    return my_texture;
}

/**
 * @brief fonction affichage qui affiche le fond, les nuages et les smileys
 * 
 * @param my_texture liste des textures
 * @param window la fenetre
 * @param renderer le rendu
 * @param nuages liste des nuages
 * @param smileys liste des smileys
 * @param rect_smiley la liste des position des smileys
 */
void draw_fond(SDL_Texture **my_texture, SDL_Window *window,
            SDL_Renderer *renderer, SDL_Rect *nuages, 
smiley_t * smileys[MAX], SDL_Rect rect_smiley[4]){
    SDL_Rect 
        source = {0},                         // Rectangle définissant la zone de la texture à récupérer
        window_dimensions = {0},              // Rectangle définissant la fenêtre, on n'utilisera que largeur et hauteur
        destination = {0};                    // Rectangle définissant où la zone_source doit être déposée dans le renderer

    SDL_GetWindowSize(
    window, &window_dimensions.w,
    &window_dimensions.h);                    // Récupération des dimensions de la fenêtre
    SDL_QueryTexture(my_texture[2], NULL, NULL,
                &source.w, &source.h);     // Récupération des dimensions de l'image

    nuages[0].x += 6;
    nuages[1].x += 10;
    if (nuages[0].x > window_dimensions.w + window_dimensions.w/5)
    {
        nuages[0].x = -window_dimensions.w/5;
    }
    if (nuages[1].x > window_dimensions.w + window_dimensions.w/5)
    {
        nuages[1].x = -window_dimensions.w/3;
    }
    mise_a_jour_pos_smiley(smileys, window_dimensions.h);
    destination = window_dimensions;              // On fixe les dimensions de l'affichage à  celles de la fenêtre
    /* On veut afficher la texture de façon à ce que l'image occupe la totalité de la fenêtre */
    SDL_RenderCopy(renderer, my_texture[0],
            &source,
            &destination);                 // Création de l'élément à afficher

        SDL_Rect    rect_temps = {0},
                rect_score = {0};

    SDL_QueryTexture(my_texture[5], NULL, NULL, &rect_temps.w, &rect_temps.h);
    rect_temps.x = window_dimensions.w - rect_temps.w;
    SDL_RenderCopy(renderer, my_texture[5], NULL, &rect_temps); 
    
    SDL_QueryTexture(my_texture[6], NULL, NULL, &rect_score.w, &rect_score.h);
    SDL_RenderCopy(renderer, my_texture[6], NULL, &rect_score);

    SDL_RenderCopy(renderer, my_texture[2], NULL, &nuages[0]);
    affichage_smileys(smileys, renderer, rect_smiley, my_texture);
    SDL_RenderCopy(renderer, my_texture[3], NULL, &nuages[1]);

}

/**
 * @brief affichage lorsque le perso est à l'arrêt
 * 
 * @param my_texture liste des textures
 * @param window la fenetre
 * @param renderer le rendu
 * @param state la position du rectangle sur les smileys
 * @param destination la position du rectangle sur le renderer
 * @param nuages liste des nuages
 * @param smileys liste des smileys
 * @param rect_smiley la liste des position des smileys
 */
void perso_arret(SDL_Texture *my_texture[10], SDL_Window *window,
            SDL_Renderer *renderer, 
            SDL_Rect *destination, SDL_Rect *state, SDL_Rect *nuages,
            smiley_t * smileys[MAX], SDL_Rect rect_smiley[4]) {
    SDL_Rect 
        source = {0},                         // Rectangle définissant la zone de la texture à récupérer
        window_dimensions = {0};            // Rectangle définissant la fenêtre, on n'utilisera que largeur et hauteu                   // Rectangle définissant où la zone_source doit être déposée dans le renderer

    SDL_GetWindowSize(
    window, &window_dimensions.w,
    &window_dimensions.h);                    // Récupération des dimensions de la fenêtre
    SDL_QueryTexture(my_texture[1], NULL, NULL,
                &source.w, &source.h);       // Récupération des dimensions de l'image
             // On fixe les dimensions de l'affichage à  celles de la fenêtre

    int nb_images = 9;                     // Il y a 8 vignette dans la ligne de l'image qui nous intéresse
    float zoom = 1;                        // zoom, car ces images sont un peu petites
    int offset_x = source.w / nb_images,   // La largeur d'une vignette de l'image, marche car la planche est bien réglée
        offset_y = source.h / 5;

    state->w = offset_x;                    // Largeur de la vignette
    state->h = offset_y;                    // Hauteur de la vignette

    destination->w = offset_x * zoom;       // Largeur du sprite à l'écran
    destination->h = offset_y * zoom;       // Hauteur du sprite à l'écran

    /* On veut afficher la texture de façon à ce que l'image occupe la totalité de la fenêtre */

    draw_fond(my_texture, window, renderer, nuages, smileys, rect_smiley);

    SDL_RenderCopy(renderer, my_texture[1], // Préparation de l'affichage
        state,
        destination);
    SDL_RenderPresent(renderer);  
    SDL_Delay(40); 
    
    
}

/**
 * @brief affichage lorsque le perso bouge
 * 
 * @param my_texture liste des textures
 * @param window la fenetre
 * @param renderer le rendu
 * @param state la position du rectangle sur les smileys
 * @param destination la position du rectangle sur le renderer
 * @param nuages liste des nuages
 * @param direction la direction du perso 
 * @param smileys liste des smileys
 * @param rect_smiley la liste des position des smileys
 */
void perso_bouge(SDL_Texture** my_texture,
            SDL_Window* window,
            SDL_Renderer* renderer, SDL_Rect *state, SDL_Rect *destination, SDL_Rect *nuages, int direction,
            smiley_t * smileys[MAX], SDL_Rect rect_smiley[4]) {

    SDL_Rect 
    source = {0},                    // Rectangle définissant la zone totale de la planche
    window_dimensions = {0};


    SDL_GetWindowSize(window,                   // Récupération des dimensions de la fenêtre
              &window_dimensions.w, 
              &window_dimensions.h); 
    SDL_QueryTexture(my_texture[1], NULL, NULL,    // Récupération des dimensions de l'image
             &source.w, &source.h); 

    int nb_images = 9;                     // Il y a 8 vignette dans la ligne de l'image qui nous intéresse
    float zoom = 1;                        // zoom, car ces images sont un peu petites
    int offset_x = source.w / nb_images,   // La largeur d'une vignette de l'image, marche car la planche est bien réglée
        offset_y = source.h / 5;

    state->w = offset_x;                    // Largeur de la vignette
    state->h = offset_y;                    // Hauteur de la vignette

    state->y = offset_y * 4;

    destination->w = offset_x * zoom;       // Largeur du sprite à l'écran
    destination->h = offset_y * zoom;       // Hauteur du sprite à l'écran

    int speed = 15;

    SDL_RenderClear(renderer); // Effacer l'image précédente avant de dessiner la nouvelle
    draw_fond(my_texture, window, renderer, nuages, smileys, rect_smiley);

    if(direction == SDLK_LEFT){
        destination->x -= speed;
        if (destination->x < 0)
        {
            destination->x = 0;
        }
        SDL_Point point = {offset_x/2, offset_x/2};
        SDL_RenderCopyEx(renderer, my_texture[1], // Préparation de l'affichage
            state,
            destination, 0.0, &point, SDL_FLIP_HORIZONTAL);
    }else{
        destination->x += speed;
        if (destination->x > (window_dimensions.w - destination->w))
        {
            destination->x = (window_dimensions.w - destination->w);
        }
        SDL_RenderCopy(renderer, my_texture[1], // Préparation de l'affichage
            state,
            destination);
    }
    SDL_RenderPresent(renderer);         // Affichage 
    SDL_Delay(40);                  
    SDL_RenderClear(renderer);             // Effacer la fenêtre avant de rendre la main
    state->x += offset_x;
    state->x %= source.w - offset_x;
}


/**
 * @brief retourne si la souris se trouve dans un des boutons du menu
 * 
 * @param mouse_x position de la souris en x
 * @param mouse_y position de la souris en y
 * @param size_menu_x taille de la largeur du rectangle
 * @param size_menu_y taille de la hauteur du rectangle
 * @param x position du rectangle en x
 * @param y position du rectangle en y
 * @return int entier qui retourne si la position de la souris se trouve dans Jouer (1) ou Quitter (0)
 */
int check_click_menu(int mouse_x, int mouse_y, int size_menu_x, int size_menu_y, int x, int y, int * score)
{
    int zone = 0;

    if((mouse_x > x) && (mouse_x < x + size_menu_x) && (mouse_y > y) && (mouse_y < y + size_menu_y))
    {
        zone = 1;
        *score = 0;
    }
    else if((mouse_x > x) && (mouse_x < x + size_menu_x) && (mouse_y > 3*y) && (mouse_y < 3*y + size_menu_y))
    {
        zone = -1;
    }

    return zone;
}

/**
 * @brief dessine les rectangle dans le menu
 * 
 * @param renderer pointeur du rendu
 * @param window pointeur de la fenêtre
 * @param font pointeur de la police
 * @param etat savoir dans quel type d'état pour afficher le texte dans le rectangle
 */
void draw_rectangle_menu(SDL_Renderer *renderer, SDL_Window *window, TTF_Font *font, int etat, Uint8 color_rect)
{
    SDL_Rect rectangle = {0};
    SDL_Color color = {0, 0, 200, 255};
    SDL_Surface *text_surface = NULL;

    int w, h;
    char mot[255];
    SDL_Rect pos = {0,0,0,0};

    SDL_SetRenderDrawColor(renderer, color_rect, color_rect, color_rect, 255);
    
    SDL_GetWindowSize(window, &w, &h);

    if (etat == 1)
    {
        rectangle.y = (h - h * 0.2) / 4; // y haut gauche du rectangle
        sprintf(mot, "JOUER");       
    }
    else
    {
        rectangle.y = 3 * (h - h * 0.2) / 4; // y haut gauche du rectangle
        sprintf(mot, "QUITTER");     
    }

    rectangle.x = (w - w * 0.5) / 2;    // x haut gauche du rectangle
    rectangle.w = w * 0.5;               // sa largeur (w = width)
    rectangle.h = h * 0.2;               // sa hauteur (h = height)

    SDL_RenderFillRect(renderer, &rectangle);
    SDL_SetRenderDrawColor(renderer, 0, 0, 200, 255);
    SDL_RenderDrawRect(renderer, &rectangle);

    text_surface = TTF_RenderText_Blended(font, mot, color);
    SDL_Texture *texture = SDL_CreateTextureFromSurface(renderer, text_surface); // transfert de la surface à la texture
    SDL_FreeSurface(text_surface);                                               // la texture ne sert plus à rien
    SDL_QueryTexture(texture, NULL, NULL, &pos.w, &pos.h);     
    pos.x = rectangle.x + rectangle.w/2 - pos.w/2;
    pos.y = rectangle.y + rectangle.h/2 - pos.h/2;
    SDL_RenderCopy(renderer, texture, NULL, &pos);  // Ecriture du texte dans le renderer
}

/**
 * @brief afficher les smileys
 * 
 * @param smileys liste de smileys
 * @param renderer le rendu
 * @param rect_smiley la liste des positions des smileys
 * @param texture liste de texture
 */
void affichage_smileys(smiley_t * smileys[MAX], SDL_Renderer * renderer, SDL_Rect rect_smiley[4],SDL_Texture * texture[10])
{
    SDL_Rect pos  = {0};
    for (int i = 0; i < MAX; i++)
    {
        if(smileys[i])
        {
            pos.x = smileys[i]->x;
            pos.y = smileys[i]->y;
            pos.w = smileys[i]->size;
            pos.h = smileys[i]->size;

            SDL_RenderCopy(renderer, texture[4], &rect_smiley[smileys[i]->texture], &pos);
        }
    }
}

/**
 * @brief gestion des smileys et du scores lorsque le perso touche un smiley
 * 
 * @param tab_smileys tableau des smileys
 * @param rect_perso postion du perso
 * @param score valeur du score
 */
void inter_perso_smiley(smiley_t *tab_smileys[MAX], SDL_Rect rect_perso, int * score)
{
    int i;

    for(i = 0; i < MAX; i++)
    {
        if(tab_smileys[i])
        {
            if(tab_smileys[i]->y + tab_smileys[i]->size > rect_perso.y + rect_perso.y/6)
            {
                if((tab_smileys[i]->x + tab_smileys[i]->size > rect_perso.x + rect_perso.x/12 && tab_smileys[i]->x + tab_smileys[i]->size < rect_perso.x + rect_perso.w) ||
                (tab_smileys[i]->x < rect_perso.x + rect_perso.w - rect_perso.x/12 && tab_smileys[i]->x > rect_perso.x))
                {
                    *score += tab_smileys[i]->score;
                    free(tab_smileys[i]);
                    tab_smileys[i] = NULL;
                }
            }
        }
    } 
}

/**
 * @brief lit le meilleur score 
 * @param nom_fichier chemin du fichier qui contient le meilleur score
 * @return un entier qui contient le meilleur score lu
*/
int lecture_du_meilleur_score(char *nom_fichier){
    FILE *flot;
    int score = 0;
    flot = fopen(nom_fichier, "r");
    if(flot != NULL){
        fscanf(flot, "%d", &score);
    }
    return score;
}

/**
 * @brief sauvegarde le score en paramètre dans le fichier
 * @param nom_fichier chaines de caractère contenant le nom du fichier
 * @param score entier qui contient le score à save
*/
void sauvegarde_du_meilleur_score(char *nom_fichier, int score){
    FILE *flot;
    flot = fopen(nom_fichier, "w");
    if(flot != NULL){
        fprintf(flot, "%d", score);
    }
}

int SDL()
{

    SDL_Window *window = NULL;              
    SDL_Renderer *renderer = NULL;
    TTF_Font *font = NULL;    
    SDL_Texture *texture[10];                       // liste des textures
    SDL_Rect nuage[2], destination, state, rect_temps = {0}; // liste rect des nuages
    SDL_Color color = {255, 255, 255, 255};         // couleur de base pour les écriture (blanche)
    SDL_Rect rect_smiley[4];                        // rect des smileys
    smiley_t * smileys[MAX];                        // liste des smileys
    int meilleur_score = 0;
    int right_keydown = 0, left_keydown = 0;

    meilleur_score = lecture_du_meilleur_score("persistance/meilleur_score.don");
    for (int i = 0; i < MAX; i++)
    {
        smileys[i] = NULL;
    }
    

    int h, w,                     // dimension de la page
        mouse_x, mouse_y,         // parametre ou se trouve l'endroit ou on clique
        size_menu_x, size_menu_y, // taille des boutons du menu
        rectangle_x, rectangle_y, // position du rectangle
        temps_courant, temps_max, temps_depart, // temps du jeu;   
        score = 0;                  // initialisation du score    

    // initialisation des textures
    for (int i = 0; i < 10; i++)
    {
        texture[i] = NULL;
    }

    SDL_DisplayMode screen;

    /*********************************************************************************************************************/
    /*                         Initialisation de la SDL  + gestion de l'échec possible                                   */
    /*********************************************************************************************************************/
    if (SDL_Init(SDL_INIT_VIDEO) != 0)
        end_sdl(0, "ERROR SDL INIT", window, renderer, texture);

    

    SDL_GetCurrentDisplayMode(0, &screen);

    /* Création de la fenêtre */
    window = SDL_CreateWindow("Chez DEuve",
                              SDL_WINDOWPOS_CENTERED,
                              SDL_WINDOWPOS_CENTERED, screen.w * 0.66,
                              screen.h * 0.66,
                              SDL_WINDOW_OPENGL);
    if (window == NULL)
        end_sdl(0, "ERROR WINDOW CREATION", window, renderer, texture);

    /* Création du renderer */
    renderer = SDL_CreateRenderer(window, -1,
                                  SDL_RENDERER_ACCELERATED | SDL_RENDERER_PRESENTVSYNC);
    if (renderer == NULL)
        end_sdl(0, "ERROR RENDERER CREATION", window, renderer, texture);

    if (TTF_Init() < 0)
        end_sdl(0, "Couldn't initialize SDL TTF", window, renderer, NULL);
    font = TTF_OpenFont("./fonts/Salma.otf", 50); // La police à charger, la taille désirée

    // avoir la taille de l'écran
    SDL_GetWindowSize(window, &w, &h);


    state.x = 0;
    state.y = 0;

    const Uint8 *keystates = SDL_GetKeyboardState(NULL); // Récupère l'état des touches du clavier

    // liste de texture tiré des images
    texture[0] = load_texture_from_image("img/fond.png", window, renderer, texture);
    texture[1] = load_texture_from_image("img/character.png", window, renderer, texture);
    texture[2] = load_texture_from_image("img/nuage1.png", window, renderer, texture);
    texture[3] = load_texture_from_image("img/nuage2.png", window, renderer, texture);
    texture[4] = load_texture_from_image("img/bonus.png", window, renderer, texture);

    SDL_QueryTexture(texture[2], // Récupération des dimensions de l'image
                     NULL, NULL,
                     &(nuage[0].w), &(nuage[0].h));

    SDL_QueryTexture(texture[3], // Récupération des dimensions de l'image
                     NULL, NULL,
                     &(nuage[1].w), &(nuage[1].h));

    SDL_QueryTexture(texture[4],           // Récupération des dimensions de l'image
            NULL, NULL,
            &(rect_smiley[0].w), &(rect_smiley[0].h));

    int nb_images = 4;                           // Il y a 4 vignette dans la ligne de l'image qui nous intéresse
    int offset_x = rect_smiley[0].w / nb_images; // La largeur d'une vignette de l'image, marche car la planche est bien réglée

    destination.y = h - 300; // position du sprite au centre de l'écran
    destination.x = (w - 200) / 2;

    // rect de chaque image (objet à faire tomber)
    for (int i = 0; i < 4; i++)
    {
        rect_smiley[i].x = i * offset_x;
        rect_smiley[i].y = 0;
        rect_smiley[i].h = rect_smiley[0].h;
        rect_smiley[i].w = offset_x;
    }

    // position des nuages et de leur taille 
    nuage[0].x = -w / 5;
    nuage[0].y = h / 12;
    nuage[0].h = h / 4;
    nuage[0].w = w / 4;

    nuage[1].x = -w / 5;
    nuage[1].y = h / 7;
    nuage[1].h = h / 3;
    nuage[1].w = w / 3;

    SDL_bool program_on = SDL_TRUE; // Booléen pour dire que le programme doit continuer
    SDL_Event event;                // c'est le type IMPORTANT !!

    // zone correspond à l'endroit où on se trouve (0 si menu, 1 si jeu et 2 écran de fin)
    int zone = 0;

    SDL_GetWindowSize(window, &w, &h);

    // taille des rectangles dans le menu
    size_menu_x = w * 0.5;
    size_menu_y = h * 0.2;

    // position des rectangles du menu
    rectangle_x = (w - size_menu_x) / 2;
    rectangle_y = (h - size_menu_y) / 4;

    temps_max = 20000;
    temps_courant = 0;

    // buffer pour ranger les phrases que l'on affiche dans le jeu
    char mot_temps[255];
    char mot_de_fin[255];
    char mot_de_credits[255];

    SDL_Rect rect_score = {0};
    SDL_Rect rect_credit = {0};
    SDL_Rect rect_meil_score = {0};
    char aff_meil_score[MAX];

    //gestion couleur bouton
    Uint8   color_jouer,
            color_quitter;
    int check;

    while (program_on && zone != -1)
    {
        keystates = SDL_GetKeyboardState(NULL);
        switch (event.type)
        {                           // En fonction de la valeur du type de cet évènement
        case SDL_QUIT:              // Un évènement simple, on a cliqué sur la x de la fenêtre
            program_on = SDL_FALSE; // Il est temps d'arrêter le programme
            break;

        default:
            break;
        }
        if (keystates[SDL_SCANCODE_ESCAPE])
            program_on = SDL_FALSE;

        SDL_Rect
            fond = {0},   // Rectangle définissant la zone totale de la planche
            source = {0}; // Rectangle définissant la zone totale de la planche

        SDL_QueryTexture(texture[1], NULL, NULL, &source.w, &source.h);

        fond.x = 0; // x haut gauche du rectangle
        fond.y = 0; // y haut gauche du rectangle
        fond.w = w; // sa largeur (w = width)
        fond.h = h; // sa hauteur (h = height)

        // affichage du fond et des nuages
        SDL_RenderCopy(renderer, texture[0], NULL, &fond);
        SDL_RenderCopy(renderer, texture[2], NULL, &nuage[0]);
        SDL_RenderCopy(renderer, texture[3], NULL, &nuage[1]);

        // si on est au menu
        if (zone == 0 || zone == 2)
        {
            if (SDL_PollEvent(&event))
            {
                keystates = SDL_GetKeyboardState(NULL);
                switch (event.type)
                {   
                // si le joueur clique on regarde si il clique dans un carré
                case SDL_MOUSEBUTTONDOWN:
                    mouse_x = event.button.x;
                    mouse_y = event.button.y;
                    zone = check_click_menu(mouse_x, mouse_y, size_menu_x, size_menu_y, rectangle_x, rectangle_y, &score);
                    if (zone == 1)
                    {
                        liberation_smiley(smileys);
                        temps_depart = SDL_GetTicks(); // le decompte commence
                        destination.x = (w - 200) / 2;
                        left_keydown = 0;
                        right_keydown = 0;
                    }
                    break;
                default:
                    break;
                }
            }


            SDL_GetMouseState(&mouse_x, &mouse_y);
            int pointeur = 0;
            check = check_click_menu(mouse_x, mouse_y, size_menu_x, size_menu_y, rectangle_x, rectangle_y, &pointeur);

            color_jouer = 220;
            color_quitter = 220;

            if(check == 1)
            {
                color_jouer = 255;
            }
            else if(check == -1)
            {
                color_quitter = 255;               
            }
            draw_rectangle_menu(renderer, window, font, 1, color_jouer);
            draw_rectangle_menu(renderer, window, font, 0, color_quitter);                

            if(zone == 2)
            {       
                // affichage du score si le joueur a déjà joué         
                sprintf(mot_de_fin, "Votre score est %d", score);
                SDL_Surface *text_surface_mot = TTF_RenderText_Blended(font, mot_de_fin, color);
                if (text_surface_mot == NULL)
                    end_sdl(0, "Can't create text surface", window, renderer, texture); // la texture qui contient le texte
                texture[6] = SDL_CreateTextureFromSurface(renderer, text_surface_mot);
                if (texture[6] == NULL)
                    end_sdl(0, "Can't create texture from surface", window, renderer, texture);
                SDL_FreeSurface(text_surface_mot);
                SDL_QueryTexture(texture[6], NULL, NULL, &rect_score.w, &rect_score.h);
                rect_score.x = w/2 - rect_score.w/2;
                SDL_RenderCopy(renderer, texture[6], NULL, &rect_score);
            }

            // afficher les crédits
            sprintf(mot_de_credits, "Antoine VITON - Pierre BOUTERIGE - Cynthia LOURENCO");
            SDL_Surface *text_surface_credit = TTF_RenderText_Blended(font, mot_de_credits, color);
            if (text_surface_credit == NULL)
                end_sdl(0, "Can't create text surface", window, renderer, texture); // la texture qui contient le texte
            texture[7] = SDL_CreateTextureFromSurface(renderer, text_surface_credit);
            if (texture[7] == NULL)
                end_sdl(0, "Can't create texture from surface", window, renderer, texture);
            SDL_FreeSurface(text_surface_credit);

            SDL_QueryTexture(texture[7], NULL, NULL, &rect_credit.w, &rect_credit.h);
            rect_credit.x = w/2 - rect_credit.w/2;
            rect_credit.y = h - rect_credit.h;
            SDL_RenderCopy(renderer, texture[7], NULL, &rect_credit);


            //affichage meilleur score
            sprintf(aff_meil_score, "High score : %d", meilleur_score);
            SDL_Surface *text_surface_meil_score = TTF_RenderText_Blended(font, aff_meil_score, color);
            if (text_surface_meil_score == NULL)
                end_sdl(0, "Can't create text surface", window, renderer, texture); // la texture qui contient le texte
            texture[8] = SDL_CreateTextureFromSurface(renderer, text_surface_meil_score);
            if (texture[8] == NULL)
                end_sdl(0, "Can't create texture from surface", window, renderer, texture);
            SDL_FreeSurface(text_surface_meil_score);

            SDL_QueryTexture(texture[8], NULL, NULL, &rect_meil_score.w, &rect_meil_score.h);
            rect_meil_score.x = w/2 - rect_meil_score.w/2;
            rect_meil_score.y = 0 + rect_score.h;
            SDL_RenderCopy(renderer, texture[8], NULL, &rect_meil_score);

            // affichage dans le menu
            SDL_RenderPresent(renderer); // affichage
        }

        // si l'écran est le jeu
        else if (zone == 1)
        {
            if (rand()%100 < 8)
            {
                smiley_t * tmp = init_smiley(w, temps_courant);
                int indice = 0;
                while(indice < MAX && smileys[indice] != NULL)
                {
                    indice ++;
                }
                if(MAX != indice)
                    smileys[indice] = tmp;
            }
            inter_perso_smiley(smileys, destination, &score);
            if (SDL_PollEvent(&event))
            {
                keystates = SDL_GetKeyboardState(NULL);
                switch (event.type)
                {                           // En fonction de la valeur du type de cet évènement
                case SDL_QUIT:              // Un évènement simple, on a cliqué sur la x de la fenêtre
                    program_on = SDL_FALSE; // Il est temps d'arrêter le programme
                    break;
                case SDL_KEYUP:
                    right_keydown = 0;
                    left_keydown = 0;
                    break;
                default:
                    break;
                }
                if (keystates[SDL_SCANCODE_RIGHT]){
                    ++right_keydown;
                }
                else if (keystates[SDL_SCANCODE_LEFT]){
                    ++left_keydown;
                }
                else if (keystates[SDL_SCANCODE_ESCAPE])
                    program_on = SDL_FALSE;
            }

            else {
                if(!left_keydown && !right_keydown){
                    state.x = 0;
                    state.y = 0;
                    perso_arret(texture, window, renderer, &destination, &state, nuage, smileys, rect_smiley);
                }
            }


            if(left_keydown)
                perso_bouge(texture, window, renderer, &state, &destination, nuage, SDLK_LEFT, smileys, rect_smiley);
            else if (right_keydown)
                perso_bouge(texture, window, renderer, &state, &destination, nuage, SDLK_RIGHT, smileys, rect_smiley);


            // affiche temps
            temps_courant = SDL_GetTicks() - temps_depart;

            sprintf(mot_temps, "Temps restant :  %d s", (temps_max-temps_courant)/1000);
            SDL_Surface *text_surface_temps = TTF_RenderText_Blended(font, mot_temps, color);
            if (text_surface_temps == NULL)
                end_sdl(0, "Can't create text surface", window, renderer, texture); // la texture qui contient le texte
            texture[5] = SDL_CreateTextureFromSurface(renderer, text_surface_temps);    
            if (texture[5] == NULL)
                end_sdl(0, "Can't create texture from surface", window, renderer, texture);
            SDL_FreeSurface(text_surface_temps); 
            SDL_QueryTexture(texture[5], NULL, NULL, &rect_temps.w, &rect_temps.h);

            // affiche score
            sprintf(mot_de_fin, "Score : %d", score);
            SDL_Surface *text_surface_mot = TTF_RenderText_Blended(font, mot_de_fin, color);
            if (text_surface_mot == NULL)
                end_sdl(0, "Can't create text surface", window, renderer, texture); // la texture qui contient le texte
            texture[6] = SDL_CreateTextureFromSurface(renderer, text_surface_mot);
            if (texture[6] == NULL)
                end_sdl(0, "Can't create texture from surface", window, renderer, texture);
            SDL_FreeSurface(text_surface_mot);

            if (temps_max < temps_courant)
            {
                zone = 2;
                if(score > meilleur_score){
                    meilleur_score = score;
                }
            }
        }

    }

    /* on referme proprement la SDL */
    liberation_smiley(smileys);
    end_sdl(1, "Normal ending", window, renderer, texture);
    sauvegarde_du_meilleur_score("persistance/meilleur_score.don", meilleur_score);
    return EXIT_SUCCESS;
}
