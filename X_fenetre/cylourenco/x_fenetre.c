#include <SDL2/SDL.h>
#include <stdio.h>

int main(int argc, char **argv)
{
  (void)argc;
  (void)argv;

  SDL_Window *window = NULL; // Future fenêtre

  /* Initialisation de la SDL  + gestion de l'échec possible */
  if (SDL_Init(SDL_INIT_VIDEO) != 0)
  {
    SDL_Log("Error : SDL initialisation - %s\n", SDL_GetError()); // l'initialisation de la SDL a échoué
    exit(EXIT_FAILURE);
  }

  window = SDL_CreateWindow(
      "Fenêtre",             // codage en utf8, donc accents possibles
      0, 0,                  // coin haut gauche en haut gauche de l'écran
      400, 300,              // largeur = 400, hauteur = 300
      SDL_WINDOW_RESIZABLE); // redimensionnable

  if (window == NULL)
  {
    SDL_Log("Error : SDL window creation - %s\n", SDL_GetError()); // échec de la création de la fenêtre
    SDL_Quit();                                                    // On referme la SDL
    exit(EXIT_FAILURE);
  }

  SDL_bool program_on = SDL_TRUE; // Booléen pour dire que le programme doit continuer
  SDL_Event event;
  int position_x, position_y, window_width, window_height;

  while (program_on)
  { // Voilà la boucle des évènements
    while (SDL_PollEvent(&event))
    {
      switch (event.type)
      {              // En fonction de la valeur du type de cet évènement
      case SDL_QUIT: // Un évènement simple, on a cliqué sur la x de la // fenêtre
        program_on = SDL_FALSE;
        break;

      case SDL_KEYDOWN:
        switch (event.key.keysym.sym)
        { // la touche appuyée est ...

        case SDLK_LEFT: // si le bouton de fleche gauche touché la fenetre se deplace sur la gauche
          SDL_GetWindowPosition(window, &position_x, &position_y);
          position_x -= 100;
          SDL_SetWindowPosition(window, position_x, position_y);
          break;
        case SDLK_RIGHT: // si le bouton de fleche droit touché la fenetre se deplace sur la droite
          SDL_GetWindowPosition(window, &position_x, &position_y);
          position_x += 100;
          SDL_SetWindowPosition(window, position_x, position_y);
          break;
        case SDLK_UP: // si le bouton de fleche haut touché la fenetre se deplace en haut
          SDL_GetWindowPosition(window, &position_x, &position_y);
          position_y -= 100;
          SDL_SetWindowPosition(window, position_x, position_y);
          break;
        case SDLK_DOWN: // si le bouton de fleche bas touché la fenetre se deplace en bas
          SDL_GetWindowPosition(window, &position_x, &position_y);
          position_y += 100;
          SDL_SetWindowPosition(window, position_x, position_y);
          break;

        case SDLK_1: // si la touche 1 appuyer agrandissement de la fenetre
          SDL_GetWindowSize(window, &window_width, &window_height);
          window_width += 100;
          window_height += 100;
          SDL_SetWindowSize(window, window_width, window_height);
          break;

        case SDLK_2: // si la touche 2 appuyer retrecissement de la fenetre
          SDL_GetWindowSize(window, &window_width, &window_height);
          window_width -= 100;
          window_height -= 100;
          SDL_SetWindowSize(window, window_width, window_height);
          break;

        case SDLK_ESCAPE: // 'ESCAPE'
        case SDLK_q:      // ou 'q'
          program_on = SDL_FALSE;
          break;

        default: // Une touche appuyée qu'on ne traite pas
          break;
        }
        break;

      default:
        break;
      }
    }
  }

  SDL_DestroyWindow(window);
  SDL_Quit(); // la SDL

  return 0;
}
