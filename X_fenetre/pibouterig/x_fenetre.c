#include <SDL2/SDL.h>
#include <stdio.h>

/************************************/
/*  exemple de création de fenêtres */
/************************************/

    int main(int argc, char **argv) {
    (void)argc;
    (void)argv;

    int x, y, xshift, yshift;

    SDL_Window * Tab_window[12];

    SDL_DisplayMode screen;
    

    /* Initialisation de la SDL  + gestion de l'échec possible */
    if (SDL_Init(SDL_INIT_VIDEO) != 0) {
        SDL_Log("Error : SDL initialisation - %s\n", 
                    SDL_GetError());                // l'initialisation de la SDL a échoué 
        exit(EXIT_FAILURE);
    }

    SDL_GetCurrentDisplayMode(0, &screen);
    
    /* Création des fenêtres */
    for (int i = 0 ; i < 7 ; i++){
        Tab_window[i] = SDL_CreateWindow(
        "cercle",                                                                             // codage en utf8, donc accents possibles
        0,0,                                                                                  // coin haut gauche en haut gauche de l'écran
        screen.w/7, screen.h/7,                                                               // largeur = 400, hauteur = 300
        SDL_WINDOW_RESIZABLE);                                                                // redimensionnable

        if (Tab_window[i] == NULL) {
            SDL_Log("Error : SDL window 1 creation - %s\n", 
                        SDL_GetError());                 // échec de la création de la fenêtre
            SDL_Quit();                                  // On referme la SDL       
            exit(EXIT_FAILURE);
        }
        if (i < 4)
            SDL_SetWindowPosition(Tab_window[i], screen.w/15 + i*screen.w/8, screen.h/2 - screen.h/14 - i*screen.h/7);
        else
            SDL_SetWindowPosition(Tab_window[i], screen.w/15 + i*screen.w/8, screen.h/2 - screen.h/14 - (6-i)*screen.h/7);

    }

    for (int i = 7 ; i < 12 ; i++){
        Tab_window[i] = SDL_CreateWindow(
        "cercle",                                                                             // codage en utf8, donc accents possibles
        0,0,                                                                                  // coin haut gauche en haut gauche de l'écran
        screen.w/7, screen.h/7,                                                               // largeur = 400, hauteur = 300
        SDL_WINDOW_RESIZABLE);                                                                // redimensionnable

        if (Tab_window[i] == NULL) {
            SDL_Log("Error : SDL window 1 creation - %s\n", 
                        SDL_GetError());                 // échec de la création de la fenêtre
            SDL_Quit();                                  // On referme la SDL       
            exit(EXIT_FAILURE);
        }
        if (i < 10)
            SDL_SetWindowPosition(Tab_window[i], screen.w/15 + (i-6)*screen.w/8, screen.h/2 - screen.h/14 + (i-6)*screen.h/7);
        else
            SDL_SetWindowPosition(Tab_window[i], screen.w/15 + (i-6)*screen.w/8, screen.h/2 - screen.h/14 + (12-i)*screen.h/7);

    }


    for (int t = 0; t < 25; t++)
    {
        SDL_Delay(1000);
        for (int i = 1; i < 6; i++)
        {
            xshift = (3-i) * screen.w/200;
            if(i <= 3 )
                yshift = i * screen.h/200;
            else
                yshift = (6-i) * screen.h/200;

            SDL_GetWindowPosition(Tab_window[i],&x,&y);
            SDL_SetWindowPosition(Tab_window[i],x + xshift,y + yshift);
            SDL_GetWindowPosition(Tab_window[i+6],&x,&y);
            SDL_SetWindowPosition(Tab_window[i+6],x + xshift,y - yshift);
        }
        SDL_GetWindowPosition(Tab_window[0],&x,&y);
        SDL_SetWindowPosition(Tab_window[0],x + 3*screen.w/200,y);
        SDL_GetWindowPosition(Tab_window[6],&x,&y);
        SDL_SetWindowPosition(Tab_window[6],x - 3*screen.w/200,y);
    }
    
    
    

    /* et on referme tout ce qu'on a ouvert en ordre inverse de la création */
    for (int i = 0 ; i <12 ; i++){
        SDL_DestroyWindow(Tab_window[i]);
    }

    SDL_Quit();                                // la SDL

    return 0;
}
