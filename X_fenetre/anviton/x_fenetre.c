#include <SDL2/SDL.h>
#include <stdio.h>

/**
 * @brief créer un V à partir de 21 fenêtres
*/
void creation_fenetre(void){

    SDL_Window 
    *window_1[10],                     
    *window_2[10];
    int nb_fenetre_diago = 9, indice, indice_mouvement = 0;                    
    int pos_bas_x, pos_bas_y, x_window, y_window;

    SDL_DisplayMode ecran;

    /* Initialisation de la SDL  + gestion de l'échec possible */
    if (SDL_Init(SDL_INIT_VIDEO) != 0) {
        SDL_Log("Error : SDL initialisation - %s\n", 
                    SDL_GetError());                // l'initialisation de la SDL a échoué 
        exit(EXIT_FAILURE);
    }
    SDL_GetCurrentDisplayMode(0, &ecran);
    printf("Taille %d\n", ecran.h);

    for(indice = 0; indice < nb_fenetre_diago; indice++){
    window_1[indice] = SDL_CreateWindow(
        "V de Viton",                    // codage en utf8, donc accents possibles
        ecran.w/6+ indice*50, 0 + indice*100,                       
        100, 100,                              // largeur = 100, hauteur = 100
        SDL_WINDOW_RESIZABLE);
        if (window_1[indice] == NULL) {
            SDL_Log("Error : SDL window 1 creation - %s\n", SDL_GetError());  // échec de la création de la fenêtre
            SDL_Quit();                              // On referme la SDL       
            exit(EXIT_FAILURE);
        }
    }
    pos_bas_x = ecran.w/8 + indice*50;
    pos_bas_y = indice*100;
    for(indice = 0; indice < nb_fenetre_diago; indice++){
        window_2[indice] = SDL_CreateWindow(
        "V de Viton",                    // codage en utf8, donc accents possibles
        pos_bas_x + indice*50, pos_bas_y - indice*100,                                
        100, 100,                              // largeur = 100, hauteur = 100
        SDL_WINDOW_RESIZABLE);
        if (window_2[indice] == NULL) {
            SDL_Log("Error : SDL window 1 creation - %s\n", SDL_GetError());                 // échec de la création de la fenêtre
            SDL_Quit();                              // On referme la SDL       
            exit(EXIT_FAILURE);
        }
    }
    while (indice_mouvement < 20)
    {
        for(indice = 0; indice < nb_fenetre_diago; indice++){
            SDL_GetWindowPosition(window_1[indice], &x_window, &y_window);
            SDL_SetWindowPosition(window_1[indice], x_window + 10, y_window + 10);

            SDL_GetWindowPosition(window_2[indice], &x_window, &y_window);
            SDL_SetWindowPosition(window_2[indice], x_window + 10, y_window + 10);
    }
    indice_mouvement++;
    }
    
    SDL_Delay(10000);    
    for(indice = 0; indice<nb_fenetre_diago; indice++){
        SDL_DestroyWindow(window_1[indice]);
    }
    for(indice = 0; indice<nb_fenetre_diago; indice++){
        SDL_DestroyWindow(window_2[indice]);
    }                

    SDL_Quit();                           
}