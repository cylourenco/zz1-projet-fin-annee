/**
 * @file x_fenetre.h
 * @brief fichier d'entete pour la création d'une fenêtre
 */
#ifndef __X_FENETRE_H__
#define __X_FENETRE_H__

void creation_fenetre(void);

#endif